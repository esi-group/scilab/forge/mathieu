// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - 2016 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

// Two Even Soft modes
    a = 0.05;
    b = 0.03;
    N_xi = 101;
    N_eta = 101;
    options.font_size_axes = 4;

    f_wh = get(0, 'screensize_px'); margin = 50;
    f_h = figure('Background',-2);
    f_h.figure_name = 'Elliptic membrane: comparison of 2 even soft modes';

    subplot(1,2,1);
        mathieu_membrane_mode(a, b, 2, 4, 'Mc1', %t, N_xi, N_eta, options);
        ax = gca();
        ax.rotation_angles = [0,-90];
        ax.margins(1) = 0.2;
        zlabel('');
    subplot(1,2,2);
        mathieu_membrane_mode(a, b, 4, 2, 'Mc1', %t, N_xi, N_eta, options);
        ax = gca();
        ax.rotation_angles = [0,-90];
        ax.margins(1) = 0.2;
        zlabel('');

    f_h.figure_size = [f_wh(3) - 2*margin, f_wh(4) - 2*margin];
    f_h.figure_position = [margin margin];
