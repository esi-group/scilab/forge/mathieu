function Mathieu_Gui()
    // GUI for Mathieu functions toolbox for Scilab
    //
    // --------------------------------------------------------------------------------------- //
    //  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
    // --------------------------------------------------------------------------------------- //
    // TODO: 
    //  1. fix tab1 - a<b -> a>b, but 4 checkboxes unchecked - do not plot

    // identifiers and expressions shorting for Scilab 5.x:
    //  frame -> frm -> fr
    //  label -> lbl -> ll
    //  margin -> mrg
    //  show -> sh
    //  options -> opts
    //  ellipse -> ell
    //  ellipses -> ells
    //  height -> hght -> ht
    //  width -> wdth
    //  hyperb -> hprb
    //  annot -> ant
    //  checkboxes -> chbs
    //  isolines -> isls
    //  elements -> elmts -> els
    //  button -> btn
    //  plot -> plt
    //  function -> fun -> fn
    //  derivative -> der
    //  color -> cr
    //  position -> pos
    //  string -> str
    //  boundary_condition -> b_c
    //  bottom -> bot -> bm
    //  angular -> ang
    //  value -> val
    //  button -> btn
    //  types -> ts
    //  membrane -> membr

    // check Scilab version
    v = getversion("scilab");
    if v(1) <= 5 & v(2) < 5 then
        error(gettext("Scilab 5.5 or more is required for this GUI."));
    end
    
    // ignore 'Division by zero'
    ieee(2);

    // <LOCAL FUNCTIONS>
        // create titled frame
        function [uh_lbl, uh_frame] = create_titled_frame(uh_parent, position, title_size, title_text, font_size)
            global platform_props; // platform properties
            
            title_x = position(1) + fix((position(3) - title_size(1)) / 2);
            title_y = position(2) + position(4) - fix(title_size(2)/2);
            title_wdth = title_size(1);
            title_hght = title_size(2);
            
            uh_lbl = [];
            if ~platform_props.p_titled_createBorder then // platform supports titled createBorder?
                uh_lbl = uicontrol(uh_parent, "style","text", ...
                                     "string",title_text,...
                                     "position",[title_x, title_y, title_wdth, title_hght],...
                                     "fontsize",font_size,...
                                     "background",[1 1 1]);

                uh_frame = uicontrol(uh_parent, "relief","groove", ...
                                    "style","frame","horizontalalignment","center",...
                                    "position", position, ...
                                    "background",[1 1 1]);
            else
                uh_frame = uicontrol(uh_parent, "relief","groove", ...
                                    "style","frame","horizontalalignment","center",...
                                    "position", position, ...
                                    "border", createBorder("titled", createBorder("etched"), title_text, "center", "top", createBorderFont("", font_size, "normal"), "black"), ...
                                    "background",[1 1 1]);
            end
        endfunction
        
        // equally spaced elements
        function uh_array = equally_spaced_elements(pos_matrix_size, position_to_be_filled, uh_array, margin)
            rows = pos_matrix_size(1);
            cols = pos_matrix_size(2);

            position_to_be_filled = position_to_be_filled - [position_to_be_filled(1) position_to_be_filled(2) 0 0] + [margin margin -2*margin -4*margin];
            area_x = position_to_be_filled(1);
            area_y = position_to_be_filled(2);
            area_wdth = position_to_be_filled(3);
            area_hght = position_to_be_filled(4);
            
            cur_w = fix(area_wdth/cols);
            cur_h = fix(area_hght/rows);
            
            [uh_rows, uh_cols] = size(uh_array);
            
            if rows > 1 & cols > 1 then // rectangular grid
                for i=1:rows
                    for j=1:cols
                        if i <= uh_rows & j <= uh_cols then
                            cur_x = (i-1)*cur_w + area_x;
                            cur_y = (j-1)*cur_h + area_y;
                            uh_array(i,j).Position = [cur_x cur_y cur_w cur_h];
                        end
                    end
                end
            elseif cols == 1 & rows > 1 then // column
                for i=1:rows
                    if i <= uh_rows then
                        cur_x = area_x;
                        cur_y = (i-1)*cur_h + area_y;
                        uh_array(i,1).Position = [cur_x cur_y cur_w cur_h];
                    end
                end
            elseif rows == 1 & cols > 1 then // rows
                for j=1:cols
                    if j <= uh_cols then
                        cur_x = (j-1)*cur_w + area_x;
                        cur_y = area_y;
                        uh_array(1,j).Position = [cur_x cur_y cur_w cur_h];
                    end
                end
            end
        endfunction
        
        // function for control frame creation
        function uh_frame = create_control_frame(uh_tab, frm_w, frm_h, font_size, font_style, font_color)
            uh_frame = uicontrol(uh_tab, ...
                                "relief","groove", ...
                                "style","frame", ...
                                "position",[0 0 frm_w frm_h], ...
                                "border", createBorder("titled", createBorder("etched"), "Controls", "center", "top", createBorderFont("", font_size, font_style), font_color), ...
                                "backgroundcolor", [1 1 1]);
        endfunction
        
        // function for Plot button
        function uh_button = create_plot_button(uh_parent, font_size, position, text, tag, callback)
            uh_button = uicontrol(uh_parent, ...
                                    "style", "pushbutton", "fontsize", font_size, ...
                                    "backgroundcolor", [1 1 1], ...
                                    "position", position,...
                                    "relief", "raised", ...
                                    "string", text, ...
                                    "tag", tag,...
                                    "callback", callback);
        endfunction
        
        
    // </LOCAL FUNCTIONS>

    // <VARIABLES>
        global platform_props; // platform properties
            platform_props.p_slow_graphics = %t; // slow graphics
            platform_props.p_legend = %t; // platform supports legend?
            platform_props.p_titled_createBorder = %t; // platform supports titled createBorder?
            platform_props.p_createLayoutOptions = %t; // platform supports createLayoutOptions?
            platform_props.p_xstring = %t; // platform supports xstring?
            platform_props.p_latex = %t; // platform supports LaTeX?
            platform_props.p_colormap_plots = %t; // platform supports colormapped plots?
            platform_props.p_colorbar = %f; // platform supports colorbars? (%f if http://bugzilla.scilab.org/show_bug.cgi?id=14711 will be confirmed)
            platform_props.p_surf = %t; // platform supports surf plots?
    
        global interface_props; // interface properties
            interface_props.font_size = 14; // font size
            interface_props.font_size_frm_title = 15; // font size for frame title
            interface_props.font_size_axes = 3; // font size for axes
            interface_props.linestyle_fun = "r-"; // line style for function
            interface_props.linestyle_der = "g-"; // line style for derivative
            interface_props.rootfinder_print = %f; // allow rootfinder text messages? 
            

        // Figure size and parameters
            frm_w = 320; frm_h = 600; // control frame size
            plot_w = 600; plot_h = frm_h;	// plots zone size
            axes_w = frm_w + plot_w;	// axes (inner figure) width
            axes_h = frm_h;          	// axes (inner figure) height
            margin  = 5; // extra margin
    // </VARIABLES>
    
    // Font setup
    if exists("defaultFontProperties") then
        defaultFontProperties("Lato", "normal", interface_props.font_size, "normal", "pixels");
    end
    
    // Figure constuction    
    fh_mathieu_gui = figure();
        fh_mathieu_gui.figure_id = 100001;
        fh_mathieu_gui.figure_name = gettext("GUI for Mathieu Functions Toolbox");
        fh_mathieu_gui.background = -2; // white background
        fh_mathieu_gui.axes_size = [axes_w axes_h]; // axes (inner figure) size
        fh_mathieu_gui.tag = "mathieu_fig"; // tag
        fh_mathieu_gui.resize = "off"; // disable resize
        
        fh_mathieu_gui.toolbar_visible = "off"; // hide toolbar
        fh_mathieu_gui.menubar_visible = "off"; // hide menubar
    
    // Tabs creation
    uh_tabs = uicontrol(fh_mathieu_gui, "style", "tab", "fontsize", interface_props.font_size_frm_title, "tag" ,"tab", "Position", [0 0 axes_w axes_h]);
    if platform_props.p_surf then
        uh_tab4 = uicontrol(uh_tabs, "style", "frame", "fontsize", interface_props.font_size_frm_title, "tag", "tab_4", "string", "Elliptic membrane");
    end
        uh_tab3 = uicontrol(uh_tabs, "style", "frame", "fontsize", interface_props.font_size_frm_title, "tag", "tab_3", "string", "Radial Mathieu Functions");
        uh_tab2 = uicontrol(uh_tabs, "style", "frame", "fontsize", interface_props.font_size_frm_title, "tag", "tab_2", "string", "Angular Mathieu Functions");
        uh_tab1 = uicontrol(uh_tabs, "style", "frame", "fontsize", interface_props.font_size_frm_title, "tag", "tab_1", "string", "Elliptic and Cartesian coordinates");

        // temporary disable other tabs
     if platform_props.p_surf then
        set([uh_tab2 uh_tab3 uh_tab4], "Enable", "off");
     else
        set([uh_tab2 uh_tab3], "Enable", "off");
     end

    // Adjust sizes
    frm_h = frm_h  - uh_tab1.Position(2) - margin;
    plot_h = frm_h;

    // widgets
        button_wdth = round(frm_w/2);
        button_hght = 40;
        margin_bottom = 4*margin;
        margin_left = 2*margin;
        margin_right = 2*margin;
        
        plot_buttons_position = [round(button_wdth/2), margin_bottom, button_wdth, button_hght];

/// <TAB1>
    // Control frame creation for tab_1
    uh_frm_left_tab1 = create_control_frame(uh_tab1, frm_w, frm_h, interface_props.font_size_frm_title, "bold", "black");

    // GUI elements sizes
        sh_opts_tab1_mrg_bt = margin_bottom*2;
        sh_opts_frm_tab1_hght = 100;
        islns_opts_frm_tab1_hght = 130;
        ell_opts_frm_tab1_hght = 170;
        title_size = [100 20];

    // Left panel widgets
        // Show / Hide section
        options_frm_position = [ margin_left, sh_opts_tab1_mrg_bt + button_hght, frm_w - margin_left - margin_right, sh_opts_frm_tab1_hght];
        
        [uh_sh_opts_frm_lbl_tab1, uh_sh_opts_frm_tab1] = create_titled_frame(uh_frm_left_tab1, options_frm_position, title_size, "Show / Hide", interface_props.font_size_frm_title);
        if platform_props.p_createLayoutOptions then
            uh_sh_opts_frm_tab1.layout_options = createLayoutOptions("grid", [2, 2]);
            uh_sh_opts_frm_tab1.layout = "grid";
        end
            // Show Hyperbolas
            uh_sh_opts_frm_hprb_tab1 = uicontrol(uh_sh_opts_frm_tab1, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", "Hyperbolas", "tag", "tab1_show_hyperbolas","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // Show Ellipses
            uh_sh_opts_frm_ells_tab1 = uicontrol(uh_sh_opts_frm_tab1, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", "Ellipses", "tag", "tab1_show_ellipses","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // Show Annotations or Foci
            annot_string = "Annotations";
            if ~platform_props.p_xstring then
                annot_string = "Foci";
            end
            uh_sh_opts_frm_ant_tab1 = uicontrol(uh_sh_opts_frm_tab1, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", annot_string, "tag", "tab1_show_annotations","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // Show Ellipse
            uh_sh_opts_frm_ell_tab1 = uicontrol(uh_sh_opts_frm_tab1, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", "Ellipse", "tag", "tab1_show_ellipse","Callback","ui_disable_btn_by_chbxs(gcbo);");


        if ~platform_props.p_createLayoutOptions then
            uh_sh_opts_frm_chbs_tab1 = equally_spaced_elements([2, 2], options_frm_position, ...
                                                                            [uh_sh_opts_frm_ells_tab1 , uh_sh_opts_frm_hprb_tab1 ; ...
                                                                             uh_sh_opts_frm_ell_tab1  , uh_sh_opts_frm_ant_tab1 ]', margin);
        end


        // Isolines
        isolines_frm_position = [options_frm_position(1), options_frm_position(2) + margin_bottom + options_frm_position(4), options_frm_position(3), islns_opts_frm_tab1_hght];
        [uh_isolines_frm_lbl_tab1, uh_isolines_frm_tab1] = create_titled_frame(uh_frm_left_tab1, isolines_frm_position, title_size, "Isolines", interface_props.font_size_frm_title);
        
        if platform_props.p_createLayoutOptions then
            uh_isolines_frm_tab1.layout_options = createLayoutOptions("grid", [4, 1]);
            uh_isolines_frm_tab1.layout = "grid";
        end
        
            // Number of Hyperbolas
            hyperbolas_min = 1;
            hyperbolas_max = 10;
            hyperbolas_step = [1 1];
            hyperbolas_value = 4;

            hyperbolas_string = "Hyperbolas total: ";
            
            uh_isls_frm_hyp_sl_tab1 = uicontrol(uh_isolines_frm_tab1, "style", "slider", "Min", hyperbolas_min, "Max", hyperbolas_max, "Value", hyperbolas_value, "SliderStep", hyperbolas_step, "backgroundcolor", [1 1 1],...
                                                    "Tag", "tab1_hyperbolas_value", "Callback", "ui_slider_read(gcbo, %f);");
            uh_isls_frm_hyp_txt_tab1 = uicontrol(uh_isolines_frm_tab1, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "string", hyperbolas_string, "tag", "tab1_hyperbolas_text");
                uh_isls_frm_hyp_sl_tab1.UserData = struct('text_tag', uh_isls_frm_hyp_txt_tab1.tag,...
                                                          'text_string', hyperbolas_string);
                ui_slider_read(uh_isls_frm_hyp_sl_tab1, %f); // init hyperbolas text by slider
            
            // Number of Elipses
            ellipses_min = 1;
            ellipses_max = 16;
            ellipses_step = [1 1];
            ellipses_value = 10;
            
            ellipses_string = "Ellipses total: ";
            
            uh_isls_frm_ell_sl_tab1 = uicontrol(uh_isolines_frm_tab1, "style", "slider", "Min", ellipses_min, "Max", ellipses_max, "Value", ellipses_value, "SliderStep", ellipses_step, "backgroundcolor", [1 1 1],...
                                                     "Tag", "tab1_ellipses_value", "Callback", "ui_slider_read(gcbo, %f);");
            uh_isls_frm_ell_txt_tab1 = uicontrol(uh_isolines_frm_tab1, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "string", ellipses_string, "tag", "tab1_ellipses_text");
                uh_isls_frm_ell_sl_tab1.UserData = struct('text_tag', uh_isls_frm_ell_txt_tab1.tag,...
                                                          'text_string', ellipses_string);
                ui_slider_read(uh_isls_frm_ell_sl_tab1, %f); // init hyperbolas text by slider
        
        if ~platform_props.p_createLayoutOptions then
            uh_isls_frm_els_tab1 = equally_spaced_elements([4, 1], isolines_frm_position, ...
                                                                            [uh_isls_frm_hyp_sl_tab1;...
                                                                             uh_isls_frm_hyp_txt_tab1;...
                                                                             uh_isls_frm_ell_sl_tab1;...
                                                                             uh_isls_frm_ell_txt_tab1], margin);
        end
        
        // Ellipse parameters
        ellipse_frm_position = [isolines_frm_position(1), isolines_frm_position(2) + margin_bottom + isolines_frm_position(4), isolines_frm_position(3), ell_opts_frm_tab1_hght ];
        [uh_ellipse_frm_lbl_tab1, uh_ellipse_frm_tab1] = create_titled_frame(uh_frm_left_tab1, ellipse_frm_position, title_size, "Ellipse", interface_props.font_size_frm_title);

        if platform_props.p_createLayoutOptions then
            uh_ellipse_frm_tab1.layout_options = createLayoutOptions("grid", [5, 1]);
            uh_ellipse_frm_tab1.layout = "grid";
        end

            // Ellipse data (foci, xi, eccentricity)
                ellipse_data_string = "Foci: %2.3f. Xi: %1.3f. Eccentricity: %1.3f.";
            
                ui_ellipse_data_txt_tab1 = uicontrol(uh_ellipse_frm_tab1, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", ellipse_data_string, "tag", "tab1_ellipse_data_text");

            // Minor semi-axes
                minor_semiaxes_string = "Ellipse semi-minor axis (along y): ";
                minor_semiaxes_min = 1;
                minor_semiaxes_max = 20;
                minor_semiaxes_step = [1 1];
                minor_semiaxes_value = 4;
                
                // slider and text
                uh_ell_minor_sa_sl_tab1 = uicontrol(uh_ellipse_frm_tab1, "style", "slider", "Min", minor_semiaxes_min, "Max", minor_semiaxes_max, "Value", minor_semiaxes_value, "SliderStep", minor_semiaxes_step, "backgroundcolor", [1 1 1],...
                                                        "Tag", "tab1_ellipse_minor_sa_value", "Callback", "ui_slider_read(gcbo, %f); ui_ellipse_data_show(gcbo, %t);");
                uh_ell_minor_sa_txt_tab1 = uicontrol(uh_ellipse_frm_tab1, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", minor_semiaxes_string, "tag", "tab1_ellipse_minor_sa_text");
                    uh_ell_minor_sa_sl_tab1.UserData = struct('text_tag', uh_ell_minor_sa_txt_tab1.tag,...
                                                              'text_string', minor_semiaxes_string);
                    ui_slider_read(uh_ell_minor_sa_sl_tab1, %f); // init minor axes text by slider


            // Major semi-axes
                major_semiaxes_string = "Ellipse semi-major axis (along x): ";
                major_semiaxes_min = 1;
                major_semiaxes_max = 20;
                major_semiaxes_step = [1 1];
                major_semiaxes_value = 5;

                // slider and text
                uh_ell_major_sa_sl_tab1 = uicontrol(uh_ellipse_frm_tab1, "style", "slider", "Min", major_semiaxes_min, "Max", major_semiaxes_max, "Value", major_semiaxes_value, "SliderStep", major_semiaxes_step, "backgroundcolor", [1 1 1],...
                                                        "Tag", "tab1_ellipse_major_sa_value", "Callback", "ui_slider_read(gcbo, %f); ui_ellipse_data_show(gcbo, %t);");
                uh_ell_major_sa_txt_tab1 = uicontrol(uh_ellipse_frm_tab1, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", major_semiaxes_string, "tag", "tab1_ellipse_major_sa_text");
                    uh_ell_major_sa_sl_tab1.UserData = struct('text_tag', uh_ell_major_sa_txt_tab1.tag,...
                                                              'text_string', major_semiaxes_string);
                    ui_slider_read(uh_ell_major_sa_sl_tab1, %f); // init major axes text by slider
                    

        if ~platform_props.p_createLayoutOptions then
            uh_isls_frm_els_tab1 = equally_spaced_elements([5, 1], ellipse_frm_position - [0 0 0 margin*2], ...
                                                                            [ui_ellipse_data_txt_tab1;...
                                                                             uh_ell_minor_sa_sl_tab1;...
                                                                             uh_ell_minor_sa_txt_tab1;...
                                                                             uh_ell_major_sa_sl_tab1;...
                                                                             uh_ell_major_sa_txt_tab1], margin);
        end


        // Plot button
        uh_frm_left_plt_btn_tab1 = create_plot_button(uh_frm_left_tab1, interface_props.font_size_frm_title, plot_buttons_position,...
                                                             "Plot", "tab1_plot_button", "ui_plot_coordinates();");

                // for calculating ellipse parameters
                    // major semi-axes 
                        uh_ell_minor_sa_sl_tab1.UserData = struct('text_tag', uh_ell_minor_sa_txt_tab1.tag,...
                                                                  'text_string', minor_semiaxes_string,...
                                                                  'minor_sa_slider_tag', uh_ell_minor_sa_sl_tab1.tag,...
                                                                  'major_sa_slider_tag', uh_ell_major_sa_sl_tab1.tag,...
                                                                  'ellipse_data_text_tag', ui_ellipse_data_txt_tab1.tag,...
                                                                  'ellipse_data_text_string', ellipse_data_string,...
                                                                  'plot_button_tag', uh_frm_left_plt_btn_tab1.tag);
                    // minor semi-axes
                        uh_ell_major_sa_sl_tab1.UserData = struct('text_tag', uh_ell_major_sa_txt_tab1.tag,...
                                                                  'text_string', major_semiaxes_string,...
                                                                  'minor_sa_slider_tag', uh_ell_minor_sa_sl_tab1.tag,...
                                                                  'major_sa_slider_tag', uh_ell_major_sa_sl_tab1.tag,...
                                                                  'ellipse_data_text_tag', ui_ellipse_data_txt_tab1.tag,...
                                                                  'ellipse_data_text_string', ellipse_data_string,...
                                                                  'plot_button_tag', uh_frm_left_plt_btn_tab1.tag);
                    // set sliders 
                        ui_ellipse_data_show(uh_ell_major_sa_sl_tab1, %t);
                        ui_ellipse_data_show(uh_ell_minor_sa_sl_tab1 ,%t);
    
    //Plot 
    uh_frm_right_tab1 = uicontrol(uh_tab1, ...
                            "style", "frame", ...
                            "position",[frm_w + margin 0 plot_w - margin plot_h], ...
                            "backgroundcolor", [1 1 1]);

            // scan checkboxes
                uh_sh_opts_frm_hprb_tab1.UserData = struct('checkboxes_tags', [uh_sh_opts_frm_hprb_tab1.tag; uh_sh_opts_frm_ells_tab1.tag; uh_sh_opts_frm_ant_tab1.tag; uh_sh_opts_frm_ell_tab1.tag],...
                                                           'button_tag', uh_frm_left_plt_btn_tab1.tag);
                uh_sh_opts_frm_ells_tab1.UserData = struct('checkboxes_tags', uh_sh_opts_frm_hprb_tab1.UserData.checkboxes_tags,...
                                                           'button_tag', uh_sh_opts_frm_hprb_tab1.UserData.button_tag);
                uh_sh_opts_frm_ant_tab1.UserData = struct('checkboxes_tags', uh_sh_opts_frm_hprb_tab1.UserData.checkboxes_tags,...
                                                          'button_tag', uh_sh_opts_frm_hprb_tab1.UserData.button_tag);
                uh_sh_opts_frm_ell_tab1.UserData = struct('checkboxes_tags', uh_sh_opts_frm_hprb_tab1.UserData.checkboxes_tags,...
                                                          'button_tag', uh_sh_opts_frm_hprb_tab1.UserData.button_tag);

        // axes
            axes1_tab1 = newaxes(uh_frm_right_tab1);
        fh_mathieu_gui.immediate_drawing = "off";
            axes1_tab1.tag = "plot_tab1";
        fh_mathieu_gui.immediate_drawing = "on";

// </TAB1>

// <TAB2>
    set(uh_tab2, "Enable", "on");
    uh_frm_left_tab2 = create_control_frame(uh_tab2, frm_w, frm_h, interface_props.font_size_frm_title, "bold", "black");
    
    // Left panel widgets (from bottom to top)
        odd_amf_opts_mrg_bottom = margin_bottom*2;
        odd_amf_options_frm_hght = 160;
        even_amf_opts_frm_hght = 160;
        q_amf_options_frm_hght = 80;

        if 0 & platform_props.p_latex then //  see http://bugzilla.scilab.org/show_bug.cgi?id=14709
            odd_amf_frm_title = "$\text{Odd AMF}\ —\ se_m(z,q)$";
            even_amf_frm_title = "$\text{Even AMF}\ —\ ce_m(z,q)$";
        else
            odd_amf_frm_title = "Odd AMF — se_m(z,q)";
            even_amf_frm_title = "Even AMF — ce_m(z,q)";
        end
        q_amf_frm_title = "Parameter — q";

        // define text and colors
        amf_rmf_show_fun_text = "Show function";
        amf_rmf_show_fun_text_cr = [0 0 0];
        amf_rmf_show_der_text = "Show derivative";
        amf_rmf_show_der_text_cr = [0 0 0];
        if ~platform_props.p_legend then
            amf_rmf_show_fun_text = amf_rmf_show_fun_text + " (red)"; // !!! sync with interface_props.linestyle_fun
            amf_rmf_show_fun_text_cr = [1 0 0];
            amf_rmf_show_der_text = amf_rmf_show_der_text + " (green)"; // !!! sync with interface_props.linestyle_der
            amf_rmf_show_der_text_cr = [0 1 0];
        end

        // Odd AMF - se_m(q)
        odd_amf_options_frm_pos = [ margin_left, odd_amf_opts_mrg_bottom + button_hght, frm_w - margin_left - margin_right, odd_amf_options_frm_hght];
        [uh_odd_amf_os_f_ll_t2, uh_odd_amf_opts_fr_tab2] = create_titled_frame(uh_frm_left_tab2, odd_amf_options_frm_pos, [round(title_size(1)*1.8) title_size(2)], odd_amf_frm_title, interface_props.font_size_frm_title);
        
        if platform_props.p_createLayoutOptions then
            uh_odd_amf_opts_fr_tab2.layout_options = createLayoutOptions("grid", [5, 1]);
            uh_odd_amf_opts_fr_tab2.layout = "grid";
        end

            // derivative
            uh_odd_amf_show_der_tab2 = uicontrol(uh_odd_amf_opts_fr_tab2, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", amf_rmf_show_der_text, "ForegroundColor", amf_rmf_show_der_text_cr, "tag", "tab2_odd_amf_show_der","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // function
            uh_odd_amf_show_fun_tab2 = uicontrol(uh_odd_amf_opts_fr_tab2, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", amf_rmf_show_fun_text, "ForegroundColor", amf_rmf_show_fun_text_cr, "tag", "tab2_odd_amf_show_fun","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // b_m(q)
            amf_bmq_string = "b_m(q) = %1.8f";
            uh_odd_amf_b_val_txt_t2 = uicontrol(uh_odd_amf_opts_fr_tab2, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "ForegroundColor", [0 0 1], "string", amf_bmq_string, "tag", "tab2_odd_amf_b_value");
            
            // slider
            odd_amf_m_min = 1;
            odd_amf_m_max = 15;
            odd_amf_m_step = [1 2];
            odd_amf_m_value = 3;
            odd_amf_m_value_string = "Order m: ";
            
            uh_odd_amf_m_val_sl_t2 = uicontrol(uh_odd_amf_opts_fr_tab2, "style", "slider", "Min", odd_amf_m_min, "Max", odd_amf_m_max, "Value", odd_amf_m_value, "SliderStep", odd_amf_m_step, "backgroundcolor", [1 1 1],...
                                                    "Tag", "tab2_odd_amf_m_value", "Callback", "ui_slider_read(gcbo, %f); ui_ambm_data_show(gcbo);");
            uh_odd_amf_m_val_txt_t2 = uicontrol(uh_odd_amf_opts_fr_tab2, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "string", odd_amf_m_value_string, "tag", "tab2_odd_amf_m_value_text");
                uh_odd_amf_m_val_sl_t2.UserData = struct('text_tag', uh_odd_amf_m_val_txt_t2.tag,...
                                                         'text_string', odd_amf_m_value_string);
                ui_slider_read(uh_odd_amf_m_val_sl_t2, %f); // init odd amf m text by slider

        if ~platform_props.p_createLayoutOptions then
            uh_odd_amf_opts_f_els_t2 = equally_spaced_elements([5, 1], odd_amf_options_frm_pos - [0 0 0 margin*2], ...
                                                                            [uh_odd_amf_show_der_tab2;...
                                                                             uh_odd_amf_show_fun_tab2;...
                                                                             uh_odd_amf_b_val_txt_t2;...
                                                                             uh_odd_amf_m_val_sl_t2;...
                                                                             uh_odd_amf_m_val_txt_t2], margin);
        end

        // Even AMF - ce_m(q)
        even_amf_options_frm_pos = [ odd_amf_options_frm_pos(1), odd_amf_options_frm_pos(2) + margin_bottom + odd_amf_options_frm_pos(4), odd_amf_options_frm_pos(3), even_amf_opts_frm_hght ];
        [uh_even_amf_os_f_ll_tab2, uh_even_amf_opts_fr_tab2] = create_titled_frame(uh_frm_left_tab2, even_amf_options_frm_pos, [round(title_size(1)*1.8) title_size(2)], even_amf_frm_title, interface_props.font_size_frm_title);
        if platform_props.p_createLayoutOptions then
            uh_even_amf_opts_fr_tab2.layout_options = createLayoutOptions("grid", [5, 1]);
            uh_even_amf_opts_fr_tab2.layout = "grid";
        end

            // derivative
            uh_even_amf_show_der_t2 = uicontrol(uh_even_amf_opts_fr_tab2, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", amf_rmf_show_der_text, "ForegroundColor", amf_rmf_show_der_text_cr, "tag", "tab2_even_amf_show_der","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // function
            uh_even_amf_show_fun_t2 = uicontrol(uh_even_amf_opts_fr_tab2, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", amf_rmf_show_fun_text, "ForegroundColor", amf_rmf_show_fun_text_cr, "tag", "tab2_even_amf_show_fun","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // a_m(q)
            amf_amq_string = "a_m(q) = %1.8f";
            uh_even_amf_a_val_txt_t2 = uicontrol(uh_even_amf_opts_fr_tab2, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "ForegroundColor", [0 0 1], "string", amf_bmq_string, "tag", "tab2_even_amf_a_value");
            
            // slider
            even_amf_m_min = 0;
            even_amf_m_max = 15;
            even_amf_m_step = [1 2];
            even_amf_m_value = 3;
            even_amf_m_value_string = "Order m: ";
            
            uh_even_amf_m_val_sl_t2 = uicontrol(uh_even_amf_opts_fr_tab2, "style", "slider", "Min", even_amf_m_min, "Max", even_amf_m_max, "Value", even_amf_m_value, "SliderStep", even_amf_m_step, "backgroundcolor", [1 1 1],...
                                                    "Tag", "tab2_even_amf_m_value", "Callback", "ui_slider_read(gcbo, %f); ui_ambm_data_show(gcbo);");
            uh_even_amf_m_val_txt_t2 = uicontrol(uh_even_amf_opts_fr_tab2, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "string", even_amf_m_value_string, "tag", "tab2_even_amf_m_value_text");
                uh_even_amf_m_val_sl_t2.UserData = struct('text_tag', uh_even_amf_m_val_txt_t2.tag,...
                                                          'text_string', even_amf_m_value_string);
                ui_slider_read(uh_even_amf_m_val_sl_t2, %f); // init even amf m text by slider

        if ~platform_props.p_createLayoutOptions then
            uh_even_amf_os_f_els_t2 = equally_spaced_elements([5, 1], even_amf_options_frm_pos - [0 0 0 margin*2], ...
                                                                            [uh_even_amf_show_der_t2;...
                                                                             uh_even_amf_show_fun_t2;...
                                                                             uh_even_amf_a_val_txt_t2;...
                                                                             uh_even_amf_m_val_sl_t2;...
                                                                             uh_even_amf_m_val_txt_t2], margin);
        end
        
        // Value of q
        q_amf_options_frm_pos = [even_amf_options_frm_pos(1), even_amf_options_frm_pos(2) + margin_bottom + even_amf_options_frm_pos(4), even_amf_options_frm_pos(3), q_amf_options_frm_hght];
        [uh_q_amf_opts_frm_lbl_t2, uh_q_amf_opts_frm_tab2] = create_titled_frame(uh_frm_left_tab2, q_amf_options_frm_pos, [round(title_size(1)*1.3) title_size(2)], q_amf_frm_title, interface_props.font_size_frm_title);

        if platform_props.p_createLayoutOptions then
            uh_q_amf_opts_frm_tab2.layout_options = createLayoutOptions("grid", [2, 1]);
            uh_q_amf_opts_frm_tab2.layout = "grid";
        end
            // value of q slider and label
                q_amf_value_string = "Parameter q: ";
                q_amf_min = -25;
                q_amf_max = 25;
                q_amf_step = [1 2];
                q_amf_value = 1;

                uh_q_amf_value_sl_tab2 = uicontrol(uh_q_amf_opts_frm_tab2, "style", "slider", "Min", q_amf_min, "Max", q_amf_max, "Value", q_amf_value, "SliderStep", q_amf_step, "backgroundcolor", [1 1 1],...
                                                     "Tag", "tab2_q_amf_value", "Callback", "ui_slider_read(gcbo, %f); ui_ambm_data_show(gcbo);");
                uh_q_amf_value_txt_t2 = uicontrol(uh_q_amf_opts_frm_tab2, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", q_amf_value_string, "tag", "tab2_q_amf_value_text");
                    uh_q_amf_value_sl_tab2.UserData = struct('text_tag', uh_q_amf_value_txt_t2.tag,...
                                                             'text_string', q_amf_value_string);
                    ui_slider_read(uh_q_amf_value_sl_tab2, %f); // init q value text by slider

        if ~platform_props.p_createLayoutOptions then
            uh_q_amf_values_els_t2 = equally_spaced_elements([2, 1], q_amf_options_frm_pos - [0 0 0 margin*2], ...
                                                                            [uh_q_amf_value_sl_tab2;...
                                                                            uh_q_amf_value_txt_t2], margin)
        end

            // for calculating characteristic values

                    uh_q_amf_value_sl_tab2.UserData = struct('text_tag', uh_q_amf_value_txt_t2.tag,...
                                                             'text_string', q_amf_value_string,...
                                                             'q_value_tag', uh_q_amf_value_sl_tab2.tag,... // q slider
                                                             'm_odd_value_tag', uh_odd_amf_m_val_sl_t2.tag,... // odd
                                                             'bm_text_tag', uh_odd_amf_b_val_txt_t2.tag,...
                                                             'bm_text_string', amf_bmq_string,...
                                                             'm_even_value_tag', uh_even_amf_m_val_sl_t2.tag,... // even
                                                             'am_text_tag', uh_even_amf_a_val_txt_t2.tag,...
                                                             'am_text_string', amf_amq_string);

                // Odd - se_m(q), b_m(q)
                    // m slider
                    uh_odd_amf_m_val_sl_t2.UserData = struct('text_tag', uh_odd_amf_m_val_txt_t2.tag,...
                                                             'text_string', odd_amf_m_value_string,...
                                                             'm_odd_value_tag', uh_odd_amf_m_val_sl_t2.tag,...
                                                             'q_value_tag', uh_q_amf_value_sl_tab2.tag,...
                                                             'bm_text_tag', uh_odd_amf_b_val_txt_t2.tag,...
                                                             'bm_text_string', amf_bmq_string);

                // Even - ce_m(q), a_m(q)
                    // m slider
                    uh_even_amf_m_val_sl_t2.UserData = struct('text_tag', uh_even_amf_m_val_txt_t2.tag,...
                                                              'text_string', even_amf_m_value_string,...
                                                              'm_even_value_tag', uh_even_amf_m_val_sl_t2.tag,...
                                                              'q_value_tag', uh_q_amf_value_sl_tab2.tag,...
                                                              'am_text_tag', uh_even_amf_a_val_txt_t2.tag,...
                                                              'am_text_string', amf_amq_string);

                // set sliders
                    ui_ambm_data_show(uh_q_amf_value_sl_tab2);
                    ui_ambm_data_show(uh_odd_amf_m_val_sl_t2);
                    ui_ambm_data_show(uh_even_amf_m_val_sl_t2);

        // Plot button
        uh_frm_left_plt_btn_tab2 = create_plot_button(uh_frm_left_tab2, interface_props.font_size_frm_title, plot_buttons_position,...
                                                             "Plot", "tab2_plot_button", "ui_plot_amf();");

            // scan checkboxes
                // Even
                    uh_even_amf_show_fun_t2.UserData = struct('checkboxes_tags', [uh_even_amf_show_fun_t2.tag; uh_even_amf_show_der_t2.tag; uh_odd_amf_show_fun_tab2.tag; uh_odd_amf_show_der_tab2.tag],...
                                                              'button_tag', uh_frm_left_plt_btn_tab2.tag);
                    uh_even_amf_show_der_t2.UserData = struct('checkboxes_tags', uh_even_amf_show_fun_t2.UserData.checkboxes_tags,...
                                                              'button_tag', uh_even_amf_show_fun_t2.UserData.button_tag);

                // Odd function
                    uh_odd_amf_show_fun_tab2.UserData = struct('checkboxes_tags', [uh_odd_amf_show_fun_tab2.tag; uh_odd_amf_show_der_tab2.tag; uh_even_amf_show_fun_t2.tag; uh_even_amf_show_der_t2.tag],...
                                                               'button_tag', uh_frm_left_plt_btn_tab2.tag);
                    uh_odd_amf_show_der_tab2.UserData = struct('checkboxes_tags', uh_odd_amf_show_fun_tab2.UserData.checkboxes_tags,...
                                                               'button_tag', uh_odd_amf_show_fun_tab2.UserData.button_tag);



    //Plots
    uh_frm_right_tab2 = uicontrol(uh_tab2, ...
                            "style", "frame", ...
                            "position",[frm_w + margin 0 plot_w - margin plot_h], ...
                            "backgroundcolor", [1 1 1]);

    if platform_props.p_createLayoutOptions then
        uh_frm_right_tab2.layout_options = createLayoutOptions("grid", [2, 1]);
        uh_frm_right_tab2.layout = "grid";
    end


    uh_frm_right_axes1_tab2 = uicontrol(uh_frm_right_tab2, "style", "frame");
    uh_frm_right_axes2_tab2 = uicontrol(uh_frm_right_tab2, "style", "frame");
    
    if ~platform_props.p_createLayoutOptions then
        [tab2_elements] = equally_spaced_elements([2, 1], uh_frm_right_tab2.position, [uh_frm_right_axes1_tab2; uh_frm_right_axes2_tab2], margin);
    end
        // axes
            axes2_tab2 = newaxes(uh_frm_right_axes1_tab2);
            axes1_tab2 = newaxes(uh_frm_right_axes2_tab2);
        fh_mathieu_gui.immediate_drawing = "off";
            axes1_tab2.tag = "plot1_tab2";
            axes2_tab2.tag = "plot2_tab2";
        fh_mathieu_gui.immediate_drawing = "on";

// </TAB2>

// <TAB3>
    set(uh_tab3, "Enable", "on");
    uh_frm_left_tab3 = create_control_frame(uh_tab3, frm_w, frm_h, interface_props.font_size_frm_title, "bold", "black");
    
    // Left panel widgets (from bottom to top)
        odd_rmf_opts_mrg_bot = margin_bottom*2;
        odd_rmf_opts_frm_hght = 160;
        even_rmf_opts_fr_hght = 160;
        q_rmf_options_frm_hght = 80;
        
        odd_rmf_frm_title = "Odd RMF";
        even_rmf_frm_title = "Even RMF";
        q_rmf_frm_title = "Parameter — q";
        
        // Odd RMF
        odd_rmf_opts_frm_pos = [ margin_left, odd_rmf_opts_mrg_bot + button_hght, frm_w - margin_left - margin_right, odd_rmf_opts_frm_hght];
        [uh_odd_rmf_opts_f_lbl_t3, uh_odd_rmf_opts_frm_tab3] = create_titled_frame(uh_frm_left_tab3, odd_rmf_opts_frm_pos, title_size, odd_rmf_frm_title, interface_props.font_size_frm_title);
        
            if platform_props.p_createLayoutOptions then
                uh_odd_rmf_opts_frm_tab3.layout_options = createLayoutOptions("grid", [6, 1]);
                uh_odd_rmf_opts_frm_tab3.layout = "grid";
            end

            // derivative
            uh_odd_rmf_show_der_tab3 = uicontrol(uh_odd_rmf_opts_frm_tab3, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", amf_rmf_show_der_text, "ForegroundColor", amf_rmf_show_der_text_cr, "tag", "tab3_odd_rmf_show_der","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // function
            uh_odd_rmf_show_fun_tab3 = uicontrol(uh_odd_rmf_opts_frm_tab3, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", amf_rmf_show_fun_text, "ForegroundColor", amf_rmf_show_fun_text_cr, "tag", "tab3_odd_rmf_show_fun","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // b_m(q)
            rmf_bmq_string = "b_m(q) = %1.8f";
            uh_odd_rmf_b_val_txt_t3 = uicontrol(uh_odd_rmf_opts_frm_tab3, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "ForegroundColor", [0 0 1], "string", rmf_bmq_string, "tag", "tab3_odd_rmf_b_value");

            // slider
            odd_rmf_m_min = 1;
            odd_rmf_m_max = 15;
            odd_rmf_m_step = [1 2];
            odd_rmf_m_value = 3;
            odd_rmf_m_value_string = "Order m: ";
            
            uh_odd_rmf_m_value_sl_t3 = uicontrol(uh_odd_rmf_opts_frm_tab3, "style", "slider", "Min", odd_rmf_m_min, "Max", odd_rmf_m_max, "Value", odd_rmf_m_value, "SliderStep", odd_rmf_m_step, "backgroundcolor", [1 1 1],...
                                                    "Tag", "tab3_odd_rmf_m_value", "Callback", "ui_slider_read(gcbo, %f); ui_ambm_data_show(gcbo);");
            uh_odd_rmf_m_val_txt_t3 = uicontrol(uh_odd_rmf_opts_frm_tab3, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "string", odd_rmf_m_value_string, "tag", "tab3_odd_rmf_m_value_text");
                uh_odd_rmf_m_value_sl_t3.UserData = struct('text_tag', uh_odd_rmf_m_val_txt_t3.tag,...
                                                           'text_string', odd_rmf_m_value_string);
                ui_slider_read(uh_odd_rmf_m_value_sl_t3, %f); // init odd rmf m text by slider


            uh_odd_rmf_fun_types_t3 = uicontrol(uh_odd_rmf_opts_frm_tab3,"Style","frame","Border",[],"BackgroundColor",[1 1 1]);
                if platform_props.p_createLayoutOptions then
                    uh_odd_rmf_fun_types_t3.layout_options = createLayoutOptions("grid", [1, 5]);
                    uh_odd_rmf_fun_types_t3.layout = "grid";
                end

                uh_odd_rmf_t4 = uicontrol(uh_odd_rmf_fun_types_t3, "Style", "radiobutton", "groupname", "odd_rmf_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Gey", "tag", "tab3_odd_rmf_Gey");
                uh_odd_rmf_t3 = uicontrol(uh_odd_rmf_fun_types_t3, "Style", "radiobutton", "groupname", "odd_rmf_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Se", "tag", "tab3_odd_rmf_Se");
                uh_odd_rmf_t2 = uicontrol(uh_odd_rmf_fun_types_t3, "Style", "radiobutton", "groupname", "odd_rmf_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Ms2", "tag", "tab3_odd_rmf_Ms2");
                uh_odd_rmf_t1 = uicontrol(uh_odd_rmf_fun_types_t3, "Style", "radiobutton", "groupname", "odd_rmf_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Ms1", "tag", "tab3_odd_rmf_Ms1");
                set(uh_odd_rmf_t1,"Value",1);
                uh_odd_rmf_t0 = uicontrol(uh_odd_rmf_fun_types_t3, "Style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Type:");

            if ~platform_props.p_createLayoutOptions then
                uh_odd_rmf_fun_ts_els_t3 = equally_spaced_elements([1, 5], [odd_rmf_opts_frm_pos(1), round((4/5)*odd_rmf_opts_frm_pos(2)), odd_rmf_opts_frm_pos(3), round((1/4)*odd_rmf_opts_frm_pos(4)) ], ...
                                                                             [uh_odd_rmf_t0, uh_odd_rmf_t1, uh_odd_rmf_t2, uh_odd_rmf_t3, uh_odd_rmf_t4], margin);
            end

        if ~platform_props.p_createLayoutOptions then
            uh_odd_rmf_opts_f_els_t3 = equally_spaced_elements([6, 1], odd_rmf_opts_frm_pos - [0 0 0 margin*2], ...
                                                                            [uh_odd_rmf_show_der_tab3;...
                                                                             uh_odd_rmf_show_fun_tab3;...
                                                                             uh_odd_rmf_b_val_txt_t3;...
                                                                             uh_odd_rmf_m_value_sl_t3;...
                                                                             uh_odd_rmf_m_val_txt_t3;...
                                                                             uh_odd_rmf_fun_types_t3], margin);
        end

        // Even RMF
        even_rmf_opts_frm_pos = [ odd_rmf_opts_frm_pos(1), odd_rmf_opts_frm_pos(2) + margin_bottom + odd_rmf_opts_frm_pos(4), odd_rmf_opts_frm_pos(3), even_rmf_opts_fr_hght ];
        [uh_even_rmf_os_f_lbl_t3, uh_even_rmf_opts_frm_t3] = create_titled_frame(uh_frm_left_tab3, even_rmf_opts_frm_pos, title_size, even_rmf_frm_title, interface_props.font_size_frm_title);

            if platform_props.p_createLayoutOptions then
                uh_even_rmf_opts_frm_t3.layout_options = createLayoutOptions("grid", [6, 1]);
                uh_even_rmf_opts_frm_t3.layout = "grid";
            end

            // derivative
            uh_even_rmf_show_der_t3 = uicontrol(uh_even_rmf_opts_frm_t3, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", amf_rmf_show_der_text, "ForegroundColor", amf_rmf_show_der_text_cr, "tag", "tab3_even_rmf_show_der","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // function
            uh_even_rmf_show_fun_t3 = uicontrol(uh_even_rmf_opts_frm_t3, "style", "checkbox", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "value", 1, "string", amf_rmf_show_fun_text, "ForegroundColor", amf_rmf_show_fun_text_cr, "tag", "tab3_even_rmf_show_fun","Callback","ui_disable_btn_by_chbxs(gcbo);");

            // a_m(q)
            rmf_amq_string = "a_m(q) = %1.8f";
            uh_even_rmf_a_val_txt_t3 = uicontrol(uh_even_rmf_opts_frm_t3, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "ForegroundColor", [0 0 1], "string", rmf_bmq_string, "tag", "tab3_even_rmf_a_value");

            // slider
            even_rmf_m_min = 0;
            even_rmf_m_max = 15;
            even_rmf_m_step = [1 2];
            even_rmf_m_value = 3;
            even_rmf_m_value_string = "Order m: ";
            
            uh_even_rmf_m_val_sl_t3 = uicontrol(uh_even_rmf_opts_frm_t3, "style", "slider", "Min", even_rmf_m_min, "Max", even_rmf_m_max, "Value", even_rmf_m_value, "SliderStep", even_rmf_m_step, "backgroundcolor", [1 1 1],...
                                                    "Tag", "tab3_even_rmf_m_value", "Callback", "ui_slider_read(gcbo, %f); ui_ambm_data_show(gcbo);");
            uh_even_rmf_m_val_txt_t3 = uicontrol(uh_even_rmf_opts_frm_t3, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "string", even_rmf_m_value_string, "tag", "tab3_even_rmf_m_value_text");
                uh_even_rmf_m_val_sl_t3.UserData = struct('text_tag', uh_even_rmf_m_val_txt_t3.tag,...
                                                          'text_string', even_rmf_m_value_string);
                ui_slider_read(uh_even_rmf_m_val_sl_t3, %f); // init even rmf m text by slider


            uh_even_rmf_fun_ts_t3 = uicontrol(uh_even_rmf_opts_frm_t3,"Style","frame","Border",[],"BackgroundColor",[1 1 1]);
                if platform_props.p_createLayoutOptions then
                    uh_even_rmf_fun_ts_t3.layout_options = createLayoutOptions("grid", [1, 5]);
                    uh_even_rmf_fun_ts_t3.layout = "grid";
                end

                uh_even_rmf_t4 = uicontrol(uh_even_rmf_fun_ts_t3, "Style", "radiobutton", "groupname", "even_rmf_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Fey", "tag", "tab3_even_rmf_Fey");
                uh_even_rmf_t3 = uicontrol(uh_even_rmf_fun_ts_t3, "Style", "radiobutton", "groupname", "even_rmf_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Ce", "tag", "tab3_even_rmf_Ce");
                uh_even_rmf_t2 = uicontrol(uh_even_rmf_fun_ts_t3, "Style", "radiobutton", "groupname", "even_rmf_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Mc2", "tag", "tab3_even_rmf_Mc2");
                uh_even_rmf_t1 = uicontrol(uh_even_rmf_fun_ts_t3, "Style", "radiobutton", "groupname", "even_rmf_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Mc1", "tag", "tab3_even_rmf_Mc1");
                set(uh_even_rmf_t1,"Value",1);
                uh_even_rmf_t0 = uicontrol(uh_even_rmf_fun_ts_t3, "Style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Type:");

            if ~platform_props.p_createLayoutOptions then
                uh_even_rmf_fn_ts_els_t3 = equally_spaced_elements([1, 5], [even_rmf_opts_frm_pos(1), round((4/5)*even_rmf_opts_frm_pos(2)), even_rmf_opts_frm_pos(3), round((1/4)*even_rmf_opts_frm_pos(4)) ], ...
                                                                             [uh_even_rmf_t0, uh_even_rmf_t1, uh_even_rmf_t2, uh_even_rmf_t3, uh_even_rmf_t4], margin);
            end

        if ~platform_props.p_createLayoutOptions then
            uh_even_rmf_os_f_els_t3 = equally_spaced_elements([6, 1], even_rmf_opts_frm_pos - [0 0 0 margin*2], ...
                                                                            [uh_even_rmf_show_der_t3;...
                                                                             uh_even_rmf_show_fun_t3;...
                                                                             uh_even_rmf_a_val_txt_t3;...
                                                                             uh_even_rmf_m_val_sl_t3;...
                                                                             uh_even_rmf_m_val_txt_t3;...
                                                                             uh_even_rmf_fun_ts_t3], margin);
        end

        // Value of q
        q_rmf_options_frm_pos = [even_rmf_opts_frm_pos(1), even_rmf_opts_frm_pos(2) + margin_bottom + even_rmf_opts_frm_pos(4), even_rmf_opts_frm_pos(3), q_rmf_options_frm_hght];
        [uh_q_rmf_opts_frm_lbl_t3, uh_q_rmf_opts_frm_tab3] = create_titled_frame(uh_frm_left_tab3, q_rmf_options_frm_pos, [round(title_size(1)*1.3) title_size(2)], q_rmf_frm_title, interface_props.font_size_frm_title);

        if platform_props.p_createLayoutOptions then
            uh_q_rmf_opts_frm_tab3.layout_options = createLayoutOptions("grid", [2, 1]);
            uh_q_rmf_opts_frm_tab3.layout = "grid";
        end
            // value of q slider and label
                q_rmf_value_string = "Parameter q: ";
                q_rmf_min = 1;
                q_rmf_max = 11;
                q_rmf_step = [1 2];
                q_rmf_value = 5;

                uh_q_rmf_value_sl_tab3 = uicontrol(uh_q_rmf_opts_frm_tab3, "style", "slider", "Min", q_rmf_min, "Max", q_rmf_max, "Value", q_rmf_value, "SliderStep", q_rmf_step, "backgroundcolor", [1 1 1],...
                                                     "Tag", "tab3_q_rmf_value", "Callback", "ui_slider_read(gcbo, %f); ui_ambm_data_show(gcbo);");
                uh_q_rmf_value_txt_tab3 = uicontrol(uh_q_rmf_opts_frm_tab3, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", q_rmf_value_string, "tag", "tab3_q_rmf_value_text");
                    uh_q_rmf_value_sl_tab3.UserData = struct('text_tag', uh_q_rmf_value_txt_tab3.tag,...
                                                             'text_string', q_rmf_value_string);
                    ui_slider_read(uh_q_rmf_value_sl_tab3, %f); // init q value text by slider

        if ~platform_props.p_createLayoutOptions then
            uh_q_rmf_values_els_t3 = equally_spaced_elements([2, 1], q_rmf_options_frm_pos - [0 0 0 margin*2], ...
                                                                            [uh_q_rmf_value_sl_tab3;...
                                                                            uh_q_rmf_value_txt_tab3], margin)
        end

            // for calculating characteristic values
                // q slider
                    uh_q_rmf_value_sl_tab3.UserData = struct('text_tag', uh_q_rmf_value_txt_tab3.tag,...
                                                             'text_string', q_rmf_value_string,...
                                                             'q_value_tag', uh_q_rmf_value_sl_tab3.tag,...
                                                             'm_odd_value_tag', uh_odd_rmf_m_value_sl_t3.tag,... // odd
                                                             'bm_text_tag', uh_odd_rmf_b_val_txt_t3.tag,...
                                                             'bm_text_string', rmf_bmq_string,... // even
                                                             'm_even_value_tag', uh_even_rmf_m_val_sl_t3.tag,...
                                                             'am_text_tag', uh_even_rmf_a_val_txt_t3.tag,...
                                                             'am_text_string', rmf_amq_string);
                // Odd - se_m(q), b_m(q)
                    // m slider
                    uh_odd_rmf_m_value_sl_t3.UserData = struct('text_tag', uh_odd_rmf_m_val_txt_t3.tag,...
                                                               'text_string', odd_rmf_m_value_string,...
                                                               'm_odd_value_tag', uh_odd_rmf_m_value_sl_t3.tag,...
                                                               'q_value_tag', uh_q_rmf_value_sl_tab3.tag,...
                                                               'bm_text_tag', uh_odd_rmf_b_val_txt_t3.tag,...
                                                               'bm_text_string', rmf_bmq_string);
                // Even - ce_m(q), a_m(q)
                    // m slider
                    uh_even_rmf_m_val_sl_t3.UserData = struct('text_tag', uh_even_rmf_m_val_txt_t3.tag,...
                                                              'text_string', even_rmf_m_value_string,...
                                                              'm_even_value_tag', uh_even_rmf_m_val_sl_t3.tag,...
                                                              'q_value_tag', uh_q_rmf_value_sl_tab3.tag,...
                                                              'am_text_tag', uh_even_rmf_a_val_txt_t3.tag,...
                                                              'am_text_string', rmf_amq_string);

                // set sliders
                    ui_ambm_data_show(uh_q_rmf_value_sl_tab3);
                    ui_ambm_data_show(uh_odd_rmf_m_value_sl_t3);
                    ui_ambm_data_show(uh_even_rmf_m_val_sl_t3);
                    

        // Plot button
        uh_frm_left_plt_btn_tab3 = create_plot_button(uh_frm_left_tab3, interface_props.font_size_frm_title, plot_buttons_position,...
                                                             "Plot", "tab3_plot_button", "ui_plot_rmf();");

            // scan checkboxes
                // Even
                    uh_even_rmf_show_fun_t3.UserData = struct('checkboxes_tags', [uh_even_rmf_show_fun_t3.tag; uh_even_rmf_show_der_t3.tag; uh_odd_rmf_show_fun_tab3.tag; uh_odd_rmf_show_der_tab3.tag],...
                                                              'button_tag', uh_frm_left_plt_btn_tab3.tag);
                    uh_even_rmf_show_der_t3.UserData = struct('checkboxes_tags', uh_even_rmf_show_fun_t3.UserData.checkboxes_tags,...
                                                              'button_tag', uh_even_rmf_show_fun_t3.UserData.button_tag);
                // Odd function
                    uh_odd_rmf_show_fun_tab3.UserData = struct('checkboxes_tags', [uh_odd_rmf_show_fun_tab3.tag; uh_odd_rmf_show_der_tab3.tag; uh_even_rmf_show_fun_t3.tag; uh_even_rmf_show_der_t3.tag],...
                                                               'button_tag', uh_frm_left_plt_btn_tab3.tag);
                    uh_odd_rmf_show_der_tab3.UserData = struct('checkboxes_tags', uh_odd_rmf_show_fun_tab3.UserData.checkboxes_tags,...
                                                               'button_tag', uh_odd_rmf_show_fun_tab3.UserData.button_tag);

    //Plots
    uh_frm_right_tab3 = uicontrol(uh_tab3, ...
                            "style", "frame", ...
                            "position",[frm_w + margin 0 plot_w - margin plot_h], ...
                            "backgroundcolor", [1 1 1]);

    if platform_props.p_createLayoutOptions then
        uh_frm_right_tab3.layout_options = createLayoutOptions("grid", [2, 1]);
        uh_frm_right_tab3.layout = "grid";
    end


    uh_frm_right_axes1_tab3 = uicontrol(uh_frm_right_tab3, "style", "frame");
    uh_frm_right_axes2_tab3 = uicontrol(uh_frm_right_tab3, "style", "frame");
    
    if ~platform_props.p_createLayoutOptions then
        [tab3_elements] = equally_spaced_elements([2, 1], uh_frm_right_tab3.position, [uh_frm_right_axes1_tab3; uh_frm_right_axes2_tab3], margin);
    end
        // axes
            axes2_tab3 = newaxes(uh_frm_right_axes1_tab3);
            axes1_tab3 = newaxes(uh_frm_right_axes2_tab3);
        fh_mathieu_gui.immediate_drawing = "off";
            axes1_tab3.tag = "plot1_tab3";
            axes2_tab3.tag = "plot2_tab3";
        fh_mathieu_gui.immediate_drawing = "on";

// </TAB3>

if platform_props.p_surf
// <TAB4>
    set(uh_tab4, "Enable", "on");
    // Control frame creation for tab_4
    uh_frm_left_tab4 = create_control_frame(uh_tab4, frm_w, frm_h, interface_props.font_size_frm_title, "bold", "black");

    // GUI elements sizes
        mode_options_tab4_mrg_bm = margin_bottom*2;
        mode_options_frm_tab4_ht = 150;
        b_c_frm_tab4_hght = 80;
        membrane_opts_fr_tab4_ht = 170;

        // Mode selection
        mode_options_frm_pos = [margin_left, mode_options_tab4_mrg_bm + button_hght, frm_w - margin_left - margin_right, mode_options_frm_tab4_ht];
        
        [uh_mode_opts_frm_lbl_t4, uh_mode_options_frm_tab4] = create_titled_frame(uh_frm_left_tab4, mode_options_frm_pos, title_size, "Mode", interface_props.font_size_frm_title);

            if platform_props.p_createLayoutOptions then
                uh_mode_options_frm_tab4.layout_options = createLayoutOptions("grid", [5, 1]);
                uh_mode_options_frm_tab4.layout = "grid";
            end
            
            // Radial variations (n)
            radial_mode_min = 1;
            radial_mode_max = 10;
            radial_mode_step = [1 1];
            radial_mode_value = 2;
            radial_mode_value_string = "Radial variations (n): ";
            
            uh_radial_mode_val_sl_t4 = uicontrol(uh_mode_options_frm_tab4, "style", "slider", "Min", radial_mode_min, "Max", radial_mode_max, "Value", radial_mode_value, "SliderStep", radial_mode_step, "backgroundcolor", [1 1 1],...
                                                    "Tag", "tab4_radial_mode_value", "Callback", "ui_slider_read(gcbo, %f);");
            uh_radial_mode_vl_txt_t4 = uicontrol(uh_mode_options_frm_tab4, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "string", radial_mode_value_string, "tag", "tab4_radial_mode_value_text");
                uh_radial_mode_val_sl_t4.UserData = struct('text_tag', uh_radial_mode_vl_txt_t4.tag,...
                                                           'text_string', radial_mode_value_string);
                ui_slider_read(uh_radial_mode_val_sl_t4, %f); // init radial variation (m) by slider
                
            // Angular variations (m)
            angular_mode_min_even = 0;
            angular_mode_min_odd = 1;
            angular_mode_min = min([ angular_mode_min_odd angular_mode_min_even ]);
            angular_mode_max = 30;
            angular_mode_step = [1 1];
            angular_mode_value = 1;
            angular_mode_value_str = "Angular variations (m): ";
            
            uh_ang_mode_val_sl_tab4 = uicontrol(uh_mode_options_frm_tab4, "style", "slider", "Min", angular_mode_min, "Max", angular_mode_max, "Value", angular_mode_value, "SliderStep", angular_mode_step, "backgroundcolor", [1 1 1],...
                                                    "Tag", "tab4_angular_mode_value", "Callback", "ui_slider_read(gcbo, %f);");
            uh_ang_mode_val_txt_tab4 = uicontrol(uh_mode_options_frm_tab4, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "string", angular_mode_value_str, "tag", "tab4_angular_mode_value_text");
                uh_ang_mode_val_sl_tab4.UserData = struct('text_tag', uh_ang_mode_val_txt_tab4.tag,...
                                                          'text_string', angular_mode_value_str);
                ui_slider_read(uh_ang_mode_val_sl_tab4, %f); // init angular variation (n) by slider
                
            // Mode type
            uh_mode_types_tab4 = uicontrol(uh_mode_options_frm_tab4,"Style","frame","Border",[],"BackgroundColor",[1 1 1]);
                if platform_props.p_createLayoutOptions then
                    uh_mode_types_tab4.layout_options = createLayoutOptions("grid", [1, 3]);
                    uh_mode_types_tab4.layout = "grid";
                end
                
                uh_mode_t2 = uicontrol(uh_mode_types_tab4, "Style", "radiobutton", "groupname", "mode_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1],...
                                        "String", "Odd", "tag", "tab4_odd_mode", "Callback", "ui_change_mode_limits(gcbo);");
                uh_mode_t1 = uicontrol(uh_mode_types_tab4, "Style", "radiobutton", "groupname", "mode_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1],...
                                        "String", "Even", "tag", "tab4_even_mode", "Callback", "ui_change_mode_limits(gcbo);");
                set(uh_mode_t1,"Value",1);
                uh_mode_t0 = uicontrol(uh_mode_types_tab4, "Style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Type:");

                // Even modes may have m=0, but odd modes start from m=1
                // initialize variables for ui_change_mode_limits function
                    uh_mode_t1.UserData = struct('even_ang_var_min', angular_mode_min_even,...
                                                 'odd_ang_var_min', angular_mode_min_odd,...
                                                 'uh_slider', uh_ang_mode_val_sl_tab4);
                    
                    uh_mode_t2.UserData = struct('even_ang_var_min', angular_mode_min_even,...
                                                 'odd_ang_var_min', angular_mode_min_odd,...
                                                 'uh_slider', uh_ang_mode_val_sl_tab4);
                
            if ~platform_props.p_createLayoutOptions then
                uh_mode_types_els_t4 = equally_spaced_elements([1, 3], [mode_options_frm_pos(1), round((3/4)*mode_options_frm_pos(2)), mode_options_frm_pos(3), round((1/4)*mode_options_frm_pos(4)) ], ...
                                                                         [uh_mode_t0, uh_mode_t1, uh_mode_t2], margin);
            end

        if ~platform_props.p_createLayoutOptions then
            uh_mode_opts_frm_els_t4 = equally_spaced_elements([5, 1], odd_rmf_opts_frm_pos - [0 0 0 margin*2], ...
                                                                            [uh_radial_mode_val_sl_t4;...
                                                                             uh_radial_mode_vl_txt_t4;...
                                                                             uh_ang_mode_val_sl_tab4;...
                                                                             uh_ang_mode_val_txt_tab4;...
                                                                             uh_mode_types_tab4], margin);
        end

        // Boundary conditions
        b_cs_frm_position = [mode_options_frm_pos(1), mode_options_frm_pos(2) + margin_bottom + mode_options_frm_pos(4), mode_options_frm_pos(3), b_c_frm_tab4_hght];
        
        [uh_b_cs_frm_lbl_tab4, uh_b_cs_frm_tab4] = create_titled_frame(uh_frm_left_tab4, b_cs_frm_position, [title_size(1)*1.5 title_size(2)] , "Boundary condition", interface_props.font_size_frm_title);

            if platform_props.p_createLayoutOptions then
                uh_b_cs_frm_tab4.layout_options = createLayoutOptions("grid", [1, 2]);
                uh_b_cs_frm_tab4.layout = "grid";
            end

                uh_bc_t2 = uicontrol(uh_b_cs_frm_tab4, "Style", "radiobutton", "groupname", "bc_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Neumann (hard)", "tag", "tab4_bc_neumann");            
                uh_bc_t1 = uicontrol(uh_b_cs_frm_tab4, "Style", "radiobutton", "groupname", "bc_types", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", "Dirichlet (soft)", "tag", "tab4_bc_dirichlet");
                set(uh_bc_t1,"Value",1);                

            if ~platform_props.p_createLayoutOptions then
                uh_bc_types_els_tab4 = equally_spaced_elements([1, 2], b_cs_frm_position - [0 0 0 margin*2], [uh_bc_t1, uh_bc_t2], margin);
            end

        // Membrane geometry
        membr_opts_frm_pos = [b_cs_frm_position(1), b_cs_frm_position(2) + margin_bottom + b_cs_frm_position(4), b_cs_frm_position(3), membrane_opts_fr_tab4_ht ];
        [uh_membr_opts_frm_lbl_t4, uh_membr_opts_frm_t4] = create_titled_frame(uh_frm_left_tab4, membr_opts_frm_pos, [title_size(1)*1.5 title_size(2)], "Elliptic membrane", interface_props.font_size_frm_title);

        if platform_props.p_createLayoutOptions then
            uh_membr_opts_frm_t4.layout_options = createLayoutOptions("grid", [5, 1]);
            uh_membr_opts_frm_t4.layout = "grid";
        end

            // membrane data (foci, xi, eccentricity)
                membr_data_string = "Foci: %2.3f. Xi: %1.3f. Eccentricity: %1.3f.";
            
                ui_membr_data_txt_t4 = uicontrol(uh_membr_opts_frm_t4, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", membr_data_string, "tag", "tab4_membrane_data_text");

            // Minor semi-axes
                minor_semiaxes_string = "Membrane semi-minor axis (along y), cm: %2.1f";
                minor_semiaxes_min = 0.5;
                minor_semiaxes_max = 5;
                minor_semiaxes_step = [0.1 0.5];
                minor_semiaxes_value = 3;
                
                // slider and text
                uh_membr_minor_sa_sl_t4 = uicontrol(uh_membr_opts_frm_t4, "style", "slider", "Min", minor_semiaxes_min, "Max", minor_semiaxes_max, "Value", minor_semiaxes_value, "SliderStep", minor_semiaxes_step, "backgroundcolor", [1 1 1],...
                                                        "Tag", "tab4_membrane_minor_sa_value", "Callback", "ui_slider_read(gcbo, %t); ui_ellipse_data_show(gcbo, %f);");
                uh_membr_minor_sa_txt_t4 = uicontrol(uh_membr_opts_frm_t4, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", minor_semiaxes_string, "tag", "tab4_membrane_minor_sa_text");
                    uh_membr_minor_sa_sl_t4.UserData = struct('text_tag', uh_membr_minor_sa_txt_t4.tag,...
                                                              'text_string', minor_semiaxes_string);
                    ui_slider_read(uh_membr_minor_sa_sl_t4, %t); // init minor axes text by slider


            // Major semi-axes
                major_semiaxes_string = "Membrane semi-major axis (along x), cm: %2.1f";
                major_semiaxes_min = 1;
                major_semiaxes_max = 5;
                major_semiaxes_step = [0.1 0.5];
                major_semiaxes_value = 5;

                // slider and text
                uh_membr_major_sa_sl_t4 = uicontrol(uh_membr_opts_frm_t4, "style", "slider", "Min", major_semiaxes_min, "Max", major_semiaxes_max, "Value", major_semiaxes_value, "SliderStep", major_semiaxes_step, "backgroundcolor", [1 1 1],...
                                                        "Tag", "tab4_membrane_major_sa_value", "Callback", "ui_slider_read(gcbo, %t); ui_ellipse_data_show(gcbo, %f);");
                uh_membr_major_sa_txt_t4 = uicontrol(uh_membr_opts_frm_t4, "style", "text", "fontsize", interface_props.font_size, "backgroundcolor", [1 1 1], "String", major_semiaxes_string, "tag", "tab4_membrane_major_sa_text");
                    uh_membr_major_sa_sl_t4.UserData = struct('text_tag', uh_membr_major_sa_txt_t4.tag,...
                                                              'text_string', major_semiaxes_string);
                    ui_slider_read(uh_membr_major_sa_sl_t4, %t); // init major axes text by slider
                    

        if ~platform_props.p_createLayoutOptions then
            uh_isolines_frm_els_t4 = equally_spaced_elements([5, 1], membr_opts_frm_pos - [0 0 0 margin*2], ...
                                                                            [ui_membr_data_txt_t4;...
                                                                             uh_membr_minor_sa_sl_t4;...
                                                                             uh_membr_minor_sa_txt_t4;...
                                                                             uh_membr_major_sa_sl_t4;...
                                                                             uh_membr_major_sa_txt_t4], margin);
        end


        // Plot button
        uh_frm_left_plt_btn_tab4 = create_plot_button(uh_frm_left_tab4, interface_props.font_size_frm_title, plot_buttons_position,...
                                                             "Plot", "tab4_plot_button", "ui_plot_membrane_mode();");

            // for calculating membrane parameters
                // major semi-axes 
                    uh_membr_minor_sa_sl_t4.UserData = struct('text_tag', uh_membr_minor_sa_txt_t4.tag,...
                                                              'text_string', minor_semiaxes_string,...
                                                              'minor_sa_slider_tag', uh_membr_minor_sa_sl_t4.tag,...
                                                              'major_sa_slider_tag', uh_membr_major_sa_sl_t4.tag,...
                                                              'ellipse_data_text_tag', ui_membr_data_txt_t4.tag,...
                                                              'ellipse_data_text_string', membr_data_string,...
                                                              'plot_button_tag', uh_frm_left_plt_btn_tab4.tag);
                // minor semi-axes
                    uh_membr_major_sa_sl_t4.UserData = struct('text_tag', uh_membr_major_sa_txt_t4.tag,...
                                                              'text_string', major_semiaxes_string,...
                                                              'minor_sa_slider_tag', uh_membr_minor_sa_sl_t4.tag,...
                                                              'major_sa_slider_tag', uh_membr_major_sa_sl_t4.tag,...
                                                              'ellipse_data_text_tag', ui_membr_data_txt_t4.tag,...
                                                              'ellipse_data_text_string', membr_data_string,...
                                                              'plot_button_tag', uh_frm_left_plt_btn_tab4.tag);
                // set sliders 
                    ui_ellipse_data_show(uh_membr_major_sa_sl_t4, %f);
                    ui_ellipse_data_show(uh_membr_minor_sa_sl_t4, %f);

    // Plot and mode table
    uh_frm_right_tab4 = uicontrol(uh_tab4, ...
                            "style", "frame", ...
                            "position",[frm_w + margin 0 plot_w - margin plot_h], ...
                            "backgroundcolor", [1 1 1]);

        // mode table
        mode_table_frm_position = [0, 0, uh_frm_right_tab4.position(3) - 2*margin, round(0.2*plot_h) - margin ];
        [uh_mode_table_frm_lbl_t4, uh_mode_table_frm_tab4] = create_titled_frame(uh_frm_right_tab4, mode_table_frm_position, [round(title_size(1)*1.8) title_size(2)], "Properties of mode", interface_props.font_size_frm_title);
        set(uh_mode_table_frm_tab4, "tag", "mode_table_frm_tab4");

        uh_mode_table_tab4 = uicontrol(uh_mode_table_frm_tab4, "style", "table", "fontsize", interface_props.font_size, "Position", [2*margin, 2*margin, uh_mode_table_frm_tab4.position(3) - 4*margin, uh_mode_table_frm_tab4.position(4) - title_size(2) - 2*margin], "tag", "mode_table_tab4");
        set(uh_mode_table_frm_tab4, "Visible", "off");
        uh_mode_table_tab4.UserData = struct('mode_table_header', ["","m","n","q","ka","ws, rad/s"]);

        // plot
        uh_frm_rigth_axes_tab4 = uicontrol(uh_frm_right_tab4, "Style", "frame", "BackgroundColor", [1 1 1], "Position", [mode_table_frm_position(1) + margin, mode_table_frm_position(4) + margin, mode_table_frm_position(3)  plot_h - mode_table_frm_position(4) - margin]);
        axes1_tab4 = newaxes(uh_frm_rigth_axes_tab4);

        fh_mathieu_gui.immediate_drawing = "off";
            axes1_tab4.tag = "plot_tab4";
        fh_mathieu_gui.immediate_drawing = "on";


// </TAB4>
end

    // center figure
    s_wh = get(0, 'screensize_px');
    fh_mathieu_gui.figure_position = [s_wh(3) - fh_mathieu_gui.figure_size(1), s_wh(4) - fh_mathieu_gui.figure_size(2)]/2;

endfunction
function ui_ambm_data_show(uh)
// ui_ambm_data_show - calculate and show characteristic values a_m(q) or b_m(q) with dependence on m and q
//
// input variables:
//      uh - handle to the slider with UserData
//          uh.UserData.q_value_tag - tag of 'q' slider
//
//          uh.UserData.am_text_tag - tag of 'a_m(q)' text label
//          uh.UserData.am_text_string - text string with format for 'a_m(q)' text label
//          uh.UserData.m_even_value_tag - tag of even order 'm' slider
//
//          uh.UserData.bm_text_tag - tag of 'b_m(q)' text label
//          uh.UserData.bm_text_string - text string with format for 'b_m(q)' text label
//          uh.UserData.m_odd_value_tag - tag of odd order 'm' slider
//
// get elements:
//          uh.UserData.m_even_value_tag.Value - value of even order 'm' slider
//          uh.UserData.m_odd_value_tag.Value - value of odd order 'm' slider
//
// set elements:
//          uh.UserData.am_text_tag.String - string inside 'a_m(q)' text label
//          uh.UserData.bm_text_tag.String - string inside 'b_m(q)' text label
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//
    ud = uh.UserData; // shortcut for UserData

    q = round(get(ud.q_value_tag,"value")); // read 'q' slider

    // calc a_m(q) and show in text label
    if (isfield(ud,'am_text_tag') & isfield(ud,'am_text_string') & isfield(ud,'m_even_value_tag')) then
        m_even = round(get(ud.m_even_value_tag,"value"));
        [ Arm, am ] = mathieu_Arm( m_even, q );
        set(ud.am_text_tag, "String", sprintf(ud.am_text_string, am));
    end

    // calc b_m(q) and show in text label
    if (isfield(ud,'bm_text_tag') & isfield(ud,'bm_text_string') & isfield(ud,'m_odd_value_tag')) then
        m_odd = round(get(ud.m_odd_value_tag,"value"));
        [ Brm, bm ] = mathieu_Brm( m_odd, q );
        set(ud.bm_text_tag, "String", sprintf(ud.bm_text_string, bm));
    end

endfunction
function ui_change_mode_limits(uh_radio)
// ui_change_mode_limits - read slider to text field
//
// input variables:
//      uh_radio - handle to mode radiobutton with UserData
//          uh_radio.UserData.even_ang_var_min - minimal number of angular variations for even modes
//          uh_radio.UserData.odd_ang_var_min - minimal number of angular variations for odd modes
//          uh_radio.UserData.uh_slider - handle of angular variations slider
//
// get elements:
//      uh_radio.Value - value of radiobutton
//      uh_radio.String - label of radiobutton
//      uh_radio.UserData.uh_slider.Value - slider value
//
// set elements:
//      uh_radio.UserData.uh_slider.Value - slider value
//      uh_radio.UserData.uh_slider.Min - slider Min
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//
    ud = uh_radio.UserData; // shorcut for UserData

    if uh_radio.Value == 1 // is radio checked?
        select uh_radio.String // string in radio
            case "Even" then
                m_min = ud.even_ang_var_min;
            case "Odd" then
                m_min = ud.odd_ang_var_min;
        end

        uh_slider = ud.uh_slider; // get angluar variations slider

        old_value = uh_slider.Value; // save old value

        uh_slider.Min = m_min; // set new minimal value

        if uh_slider.Value <= m_min // move slider if it was set < min
            uh_slider.Value = m_min;
        else // or restore slider position
            uh_slider.Value = old_value;
        end

        // update slider's text label
        ui_slider_read(uh_slider, %f);
    end
endfunction
function ui_disable_btn_by_chbxs(uh)
// ui_disable_btn_by_chbxs - disable button by checkboxes
//
// input variables:
//      uh - handle of checkbox with UserData
//          uh.UserData.checkboxes_tags - array of checkboxes tags
//          uh.UserData.button_tag - tag button hich should be enabled/disabled
//
// get elements:
//          uh.UserData.checkboxes_tags(i).Value - state of checkbox(es)
//
// set elements:
//          uh.UserData.button_tag.Enable - state of Plot button
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//
    ud = uh.UserData; // shortcut to UserData

    checkboxes_tags = ud.checkboxes_tags; // array of checkboxes tags
    button_tag = ud.button_tag; // button tag
    
    status = %f;
    sz = size(checkboxes_tags)
    for i=1:sz(1)
        status = status | ( get(checkboxes_tags(i), "Value") == 1 );
    end

    if or(status) then
        set(button_tag, "Enable", "on");
    else
        set(button_tag, "Enable", "off");
    end
endfunction
function ui_ellipse_data_show(uh, allow_round)
// ui_ellipse_data_show - show ellipse data based on semi-axes sliders and enable/disable Plot button
//
// input variables:
//      uh - handle to ellipse axes slider with UserData
//          uh.UserData.major_sa_slider_tag - tag of major semi-axes 'a' slider
//          uh.UserData.minor_sa_slider_tag - tag of minor semi-axes 'b' slider
//          uh.UserData.ellipse_data_text_tag - tag of ellipse data parameters text label
//          uh.UserData.ellipse_data_text_string - text string with format for ellipse data parameters text label
//          uh.UserData.plot_button_tag - tag of Plot button
//      allow_round - allow a=b? (boolean)
//
// get elements:
//          uh.UserData.major_sa_slider_tag.Value - value of major semi-axes slider
//          uh.UserData.minor_sa_slider_tag.Value - value of minor semi-axes slider
//
// set elements:
//          uh.UserData.am_text_tag.String - string inside 'a_m(q)' text label
//          uh.UserData.bm_text_tag.String - string inside 'b_m(q)' text label
//          uh.UserData.plot_button_tag.Enable - state of Plot button
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//
    ud = uh.UserData; // shorcut for UserData
    
    a = round(get(ud.major_sa_slider_tag,"value")*10)/10;
    b = round(get(ud.minor_sa_slider_tag,"value")*10)/10;
    
    f = sqrt(a^2 - b^2);
    e = f / a;
    xi = real(acosh(a/f));


    if ((a >= b) & allow_round) | ( (a > b) & ~allow_round )  then
        set(ud.ellipse_data_text_tag, "String", sprintf(ud.ellipse_data_text_string, f, xi, e), "ForegroundColor", [0 0 1]);
        set(ud.plot_button_tag, "Enable", "on");
    else
        set(ud.ellipse_data_text_tag, "String", "Invalid ellipse! Change semi axes.", "ForegroundColor", [1 0 0]);
        set(ud.plot_button_tag, "Enable", "off");
    end

endfunction
function ui_plot_amf()
// ui_plot_amf - plot angular Mathieu functions
//
// get elements:
//      "tab2_odd_amf_m_value".Value - tag of odd order 'm' slider value
//      "tab2_even_amf_m_value".Value - tag of even order 'm' slider value
//      "tab2_q_amf_value".Value" - tag of 'q' slider value
//
//      "tab2_even_amf_show_fun" - tag of show even function checkbox
//      "tab2_odd_amf_show_fun" - tag of show odd function checkbox
//      "tab2_even_amf_show_der" - tag of show even derivative checkbox
//      "tab2_odd_amf_show_der" - tag of show odd derivarive checkbox
//      "plot1_tab2" - tag of top tab2 axes
//      "plot2_tab2" - tag of bottom tab2 axes
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//
    global platform_props; // platform properties
        p_slow_graphics = platform_props.p_slow_graphics; // slow graphics

    // input parameters
        // m
        m_odd = round(get("tab2_odd_amf_m_value","value"));
        m_even = round(get("tab2_even_amf_m_value","value"));
        // q
        q = round(get("tab2_q_amf_value","value"));

    // read controls
        // show function?
        show_even_fun = %t;
        show_odd_fun = %t;
        if get("tab2_even_amf_show_fun","value") == 0 then
            show_even_fun = %f;
        end
        if get("tab2_odd_amf_show_fun","value") == 0 then
            show_odd_fun = %f;
        end

        // show derivative?
        show_even_der = %t;
        show_odd_der = %t;
        if get("tab2_even_amf_show_der","value") == 0 then
            show_even_der = %f;
        end
        if get("tab2_odd_amf_show_der","value") == 0 then
            show_odd_der = %f;
        end

    // set plot parameters
    z = linspace(0, 2*%pi, 201);

    plot_parameters.legend_pos = -6;

    // slow graphics mode
    if p_slow_graphics then
        drawlater();
    end

    plot_parameters.ax = get("plot1_tab2");    
    calculate_and_show_plot('ce', m_even, q, z, show_even_fun, show_even_der, plot_parameters);

    plot_parameters.ax = get("plot2_tab2");
    calculate_and_show_plot('se', m_odd, q, z, show_odd_fun, show_odd_der, plot_parameters);

    // slow graphics mode
    if p_slow_graphics then
        drawnow();
    end

endfunction
function ui_plot_coordinates()
// ui_plot_coordinates - function for plotting ellipse with curves for Scilab MFT.
// Based on Mathieu ATOMS's 'demos/ell_cart.sce'
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//

    global platform_props; // platform properties
        p_slow_graphics = platform_props.p_slow_graphics; // slow graphics
        p_legend = platform_props.p_legend; // legend support
        p_xstring = platform_props.p_xstring; // xstring support
        p_latex = platform_props.p_latex; // LaTeX support
        p_colormap_plots = platform_props.p_colormap_plots; // colormap plots support

    global interface_props; // interface properties
        font_size_axes = interface_props.font_size_axes; // font size for axes

    // parameters of ellipse
        // input parameters
        a = round(get("tab1_ellipse_major_sa_value","value"));
        b = round(get("tab1_ellipse_minor_sa_value","value"));

        // additional parameters
        etas_length = 4 * round(get("tab1_hyperbolas_value","value"));
        xi_length = round(get("tab1_ellipses_value","value"));

        // output parameters
        f = sqrt(a^2 - b^2);
        xi = real(acosh(a/f));
        
        xi_length_fine = 11; // xis for fine hyperbolas plots
        etas_length_fine = 25; // etas for fine ellipse plots

    // line styles
        // ellipse
        linestyle_ellipse = 'b-';

        if p_colormap_plots then // colormap plots support
            linestyle_hyperbolas = '-.'; // hyperbolas
            linestyle_ellipses = '--'; // ellipses
        else
            linestyle_hyperbolas = 'r-.'; // hyperbolas
            linestyle_ellipses = 'g--'; // ellipses
        end

    // read controls
        // show ellipse?
        show_ellipse = %t;
        if get("tab1_show_ellipse","value") == 0 then
            show_ellipse = %f;
        end
        
        // show annotations?
        show_annotations = %t;
        if get("tab1_show_annotations","value") == 0 then
            show_annotations = %f;
        end
        
        // show hyperbolas?
        show_hyperbolas = %t;
        if get("tab1_show_hyperbolas","value") == 0 then
            show_hyperbolas = %f;
        end
        
        // show ellipses?
        show_ellipses = %t;
        if get("tab1_show_ellipses","value") == 0 then
            show_ellipses = %f;
        end


    // calculate ellipse
    eta = linspace(0, 2*%pi, 201);

    // circle or ellipse?
    is_circle = %f;
    if a - b > %eps  then
        [x_el, y_el] = mathieu_ell2cart(xi, eta, f);
    else
        rho = a;
        [x_el, y_el] = mathieu_pol2cart(eta, rho);
        is_circle = %t;
        xi = rho;
    end

    // parameters for plots
        xi_max = ceil(1.5*xi);
        etas_marks_mod = 1;
        if etas_length > 20 then
            etas_marks_mod = 2;
        end
        xi_etas_marks = xi_max*1.06;

    // slow graphics mode
    if p_slow_graphics then
        drawlater();
    end
    
        ax = get("plot_tab1");
        delete(ax.children);
        sca(ax);
        ax.zoom_box = []; // reset zoom
        ax.visible = 'off';

    // plot objects if any
    if show_ellipse | show_hyperbolas | show_annotations | show_ellipses then
    
            ax.visible = 'on';
            ax.auto_clear = "off";
            ax.data_bounds = [-1 1 -1 1];
            ax.auto_scale = "on";

            // plot ellipse
            if show_ellipse then
                plot(x_el, y_el, linestyle_ellipse);
            end
    
            ax.isoview = 'on';
                    
            ax.title.font_size = font_size_axes+1;
    
            if p_latex then // LaTeX support
                ax.title.text = '$\text{Elliptic and Cartesian coordinates}$';
                ltx = '$';
            else
                ax.title.text = "Elliptic and Cartesian coordinates";
                ltx = '';
            end
    
            ax.font_size = font_size_axes-1;
            xlabel(ltx + 'x' + ltx);
            ylabel(ltx + 'y' + ltx);
            ax.x_label.font_size = font_size_axes; 
            ax.y_label.font_size = font_size_axes;
    
        // plot other things
        if show_hyperbolas then // show hyperbolas?
            // constant angle curves (eta=const, xi=var)
                // divide in half-planes - top
                etas_top = linspace(0, %pi, etas_length/2 + 2);
                etas_top(1) = [];
                etas_top($) = [];
                // and bottom
                etas_bottom = linspace(%pi, 2*%pi, etas_length/2 + 2);
                etas_bottom(1) = [];
                etas_bottom($) = [];
                // then combine them
                etas = [etas_top etas_bottom];
    
            xi_tt = linspace(0, xi_max, xi_length_fine);
            xi_t = zeros(length(xi_tt), length(etas));
            eta_t = xi_t;
            
            if p_colormap_plots then // colormap plots support
                jc = jetcolormap(round(etas_length/4));
                jc = [jc(1:$, :); jc($:-1:1, :)];
                jc = [jc; jc];
            end
            
            for i=1:etas_length
                eta_tt = etas(i);
            
                [xi_t, eta_t] = meshgrid(xi_tt, eta_tt);
                
                if ~is_circle then // ellipse
                    [x_t, y_t] = mathieu_ell2cart(xi_t, eta_t, f);
                else // circle
                    [x_t, y_t] = mathieu_pol2cart(eta_t, xi_t);
                end
    
                if p_colormap_plots then // colormap plots support
                    jcc = jc(i,:);
                    plot(x_t,y_t, linestyle_hyperbolas, 'Color', jcc);
                else
                    plot(x_t,y_t, linestyle_hyperbolas);
                end
    
                if show_annotations then // show annotations?
                    if (modulo(i, etas_marks_mod) == 0)  then    
    
                        if ~is_circle then // ellipse
                            [mark_pos_x, mark_pos_y] = mathieu_ell2cart(xi_etas_marks, eta_tt, f);
                        else // circle
                            [mark_pos_x, mark_pos_y] = mathieu_pol2cart(eta_tt, xi_etas_marks);
                        end
    
                        if p_xstring then // xstring support
                            if p_latex then // LaTeX support
                                xstring(mark_pos_x, mark_pos_y, '$\eta='+sprintf('%1.0f', eta_tt*180/%pi )+'°$');
                            else
                                xstring(mark_pos_x, mark_pos_y, 'eta='+sprintf('%1.0f', eta_tt*180/%pi )+'°');
                            end
                            // xset("font size", font_size_axes-1);
                        end
                    end
                end
            end
        end
    
        // constant ellipse curves (eta=var, xi=const)
        if show_ellipses then // show ellipses?
            xis = linspace(0, xi_max, xi_length+1);
            xis(1) = []; // remove reduntant ellipse with xi=0
    
            eta_tt = linspace(0, 2*%pi, etas_length_fine);
    
            if p_colormap_plots then // colormap plots support
                jc = jetcolormap(xi_length);
            end
    
            for j=1:xi_length
                xis_tt = xis(j);
    
                    if ~is_circle then // ellipse
                        [x_t, y_t] = mathieu_ell2cart(xis_tt, eta_tt, f);
                    else // circle
                        [x_t, y_t] = mathieu_pol2cart(eta_tt, xis_tt);
                    end
    
                if p_colormap_plots then // colormap plots support
                    plot(x_t, y_t, linestyle_ellipses, 'Color', jc(j,:));
                else
                    plot(x_t, y_t, linestyle_ellipses);
                end
    
                if show_annotations then // show annotations?
                    if ~is_circle then // ellipse
                        [mark_pos_x, mark_pos_y] = mathieu_ell2cart(xis_tt, %pi/2, f);
                        if p_latex then // LaTeX support
                            xi_txt = '\xi=';
                        else
                            xi_txt = 'xi=';
                        end
                    else // circle
                        [mark_pos_x, mark_pos_y] = mathieu_pol2cart(%pi/2, xis_tt);
                        xi_txt = 'r=';
                    end
    
                    if p_xstring then // xstring support
                        xstring(mark_pos_x, mark_pos_y, ltx + xi_txt + sprintf('%1.4f', xis_tt )' + ltx);
                        // xset("font size", font_size_axes-1);
                    end
                end
            end
        end
        
        // mark special points
            // mark foci
            if show_annotations then // show annotations?
                plot(f,0,'b*',-f,0,'b*');
                if ~is_circle then
                    if p_xstring then // xstring support
                        xstring(-f, 0, ltx + '-f' + ltx);
                     end
                end
    
                if p_xstring then // xstring support
                    xstring(f, 0, ltx + 'f' + ltx);
                    // xset("font size", font_size_axes+2);
                end
    
                if show_hyperbolas then
    
                    // mark eta=0
                    if ~is_circle then // ellipse
                        [mark_pos_x, mark_pos_y] = mathieu_ell2cart(xi_etas_marks, 0, f);
                    else // circle
                        [mark_pos_x, mark_pos_y] = mathieu_pol2cart(0, xi_etas_marks);
                    end
    
                    if p_xstring then // xstring support
                        if p_latex then // LaTeX support
                            xstring(mark_pos_x, mark_pos_y, '$\eta=' + sprintf('%1.0f', 0 ) + '°$');
                        else
                            xstring(mark_pos_x, mark_pos_y, sprintf('eta=%1.0f°', 0 ));
                        end
                        // xset("font size", font_size_axes-1);
                    end
    
                    // mark eta=180 deg
                    if ~is_circle then // ellipse
                        [mark_pos_x, mark_pos_y] = mathieu_ell2cart(xi_etas_marks, %pi, f);
                    else // circle
                        [mark_pos_x, mark_pos_y] = mathieu_pol2cart(%pi, xi_etas_marks);
                    end
    
                    if p_xstring then // xstring support
                        if p_latex then // LaTeX support
                            xstring(mark_pos_x, mark_pos_y, '$\eta=' + sprintf('%1.0f', 180 ) + '°$');
                        else
                            xstring(mark_pos_x, mark_pos_y, sprintf('eta=%1.0f°', 180 ));
                        end
                        // xset("font size", font_size_axes-1);
                    end
                end
            end
            
        // add legend
            if show_ellipse & p_legend then // show ellipse & legend?
                if p_latex // LaTeX support
                    legend('$\text{Ellipse with}\ a='+sprintf('%d',a)+',\ b='+sprintf('%d',b)+',\ f='+sprintf('%1.3f',f)+',\ \xi='+sprintf('%1.3f', xi )+'$', -6, 'font_size', font_size_axes+1);
                else
                    legend('Ellipse with a='+sprintf('%d',a)+', b='+sprintf('%d',b)+', f='+sprintf('%1.3f',f)+', xi='+sprintf('%1.3f', xi ), -6, 'font_size', font_size_axes+1);
                end
            end
    
        // set plot limits
              dd = ceil(1.2*max(ax.data_bounds)); 
              ax.data_bounds = [-dd dd -dd dd];
              ax.margins=[0.02 0.02 0.08 0.08];
              xgrid;
    end

    // slow graphics mode
    if p_slow_graphics then
        drawnow();
    end

endfunction
function ui_plot_membrane_mode()
// ui_plot_membrane_mode - function for plotting mode of elliptic membrane
// Based on Mathieu ATOMS's unreleased 'test_memrane.sce'
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//
    global platform_props; // platform propertiea
        p_slow_graphics = platform_props.p_slow_graphics; // slow graphics
        p_colorbar = platform_props.p_colorbar; // colorbar support
        p_latex = platform_props.p_latex; // latex support

    global interface_props; // interface properties
        font_size_axes = interface_props.font_size_axes; // font size for axes
        do_print = interface_props.rootfinder_print; // allow rootfinder text messages?

    // disable Plot button
        set("tab4_plot_button", "Enable", "off");
        set("tab4_plot_button", "String", "Calculating ...");
    // mode table hide
        set("mode_table_frm_tab4", "Visible", "off")
    // plot hide
        set("plot_tab4", "Visible", "off");

    // read controls
        // parameters of elliptic membrane
            a = 0.01 * round(get("tab4_membrane_major_sa_value","value")*10)/10;
            b = 0.01 * round(get("tab4_membrane_minor_sa_value","value")*10)/10;

        // boundary condition
            fun_or_der = %t;
            bc_type_txt = "Soft";
            // Dirichlet (soft)
            if get("tab4_bc_dirichlet", "Value") == 1 then
                fun_or_der = %t;
            end
    
            // Neumann (hard)
            if get("tab4_bc_neumann", "Value") == 1 then
                fun_or_der = %f;
                bc_type_txt = "Hard";
            end

        // mode
            // type
            func_name = "Mc1";
            ell_mode_str = "Odd";
            if get("tab4_odd_mode", "Value") == 1 then
                func_name = "Ms1";
            end
            if get("tab4_even_mode", "Value") == 1 then
                func_name = "Mc1";
                ell_mode_str = "Even";
            end
    
            // angle variations
            m = round(get("tab4_angular_mode_value", "Value"));
            
            // radial variations
            n = round(get("tab4_radial_mode_value", "Value"));

    // rootfinding
        N_xi = 201;
        N_eta = 201;

        mode_options.do_print = do_print;
        mode_options.do_plot_mode = %t;
        mode_options.show_colorbar = p_colorbar;

        // slow graphics mode
        if p_slow_graphics then
            drawlater();
        end

        // plot show
        ax = get("plot_tab4");
        delete(ax.children);
        sca(ax);
        ax.zoom_box = []; // reset zoom
        set("plot_tab4", "Visible", "on");

        // calculate q_delta
        f = sqrt(a^2 - b^2);
        ecc = f/a;
        [xi0, eta_tmp] = mathieu_cart2ell(a, 0, f);
        mode_options.q_delta = (a/b)*n*abs(1 - xi0)*(m+1);

        if ecc < 0.5 then // for b > 0.86*a
            mode_options.q_delta = 0.1;
        elseif ecc > 0.85 then // for b > 0.5*a
            mode_options.q_delta = 10;
        elseif ecc > 0.95 then // for b > 0.3*a
            mode_options.q_delta = 1;
        end

        // calculate and plot mode
        [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, fun_or_der, N_xi, N_eta, mode_options);

    if ~isempty(qs) then
        if p_latex // LaTeX support
            title('$\text{'+ell_mode_str+' '+bc_type_txt+' mode with }m='+string(m)+',\ n='+string(n)+'$');
            xlabel('$x,\ \text{m}$');
            ylabel('$y,\ \text{m}$');
            zlabel('$\text{mode}$');
        else
            title(sprintf('%s %s mode with m=%d, n=%d', ell_mode_str, bc_type_txt, m, n));
            xlabel('x, m');
            ylabel('y, m');
            zlabel('mode');
        end

    // prepare data for table
        // fill table row with mode data
        show_all_modes = %f;
        if show_all_modes then
            for j=1:n
                table_data(j, 1) = "";
                table_data(j, 2) = string(m);
                table_data(j, 3) = string(j);
                table_data(j, 4) = string(qs(j));
                table_data(j, 5) = string(ka(j));
                table_data(j, 6) = string(ws(j));
            end
        else
            table_data(1, 1) = "";
            table_data(1, 2) = string(m);
            table_data(1, 3) = string(n);
            table_data(1, 4) = string(qs(n));
            table_data(1, 5) = string(ka(n));
            table_data(1, 6) = string(ws(n));
        end

        // UserData handle
        userdata = get("mode_table_tab4", "UserData");
        table_header = userdata.mode_table_header;
        table_data = [table_header; table_data];

        // set table contents
        set("mode_table_tab4", "String", table_data);

        // show table
        set("mode_table_frm_tab4", "Visible", "on")

        // slow graphics mode
        if p_slow_graphics then
            drawnow();
        end
    end

    // enable Plot button
    set("tab4_plot_button", "Enable", "on");
    set("tab4_plot_button", "String", "Plot");
    
endfunction
function ui_plot_rmf()
// ui_plot_rmf - plot angular Mathieu functions
//
// get elements:
//      "tab3_odd_rmf_m_value".Value - tag of odd order 'm' slider value
//      "tab3_even_rmf_m_value".Value - tag of even order 'm' slider value
//      "tab3_q_rmf_value".Value" - tag of 'q' slider value
//
//      "tab3_even_rmf_show_fun" - tag of show even function checkbox
//      "tab3_odd_rmf_show_fun" - tag of show odd function checkbox
//      "tab3_even_rmf_show_der" - tag of show even derivative checkbox
//      "tab3_odd_rmf_show_der" - tag of show odd derivarive checkbox
//      "plot1_tab3" - tag of top tab3 axes
//      "plot2_tab3" - tag of bottom tab3 axes
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//
    global platform_props; // platform properties
        p_slow_graphics = platform_props.p_slow_graphics; // slow graphics

    // input parameters
        // m
        m_odd = round(get("tab3_odd_rmf_m_value","value"));
        m_even = round(get("tab3_even_rmf_m_value","value"));
        // q
        q = round(get("tab3_q_rmf_value","value"));


    // read controls
        // show function?
        show_even_fun = %t;
        show_odd_fun = %t;
        if get("tab3_even_rmf_show_fun","value") == 0 then
            show_even_fun = %f;
        end
        if get("tab3_odd_rmf_show_fun","value") == 0 then
            show_odd_fun = %f;
        end

        // show derivative?
        show_even_der = %t;
        show_odd_der = %t;
        if get("tab3_even_rmf_show_der","value") == 0 then
            show_even_der = %f;
        end
        if get("tab3_odd_rmf_show_der","value") == 0 then
            show_odd_der = %f;
        end

        // function type
            // odd
            odd_func_name = '';
            odd_func_tags = ["tab3_odd_rmf_Ms1";"tab3_odd_rmf_Ms2";"tab3_odd_rmf_Se";"tab3_odd_rmf_Gey"];
            odd_func_tags_sz = size(odd_func_tags);
            for i=1:odd_func_tags_sz(1)
                odd_func_tag = odd_func_tags(i);
                if get(odd_func_tag, "value") == 1 then
                    odd_func_name = get(odd_func_tag, "String");
                    break;
                end
            end

            // even
            even_func_name = '';
            even_func_tags = ["tab3_even_rmf_Mc1";"tab3_even_rmf_Mc2";"tab3_even_rmf_Ce";"tab3_even_rmf_Fey"];
            even_func_tags_sz = size(even_func_tags);
            for i=1:even_func_tags_sz(1)
                even_func_tag = even_func_tags(i);
                if get(even_func_tag, "value") == 1 then
                    even_func_name = get(even_func_tag, "String");
                    break;
                end
            end

    // set plot parameters
    max_z = %pi;
    z = linspace(0, max_z, max([201, 20*round(max_z)*m_odd*q]));

    plot_parameters.legend_pos = 3;

    // slow graphics mode
    if p_slow_graphics then
        drawlater();
    end

    plot_parameters.ax = get("plot1_tab3");
    if ~isempty(even_func_name) then
        calculate_and_show_plot(even_func_name, m_even, q, z, show_even_fun, show_even_der, plot_parameters);
    end

    plot_parameters.ax = get("plot2_tab3");
    if ~isempty(odd_func_name) then
        calculate_and_show_plot(odd_func_name, m_odd, q, z, show_odd_fun, show_odd_der, plot_parameters);
    end

    // slow graphics mode
    if p_slow_graphics then
        drawnow();
    end

endfunction
function ui_slider_read(uh_slider, has_fmt)
// ui_slider_read - read slider to text field
//
// input variables:
//      uh_slider - handle to the slider with UserData
//          uh_slider.UserData.text_string - text string with format for 'a_m(q)' text label
//      has_fmt - use format or round value (boolean)
//
// get elements:
//          uh_slider.Value - value of the slider
//
// set elements:
//          uh.UserData.text_tag.String - string inside text label
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//
    ud = uh_slider.UserData; // shorcut for UserData

    if has_fmt then // formatted output
        set(ud.text_tag, "String", sprintf(ud.text_string, uh_slider.Value));
    else // rounded value
        set(ud.text_tag, "String", ud.text_string + string(round(uh_slider.Value)));
    end
endfunction
function calculate_and_show_plot(func_name, m, q, z, show_fun, show_der, plot_parameters)
// calculate_and_show_plot - calculate and show plot of Mathieu function
//
// input variables:
//      func_name - function name (angular - 'ce', 'se')
//      m - order of function
//      q - value of 'q' parameter
//      z - vector of argmunets
//      show_fun - show function? (boolean)
//      show_der - show first derivative? (boolean)
//      plot_parameters - structure with plot parameters
//          .legend_pos     - legend position
//          .ax             - axes handle
//
// --------------------------------------------------------------------------------------- //
//  Copyright 2016 | Nikolay Strelkov, MPEI | http://atoms.scilab.org/toolboxes/Mathieu/   //
// --------------------------------------------------------------------------------------- //
//
    global platform_props; // platform properties
        p_slow_graphics = platform_props.p_slow_graphics; // slow graphics
        p_legend = platform_props.p_legend; // legend support
        p_xstring = platform_props.p_xstring; // xstring support
        p_latex = platform_props.p_latex; // LaTeX support
        p_colormap_plots = platform_props.p_colormap_plots; // colormap plots support

    global interface_props; // interface properties
        font_size_axes = interface_props.font_size_axes; // font size for axes
        linestyle_fun = interface_props.linestyle_fun;  // line style for function
        linestyle_der = interface_props.linestyle_der; // line style for derivative

    // get variables
    legend_pos = plot_parameters.legend_pos; // legend position
    ax = plot_parameters.ax; // axes handle

    // template for plot title - function and/or first derivative
    fun_der_txt = '';
    if show_fun & ~show_der then
        fun_der_txt = 'function';
    end
    if ~show_fun & show_der then
        fun_der_txt = 'function`s first derivative';
    end
    if show_fun & show_der then
        fun_der_txt = 'function and its first derivative';
    end

    // function type selection
    ang_fun = %f;
    kind = 0;

    select func_name
        // angular functions
        case 'ce' then
            mathieu_handle = mathieu_ang_ce;
            mathieu_AB_handle = mathieu_Arm;
            y_text_plain = ['ce_'+string(m)+'(z,q)';'ce`_'+string(m)+'(z,q)'];
            y_text_latex = ['ce_{'+string(m)+'}(z,q)';'$ce`_{'+string(m)+'}(z,q)'];

            title_text_plain = "Even angular Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Even angular Mathieu '+fun_der_txt+'}$';

            ang_fun = %t;

        case 'se' then
            mathieu_handle = mathieu_ang_se;
            mathieu_AB_handle = mathieu_Brm;
            y_text_plain = ['se_'+string(m)+'(z,q)'; 'se`_'+string(m)+'(z,q)'];
            y_text_latex = ['se_{'+string(m)+'}(z,q)'; 'se`_{'+string(m)+'}(z,q)'];

            title_text_plain = "Odd angular Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Odd angular Mathieu '+fun_der_txt+'}$';

            ang_fun = %t;

        // radial functions
        case 'Mc1' then
            mathieu_handle = mathieu_rad_mc;
            mathieu_AB_handle = mathieu_Arm;
            kind = 1;
            y_text_plain = ['Mc1_'+string(m)+'(z,q)'; 'Mc1`_'+string(m)+'(z,q)'];
            y_text_latex = ['Mc^{(1)}_{'+string(m)+'}(z,q)'; 'Mc`^{(1)}_{'+string(m)+'}(z,q)'];

            title_text_plain = "Even radial Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Even radial Mathieu '+fun_der_txt+'}$';

            ang_fun = %f;

        case 'Ms1' then
            mathieu_handle = mathieu_rad_ms;
            mathieu_AB_handle = mathieu_Brm;
            kind = 1;
            y_text_plain = ['Ms1_'+string(m)+'(z,q)'; 'Ms1`_'+string(m)+'(z,q)'];
            y_text_latex = ['Ms^{(1)}_{'+string(m)+'}(z,q)'; 'Ms`^{(1)}_{'+string(m)+'}(z,q)'];

            title_text_plain = "Odd radial Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Odd radial Mathieu '+fun_der_txt+'}$';

            ang_fun = %f;

        case 'Mc2' then
            mathieu_handle = mathieu_rad_mc;
            mathieu_AB_handle = mathieu_Arm;
            kind = 2;
            y_text_plain = ['Mc2_'+string(m)+'(z,q)'; 'Mc2`_'+string(m)+'(z,q)'];
            y_text_latex = ['Mc^{(2)}_{'+string(m)+'}(z,q)'; 'Mc`^{(2)}_{'+string(m)+'}(z,q)'];

            title_text_plain = "Even radial Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Even radial Mathieu '+fun_der_txt+'}$';

            ang_fun = %f;

        case 'Ms2' then
            mathieu_handle = mathieu_rad_ms;
            mathieu_AB_handle = mathieu_Brm;
            kind = 2;
            y_text_plain = ['Ms2_'+string(m)+'(z,q)'; 'Ms2`_'+string(m)+'(z,q)'];
            y_text_latex = ['Ms^{(2)}_{'+string(m)+'}(z,q)'; 'Ms`^{(2)}_{'+string(m)+'}(z,q)'];

            title_text_plain = "Odd radial Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Odd radial Mathieu '+fun_der_txt+'}$';

            ang_fun = %f;
            
        case 'Ce' then
            mathieu_handle = mathieu_rad_ce;
            mathieu_AB_handle = mathieu_Arm;

            y_text_plain = ['Ce_'+string(m)+'(z,q)'; 'Ce`_'+string(m)+'(z,q)'];
            y_text_latex = ['Ce_{'+string(m)+'}(z,q)'; 'Ce`_{'+string(m)+'}(z,q)'];

            title_text_plain = "Even radial Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Even radial Mathieu '+fun_der_txt+'}$';

            ang_fun = %f;

        case 'Se' then
            mathieu_handle = mathieu_rad_se;
            mathieu_AB_handle = mathieu_Brm;

            y_text_plain = ['Se_'+string(m)+'(z,q)'; 'Se`_'+string(m)+'(z,q)'];
            y_text_latex = ['Se_{'+string(m)+'}(z,q)'; 'Se`_{'+string(m)+'}(z,q)'];

            title_text_plain = "Odd radial Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Odd radial Mathieu '+fun_der_txt+'}$';

            ang_fun = %f;

        case 'Fey' then
            mathieu_handle = mathieu_rad_fey;
            mathieu_AB_handle = mathieu_Arm;

            y_text_plain = ['Fey_'+string(m)+'(z,q)'; 'Fey`_'+string(m)+'(z,q)'];
            y_text_latex = ['Fey_{'+string(m)+'}(z,q)'; 'Fey`_{'+string(m)+'}(z,q)'];

            title_text_plain = "Even radial Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Even radial Mathieu '+fun_der_txt+'}$';

            ang_fun = %f;

        case 'Gey' then
            mathieu_handle = mathieu_rad_gey;
            mathieu_AB_handle = mathieu_Brm;

            y_text_plain = ['Gey_'+string(m)+'(z,q)'; 'Gey`_'+string(m)+'(z,q)'];
            y_text_latex = ['Gey_{'+string(m)+'}(z,q)'; 'Gey`_{'+string(m)+'}(z,q)'];

            title_text_plain = "Odd radial Mathieu "+fun_der_txt;
            title_text_latex = '$\text{Odd radial Mathieu '+fun_der_txt+'}$';

            ang_fun = %f;

    end

    // LaTeX or plain text?
    if p_latex then 
        ltx = '$';
        ltx_space = '\ ';
        y_text = y_text_latex;
        title_text = title_text_latex;
    else
        ltx = '';
        ltx_space = ' ';
        y_text = y_text_plain;
        title_text = title_text_plain;
    end

    y_label_text = '';
    legend_text = '';

    // prepare plot
        ax.auto_clear = "on";
        delete(ax.children);
        sca(ax);
        ax.zoom_box = []; // reset zoom
        ax.visible = 'off';
        
    // plot function and/or derivative    
    if show_fun | show_der then
        ax.visible = 'on';
        ax.auto_clear = "off";
        ax.auto_scale = "on";

        // angular functions
        if ang_fun then
            z_deg = z*180/%pi; // calc z in degrees
            x_label_text = 'z, °';
        else
            x_label_text = 'z';
        end

            ax.data_bounds = [min(z), max(z), -0.01, 0.01];
    
            i = 1;
    
                [ ABrm, abm ] = mathieu_AB_handle( m, q );
                if show_fun then
                    fun_or_der = 1;
                    if kind > 0 then
                        fun = mathieu_handle( m, q, z, fun_or_der, kind, ABrm );
                    else
                        fun = mathieu_handle( m, q, z, fun_or_der, ABrm );
                    end

                    if ang_fun then
                        plot(z_deg, fun, linestyle_fun);
                    else
                        plot(z, fun, linestyle_fun);
                    end

                    y_label_text = y_text(1);
                    legend_text(i) = ltx + y_text(1) + ltx;
                    i = i + 1;
                end
                if show_der then
                    fun_or_der = 0;
                    if kind > 0 then
                        der = mathieu_handle( m, q, z, fun_or_der, kind, ABrm );
                    else
                        der = mathieu_handle( m, q, z, fun_or_der, ABrm );
                    end

                    if ang_fun then
                        plot(z_deg, der, linestyle_der);
                    else
                        plot(z, der, linestyle_der);
                    end

                    if isempty(y_label_text) then
                        y_label_text = y_text(2);
                    else
                        y_label_text = y_label_text + ',' + ltx_space + y_text(2);
                    end
                    legend_text(i) = ltx + y_text(2) + ltx;
                end        


    // set ax properties
        ax.title.font_size = font_size_axes+1;
        ax.font_size = font_size_axes-1;
        ax.title.text = title_text;
        xlabel(ltx + x_label_text + ltx);
        ylabel(ltx + y_label_text + ltx);
        ax.x_label.font_size = font_size_axes; 
        ax.y_label.font_size = font_size_axes;
        ax.tight_limits = "on";
        xgrid;

    // add legend
        if p_legend then // show legend?
            legend(legend_text, 'font_size', font_size_axes+1, legend_pos);
        end
    end
endfunction
Mathieu_Gui();
