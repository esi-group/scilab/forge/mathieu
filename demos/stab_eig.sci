// Copyright (C) 2012-2016 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

// Stability chart for eigenvalues of Mathieu' equations (comp. http://dlmf.nist.gov/28.17)
    f=scf(); mrgn = 50; font_sz = 4;
    sz = get(0, 'screensize_px') + [mrgn-1 mrgn-1 -2*mrgn -2*mrgn ] ; 
    f.figure_name = 'Stability chart for eigenvalues of Mathieu equation (compare with http://dlmf.nist.gov/28.17)';
    nq = 30;
    q = linspace(-15, 15, nq);
    a0 = zeros(1, nq); a1 = a0; a2 = a0; a3 = a0;
    b1 = a0; b2 = a0; b3 = a0; b4 = a0;

    for i=1:nq
        [Arm_0, a_0] = mathieu_Arm(0, q(i));
        [Arm_1, a_1] = mathieu_Arm(1, q(i));
        [Arm_2, a_2] = mathieu_Arm(2, q(i));
        [Arm_3, a_3] = mathieu_Arm(3, q(i));
            a0(i) = a_0;
            a1(i) = a_1;
            a2(i) = a_2;
            a3(i) = a_3;
        [Brm_1, b_1] = mathieu_Brm(1, q(i));
        [Brm_2, b_2] = mathieu_Brm(2, q(i));
        [Brm_3, b_3] = mathieu_Brm(3, q(i));
        [Brm_4, b_4] = mathieu_Brm(4, q(i));
            b1(i) = b_1;
            b2(i) = b_2;
            b3(i) = b_3;
            b4(i) = b_4;
    end
    
    plot(q, a0, q, a1, q, a2, q, a3, q, b1, q, b2, q, b3, q, b4,'--');
    xgrid
    t = '$a$'; xlabel('$q$'); ylabel(t);
    legend('$a_0$', '$a_1$', '$a_2$', '$a_3$', '$b_1$', '$b_2$', '$b_3$', '$b_4$', pos=3);
    title('$\text{Stability chart for eigenvalues of Mathieu equation}$')
        
        h = gca(); h.font_size = font_sz-1; h.title.font_size=font_sz; 
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
        
    f.figure_position=[sz(1) sz(2)]; f.figure_size=[sz(3) sz(4)]; 
