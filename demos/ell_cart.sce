// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

// preparing figure
s_wh = get(0, 'screensize_px');
margin_h = 50;
margin_w = 5;
max_w = s_wh(3)-2*margin_w;
max_h = s_wh(4)-2*margin_h;
h = figure('BackgroundColor', [1 1 1]);
h.figure_name = 'Elliptic and Cartesian coordinates';

// parameters of ellipse
a = 5; b = 4; f = sqrt(a^2 - b^2);
xi = real(acosh(a/f));

// additional parameters
etas_length = 16;
xi_length = 10;
xi_max = ceil(xi*1.5);
etas_marks_mod = 1;
if etas_length > 20 then
    etas_marks_mod = 2;
end
xi_etas_marks = xi_max*1.06;

xi_length_fine = 11; // xis for fine hyperbolas plots
etas_length_fine = 25; // etas for fine ellipse plots

if (getos() == "Windows") then
  font_size_normal = 2; 
else
  font_size_normal = 3; 
end

// line styles
    linestyle_ellipse = 'b-'; // ellipse
    linestyle_hyperbolas = '-.'; // hyperbolas
    linestyle_ellipses = '--'; // ellipses

// draw ellipse
eta = linspace(0, 2*%pi, 201);//0:0.01*%pi:2*%pi;
[x_el, y_el] = mathieu_ell2cart(xi, eta, f);

plot(x_el, y_el, linestyle_ellipse);
h = gca(); h.isoview = 'on';
h.font_size = font_size_normal;
xtitle('$\text{Elliptic and Cartesian coordinates}$','$x$','$y$');



// constant angle curves (eta=const, xi=var)
    // divide in half-planes - top
    etas_top = linspace(0, %pi, etas_length/2 + 2);
    etas_top(1) = [];
    etas_top($) = [];
    // and bottom
    etas_bottom = linspace(%pi, 2*%pi, etas_length/2 + 2);
    etas_bottom(1) = [];
    etas_bottom($) = [];
    // then combine them
    etas = [etas_top etas_bottom];

    xi_tt = linspace(0, xi_max, xi_length_fine);
    xi_t = zeros(length(xi_tt), length(etas));
    eta_t = xi_t;

    jc = jetcolormap(round(etas_length/4));
    jc = [jc(1:$, :); jc($:-1:1, :)];
    jc = [jc; jc];

    for i=1:etas_length
        eta_tt = etas(i);
    
        [xi_t, eta_t] = meshgrid(xi_tt, eta_tt);
        [x_t, y_t] = mathieu_ell2cart(xi_t, eta_t, f);

        jcc = jc(i,:);

        plot(x_t,y_t, linestyle_hyperbolas, 'Color', jcc);
    
        if (modulo(i, etas_marks_mod) == 0)  then    
            [mark_pos_x, mark_pos_y] = mathieu_ell2cart(xi_etas_marks, eta_tt, f);
            xstring(mark_pos_x, mark_pos_y, '$\eta='+sprintf('%1.0f', eta_tt*180/%pi )+'\degree$');
        end
    end


// constant ellipse curves (eta=var, xi=const)
    xis = linspace(0, xi_max, xi_length+1);
    xis(1) = []; // remove reduntant ellipse with xi=0

    eta_tt = linspace(0, 2*%pi, etas_length_fine);

    jc = jetcolormap(xi_length);

    for j=1:xi_length
        xis_tt = xis(j);
            [x_t, y_t] = mathieu_ell2cart(xis_tt, eta_tt, f);
            plot(x_t, y_t, linestyle_ellipses, 'Color', jc(j,:));
    
            [mark_pos_x, mark_pos_y] = mathieu_ell2cart(xis_tt, %pi/2, f);            
            xstring(mark_pos_x, mark_pos_y, '$\xi='+sprintf('%1.4f', xis_tt )+'$');
    end

// mark foci
    plot(f,0,'b*',-f,0,'b*');
    gca().font_size = font_size_normal+2;
    xstring(f, 0, '$f$');
    xstring(-f, 0, '$-f$');

// mark eta=0
    h.font_size = font_size_normal;
    [mark_pos_x, mark_pos_y] = mathieu_ell2cart(xi_etas_marks, 0, f);
    xstring(mark_pos_x, mark_pos_y, '$\eta='+sprintf('%1.0f', 0 )+'\degree$');

// mark eta=180 deg
    [mark_pos_x, mark_pos_y] = mathieu_ell2cart(xi_etas_marks, %pi, f);
    xstring(mark_pos_x, mark_pos_y, '$\eta='+sprintf('%1.0f', 180 )+'\degree$');

// add legend
    legend('$'+'\text{Ellipse with}\ a=5,\ b=4,\ f=3,\ \xi='+sprintf('%1.4f', xi )+'$', -6)

// set plot limits
    h=gcf();  
    h.figure_position = [margin_w margin_h];
    h.figure_size = [max_w max_h];

    h = gca(); h.isoview = 'on'; 
    h.font_size = font_size_normal+1;
    dd = ceil(1.2*max(h.data_bounds));
    h.data_bounds = [-dd dd -dd dd];
    
    h.font_size = font_size_normal;
    h.title.font_size=font_size_normal+1;
    h.x_label.font_size=font_size_normal+1;
    h.y_label.font_size=font_size_normal+1;
    xgrid;
