// Copyright (C) 2012-2016 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html


num_points=100;
x=linspace(0,%pi,num_points);
y=ones(1,num_points);
z=zeros(length(x));


kinds = ['Fey' 'Fey'; 'Gey' 'Gey'];
orders = [0 1; 1 2];
q_min = 0;
q_step = 0.1;
q_max = 3;

[n_max m_max] = size(orders);

  font_sz = 4;
  s_wh = get(0, 'screensize_px');
  margin_h = 50;
  margin_w = 5;
  max_w = s_wh(3)-2*margin_w;
  max_h = s_wh(4)-2*margin_h;     
  h = scf();
  h.figure_name = 'Plots of Fey and Gey as a functions of z and q.';

p=1;
for n=1:n_max
  for m=1:m_max
    subplot(n_max,m_max,p);
    
    for q=q_min:q_step:q_max
        if q == 0 then
            z = %nan * x;
        else
            select kinds(n,m)
                case 'Fey' then
                    z = mathieu_rad_fey(orders(n,m), q, x, 1);
                case 'Gey' then
                    z = mathieu_rad_gey(orders(n,m), q, x, 1);
            end
        end

        plot3d3(x, q*y, z, theta=-70, alpha=65);
        mtlb_hold('on');
    end
    name_tex=sprintf('$%s_%d(z, q)$', kinds(n, m), orders(n, m));
    xtitle(name_tex,'$z$','$q$','');
      
      h=gca();
      h.tight_limits='on';
      h.box='off';
      h.title.font_size=font_sz;
      h.title.font_style=font_sz;
      h.x_label.font_size=font_sz;
      h.y_label.font_size=font_sz;
      h.z_label.font_size=font_sz;      
      h.font_size=font_sz-1;  
      h.margins(3:4) = 0.2;

      xgrid

    p = p+1;
  end
end

  h=gcf();  
  h.figure_position = [margin_w margin_h];
  h.figure_size = [max_w max_h];
