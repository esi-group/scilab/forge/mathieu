// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - 2016 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

//comparison of 4 modes - Soft and Hard, Even and Odd
    a = 0.05;
    b = 0.03;
    m = 2;
    n = 3;
    N_xi = 101;
    N_eta = 101;
    options.font_size_axes = 3;

    f_wh = get(0, 'screensize_px'); margin = 50;
    f_h = figure('Background',-2);
    f_h.figure_name = 'Elliptic membrane: comparison of 4 even & odd, soft & hard';

    subplot(2,2,1);
        mathieu_membrane_mode(a, b, m, n, 'Mc1', %t, N_xi, N_eta, options);
        ax = gca();
        ax.rotation_angles = [0,-90];
        ax.margins(1:4) = 0.15;
        zlabel('');
    subplot(2,2,2);
        mathieu_membrane_mode(a, b, m, n, 'Mc1', %f, N_xi, N_eta, options);
        ax = gca();
        ax.rotation_angles = [0,-90];
        ax.margins(1:4) = 0.15;
        zlabel('');
    subplot(2,2,3);
        mathieu_membrane_mode(a, b, m, n, 'Ms1', %t, N_xi, N_eta, options);
        ax = gca();
        ax.rotation_angles = [0,-90];
        ax.margins(1:4) = 0.15;
        zlabel('');
    subplot(2,2,4);
        mathieu_membrane_mode(a, b, m, n, 'Ms1', %f, N_xi, N_eta, options);
        ax = gca();
        ax.rotation_angles = [0,-90];
        ax.margins(1:4) = 0.15;
        zlabel('');

    f_h.figure_size = [f_wh(3) - 2*margin, f_wh(4) - 2*margin];
    f_h.figure_position = [margin margin];
