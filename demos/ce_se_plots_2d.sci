// Copyright (C) 2012-2016 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

// Comparison of periodic Mathieu functions plots for several orders and values of q with 
// Figs. 20.2-20.5 (pp. 725-726) from
// the classical book
// M. Abramowitz and I. A. Stegun, Handbook of Mathematical Functions (National Bureau of
// Standards, Washington DC, 1964).
//

// local functions
function set_xy_ticks(x_ticks, y_ticks)
  hndl = gca();
  
  for i=1:length(x_ticks)
     if(modulo(x_ticks(i), 20) == 0)      
      x_ticks_str(1,i) = '$' + sprintf('%-2d', x_ticks(i)) + '\degree$';
     else      
      x_ticks_str(1,i) = '';
    end    
  end  
  
   hndl.x_ticks = tlist(['ticks','locations','labels'], ..
                        x_ticks, ..
                        x_ticks_str);  
                       
   for i=1:length(y_ticks)
     y_ticks_str(1,i) = sprintf('$%-2.1f$', y_ticks(i));
   end
   hndl.y_ticks = tlist(['ticks','locations','labels'], ..
                     y_ticks, ..
                     y_ticks_str);

  hndl.tight_limits='on';                     
  hndl.data_bounds=[min(x_ticks),min(y_ticks); max(x_ticks), max(y_ticks)];
  hndl.margins = [0.15 0.15 0.15 0.2];
endfunction

function plot_and_annotate_graph(kind, orders, q, phis, plot_title)
  zs = phis*180/%pi;
  mathieu_data = mathieu_mathieu(kind, orders, phis, q);
  
  plot2d(zs, mathieu_data, axesflag=1);  
  
  phi0 = %pi/4;
  phi0x = (%pi/4)*90/(%pi/2);
  
  for i=1:length(orders)
    orders_str(1,i) = '$' + sprintf('%s_%1d', kind, orders(i)) + '$';
  end

  xgrid; 
  y_label_tex=sprintf('$%s_r$', kind);
  xtitle(plot_title,'$z$',y_label_tex);   
  legend(orders_str, pos=3);
  
  a=get("current_axes");
  a.title.font_size=3;
  a.title.font_style=4;
  a.x_label.font_size=5;
  a.y_label.font_size=5;
  a.font_size=3;
  leg=a.children(1)
  leg.font_size = 3;
endfunction
//////////////////////////////////////////////////////////////////////

  phi=linspace(0,%pi/2,90);
  x_ticks = 0:10:90;
      
  s_wh = get(0, 'screensize_px');
  margin_h = 50;
  margin_w = 5;
  max_w = s_wh(3)-2*margin_w;
  max_h = s_wh(4)-2*margin_h;

      
  h = scf(); 
  h.figure_name = 'Comparison of periodic Mathieu functions plots for several orders and values of q with Figs. 20.2-20.5 (pp. 725-726) of M. Abramowitz and I. A. Stegun Handbook of Mathematical Functions, 1964.';  
  h.figure_position = [margin_w margin_h];
  h.figure_size = [max_w max_h];

// plotting
subplot(2,2,1);
    q = 1;
    r = 0:5;
    y_ticks = -1.0:0.2:1.2;
    
    plot_and_annotate_graph('ce', r, q, phi, '$\text{Even periodic Mathieu functions, orders}\ 0-5,\ q=1\ \text{(compare with Fig. 20.2. p.725)}$');
    set_xy_ticks(x_ticks, y_ticks);
        
subplot(2,2,2);
    q = 1;
    r = 1:5;    
    y_ticks = -1.0:0.2:1.2;
    
    plot_and_annotate_graph('se', r, q, phi, '$\text{Odd periodic Mathieu functions, orders}\ 1-5,\ q=1\ \text{(compare with Fig. 20.3. p.726)}$');
    set_xy_ticks(x_ticks, y_ticks);
        
subplot(2,2,3);
    q = 10;
    r = 0:5;    
    y_ticks = -1.0:0.2:1.6;
    
    plot_and_annotate_graph('ce', r, q, phi, '$\text{Even periodic Mathieu functions, orders}\ 0-5,\ q=10\ \text{(compare with Fig. 20.4. p.726)}$');
    set_xy_ticks(x_ticks, y_ticks);
    
subplot(2,2,4);
    q = 10;
    r = 1:5;    
    y_ticks = -1.0:0.2:1.6;
    
    plot_and_annotate_graph('se', r, q, phi, '$\text{Odd periodic Mathieu functions, orders}\ 1-5,\ q=10\ \text{(compare with Fig. 20.4. p.726)}$');
    set_xy_ticks(x_ticks, y_ticks);
