// ====================================================================
// Copyright INRIA 2008
// Allan CORNET
// This file is released into the public domain
// ====================================================================
// Copyright (C) 2010 - 2016 - N. O. Strelkov, NRU MPEI

demopath = get_absolute_file_path("mathieu.dem.gateway.sce");

subdemolist = ["Elliptic and Cartesian coordinates", "ell_cart.sce"; ..
               "Stability chart for eigenvalues of Mathieu`s equations", "stab_eig.sci";..
               "Plots of ce, se (z,q) of order 0-5 for q=1,10 for comparison with Abramowitz and Stegun", "ce_se_plots_2d.sci"; ..
               "Tables of ce and ce` (z,q) for comparison with Abramowitz and Stegun", "ce_tables.sci";..
               "Tables of se and se` (z,q) for comparison with Abramowitz and Stegun", "se_tables.sci";..
               "Plots of ce, se (z,q) of order 0-3 for q=0-30 and z=[0, π]", "ce_se_plots_3d.sci"; ..
               "Plots of Mc(1), Ms(1) (z,q) of order 0-2 for q=0-3 and z=[0, π]", "mc1_ms1_plots_3d.sci";..
               "Plots of Mc(2), Ms(2) (z,q) of order 0-2 for q=0-3 and z=[0, π]", "mc2_ms2_plots_3d.sci";..
               "Plots of Ce, Se (z,q) of order 0-2 for q=0-3 and z=[0, π]", "rad_ce_se_plots_3d.sci";..
               "Plots of Fey, Gey (z,q) of order 0-2 for q=0-3 and z=[0, π]", "rad_fey_gey_plots_3d.sci";..
               "Elliptic membrane: comparison of 2 even soft modes" ,"confroq.sce" ; ..
               "Elliptic membrane: comparison of 4 even & odd, soft & hard" ,"confroq4.sce"; ..
               "Elliptic membrane: 3D surface plot of a even soft mode with m=3, n=3", "ellissi.sce"];

// check Scilab version
v = getversion("scilab");
if ~(v(1) <= 5 & v(2) < 5) then
subdemolist = [ "GUI for Mathieu Function Toolbox", "mathieu_gui.sci"; ..
               subdemolist ];
end

subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================
