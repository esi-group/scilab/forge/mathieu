// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

// Characteristic values, joining factors, some critical values (Even solutions) for comparison with some values from Table 20.1 (p. 748)
// in the classical book
// M. Abramowitz and I. A. Stegun, Handbook of Mathematical Functions (National Bureau of
// Standards, Washington DC, 1964).
//
//
// local functions

function str = float2str_e(val, decs)
  if(length(val)>1)
    str = sprintf('%-15s',val);    
  else
    if decs==7 then
      str = sprintf('%-15.7e',val);      
    else
      str = sprintf('%-15.8e',val);
    end
  end
endfunction

function str = float2str(val)
  if(length(val)>1)
    str = sprintf('%-15s',val);    
  else
    str = sprintf('%-15.8f',val);
  end
endfunction

function str = int2str(val)
  str = sprintf('%2d', val)
endfunction

function table_row_1column (s)
  printf('%s\n', s);  
endfunction

function table_row_5columns (r, q, ab_str, y1_str, y2_str)
  printf('| %2s | %2s | %-15s | %-15s | %-15s |\n', r, q, ab_str, y1_str, y2_str);
endfunction
//////////////////////////////////////////////////////////////////////////////////////////

  r = [0 2 10; 1 5 15];
  q = 0:5:25;
  prec = 1e-12;
  
  table_row_1column('Characteristic values, joining factors, some critical values for comparison');
  table_row_1column('with some values from Table 20.1 (p. 748) in M. Abramowitz and I. A. Stegun, Handbook of Mathematical Functions, 1964');
  table_row_1column('');
  table_row_1column('Even solutions');
  table_row_1column('');    
  table_row_5columns('--', '--', '---------------', '---------------', '---------------');  
  table_row_5columns('r', 'q', 'a_r', 'ce_r(0, q)', 'ce_r(π/2, q)   ')
  table_row_5columns('--', '--', '---------------', '---------------', '---------------');  
  
  r_row = 1;
  
for i=1:max(size(r))
  for j=1:length(q)
      if (j==1)
        rr = int2str(r(r_row,i));
      else
        rr = '';
      end
      
      qs = int2str(q(j));
      [y1 ab] = mathieu_mathieu('ce', r(r_row,i), 0, q(j), prec);
      [y2 ab] = mathieu_mathieu('ce', r(r_row,i), %pi/2, q(j), prec);

      table_row_5columns(rr , qs,  float2str(ab), float2str_e(y1, 8), float2str_e(y2, 7));

      if(j == length(q))
        table_row_5columns('--', '--', '---------------', '---------------', '---------------');
      end      
    end
end
  

  table_row_1column('');    
  table_row_5columns('--', '--', '---------------', '---------------', '---------------');  
  table_row_5columns('r', 'q', 'a_r', 'ce_r(0, q)', 'ce`_r(π/2, q)  ')
  table_row_5columns('--', '--', '---------------', '---------------', '---------------');  
  
r_row = 2;  
  
for i=1:max(size(r))
  for j=1:length(q)
      if (j==1)
        rr = int2str(r(r_row,i));
      else
        rr = '';
      end
      
      qs = int2str(q(j));
      [y1 ab] = mathieu_mathieu('ce', r(r_row,i), 0, q(j), prec);
      [y2 ab] = mathieu_mathieu('ce_', r(r_row,i), %pi/2, q(j), prec);

      table_row_5columns(rr , qs,  float2str(ab), float2str_e(y1, 8), float2str_e(y2, 7));

      if(j == length(q))
        table_row_5columns('--', '--', '---------------', '---------------', '---------------');
      end      
    end
end

  table_row_1column('');    
  table_row_1column('');    
