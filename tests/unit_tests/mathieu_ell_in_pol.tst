// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

a = 5; b = 4;
[theta, rho] = mathieu_ell_in_pol (a, b, 0);
assert_checkalmostequal(rho, a, 1.e-6);
assert_checkalmostequal(theta, 0, 1.e-6);

// draw the ellipse
a = 5; b = 4; e = sqrt (a^2 - b^2) / a; // parameters of ellipse
tht = linspace(0, 2*%pi, 100);
[thta, rho] = mathieu_ell_in_pol(a, b, tht);
[x, y] = mathieu_pol2cart(thta, rho);
polarplot(thta, rho);
plot(x, y, 'c:'); xgrid; h=gca(); h.isoview='on';
xtitle('Ellipse in polar and Cartesian coordinates', 'x', 'y');
legend('polar','Cartesian');
