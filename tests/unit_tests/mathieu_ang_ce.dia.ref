// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// comparison with values from Table 20.1 from M. Abramowitz and I. A. Stegun
tol = 5.e-8;
// functions
fun_or_der = 1;
    // m = 0
        // z = 0
        m = 0; q = 0; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 7.07106781e-1, tol);
        m = 0; q = 25; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 2.15863018e-4, tol);
        // z = pi/2
        m = 0; q = 0; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 7.07106781e-1, tol);
        m = 0; q = 25; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.6575103, tol);
    // m = 2
        // z = 0
        m = 2; q = 0; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.0, tol);
        m = 2; q = 25; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.15128663e-2, tol);
        // z = pi/2
        m = 2; q = 0; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -1.0, tol);
        m = 2; q = 25; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -1.1162790, tol);
    // m = 10
        // z = 0
        m = 10; q = 0; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.0, tol);
        m = 10; q = 25; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.15623992, tol);
        // z = pi/2
        m = 10; q = 0; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -1.0, tol);
        m = 10; q = 25; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -8.8269192e-1, tol);
    // m = 1
        // z = 0
        m = 1; q = 0; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.0, tol);
        m = 1; q = 25; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.91105151e-3, tol);
    // m = 5
        // z = 0
        m = 5; q = 0; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.0, tol);
        m = 5; q = 25; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 6.10694310e-1, tol);
    // m = 15
        // z = 0
        m = 15; q = 0; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.0, tol);
        m = 15; q = 25; z = 0;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.05980044, tol);
// derivatives
fun_or_der = 0;
    // m = 1
        // z = pi/2
        m = 1; q = 0; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -1.0, tol);
        m = 1; q = 25; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -7.1067415, tol);
    // m = 5
        // z = pi/2
        m = 5; q = 0; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -5.0, tol);
        m = 5; q = 25; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -7.0598845, tol);
    // m = 15
        // z = pi/2
        m = 15; q = 0; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.5e1, tol);
        m = 15; q = 25; z = %pi/2;
        y = mathieu_ang_ce( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.5744472e1, tol);
// Arm pass test
    m = 12;
    q = 34;
    z = %pi/4;
    tol = 1e-16;
    // function
    fun_or_der = 1; 
        Arm_ext = mathieu_Arm(m, q);
        y_int = mathieu_ang_ce(m, q, z, fun_or_der);
        y_ext = mathieu_ang_ce(m, q, z, fun_or_der, Arm_ext);
        assert_checkalmostequal(y_int, y_ext, tol);
    // derivative
    fun_or_der = 0; 
        Arm_ext = mathieu_Arm(m, q);
        y_int = mathieu_ang_ce(m, q, z, fun_or_der);
        y_ext = mathieu_ang_ce(m, q, z, fun_or_der, Arm_ext);
        assert_checkalmostequal(y_int, y_ext, tol);
// comparison with values from Tables 14.5(a) from Zhang & Jin
// p. 527
    tol = 1e-6;
    q = 10;
  // ce
    fun_or_der  = 1;
    // z = 0 deg
    z = 0;
        assert_checkalmostequal(mathieu_ang_ce(0, q, z, fun_or_der), 0.00762652, tol);
        assert_checkalmostequal(mathieu_ang_ce(1, q, z, fun_or_der), 0.05359875, tol);
        assert_checkalmostequal(mathieu_ang_ce(2, q, z, fun_or_der), 0.24588835, tol);
        assert_checkalmostequal(mathieu_ang_ce(3, q, z, fun_or_der), 0.70482793, tol);
        assert_checkalmostequal(mathieu_ang_ce(4, q, z, fun_or_der), 1.12710679, tol);
   // z = 45 deg
   z = %pi / 4;
        assert_checkalmostequal(mathieu_ang_ce(0, q, z, fun_or_der), 0.25041881, tol);
        assert_checkalmostequal(mathieu_ang_ce(1, q, z, fun_or_der), 0.69293798, tol);
        assert_checkalmostequal(mathieu_ang_ce(2, q, z, fun_or_der), 1.06341281, tol);
        assert_checkalmostequal(mathieu_ang_ce(3, q, z, fun_or_der), 0.76953706, tol);
        assert_checkalmostequal(mathieu_ang_ce(4, q, z, fun_or_der), -0.09231517, tol);
   // z = 90 deg
   z = %pi / 2;
        assert_checkalmostequal(mathieu_ang_ce(0, q, z, fun_or_der), 1.46866047, tol);
        assert_checkalmostequal(mathieu_ang_ce(2, q, z, fun_or_der), -0.92675926, tol);
        assert_checkalmostequal(mathieu_ang_ce(4, q, z, fun_or_der), 0.65132461, tol);
// comparison with values from Tables 14.5(b) from Zhang & Jin
// p. 527
  // ce'
    fun_or_der = 0;
    // z = 0 deg
    z = 0;
        assert_checkalmostequal(mathieu_ang_ce(0, q, z, fun_or_der), 0, tol);
        assert_checkalmostequal(mathieu_ang_ce(1, q, z, fun_or_der), 0, tol);
        assert_checkalmostequal(mathieu_ang_ce(2, q, z, fun_or_der), 0, tol);
        assert_checkalmostequal(mathieu_ang_ce(3, q, z, fun_or_der), 0, tol);
        assert_checkalmostequal(mathieu_ang_ce(4, q, z, fun_or_der), 0, tol);
   // z = 45 deg
   z = %pi / 4;
        assert_checkalmostequal(mathieu_ang_ce(0, q, z, fun_or_der), 1.06456366, tol);
        assert_checkalmostequal(mathieu_ang_ce(1, q, z, fun_or_der), 1.94003907, tol);
        assert_checkalmostequal(mathieu_ang_ce(2, q, z, fun_or_der), 0.90590998, tol);
        assert_checkalmostequal(mathieu_ang_ce(3, q, z, fun_or_der), -2.17212805, tol);
        assert_checkalmostequal(mathieu_ang_ce(4, q, z, fun_or_der), -3.51665513, tol);
   // z = 90 deg
   z = %pi / 2;
        assert_checkalmostequal(mathieu_ang_ce(1, q, z, fun_or_der), -4.85043830, tol);
        assert_checkalmostequal(mathieu_ang_ce(3, q, z, fun_or_der), 4.36839995, tol);
// Example: Even periodic Mathieu functions and their first derivatives
    f=scf(); mrgn = 50; font_sz = 4;
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ; 
    f.figure_name = 'Even periodic Mathieu functions and their first derivatives';
    z = linspace(0, 2*%pi, 100);
    fun = 1; der = 0;
    m = 0;
    subplot(2,2,1)
        plot(z, mathieu_ang_ce(m,-3,z,fun), z, mathieu_ang_ce(m,-1,z,fun), z, mathieu_ang_ce(m,0,z,fun), z, mathieu_ang_ce(m,1,z,fun), z, mathieu_ang_ce(m,3,z,fun))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$ce_0(z,q)$'; xlabel('$z$'); ylabel(ty);
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(2,2,3)
        plot(z, mathieu_ang_ce(m,-3,z,der), z, mathieu_ang_ce(m,-1,z,der), z, mathieu_ang_ce(m,0,z,der), z, mathieu_ang_ce(m,1,z,fun), z, mathieu_ang_ce(m,3,z,der))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$ce^{\ \prime}_0(z,q)$'; xlabel('$z$'); ylabel(ty);
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    m = 1;
    subplot(2,2,2)        
        plot(z, mathieu_ang_ce(m,-3,z,fun), z, mathieu_ang_ce(m,-1,z,fun), z, mathieu_ang_ce(m,0,z,fun), z, mathieu_ang_ce(m,1,z,fun), z, mathieu_ang_ce(m,3,z,fun))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$',pos=4);
        ty = '$ce_1(z,q)$'; xlabel('$z$'); ylabel(ty);
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(2,2,4)
        plot(z, mathieu_ang_ce(m,-3,z,der), z, mathieu_ang_ce(m,-1,z,der), z, mathieu_ang_ce(m,0,z,der), z, mathieu_ang_ce(m,1,z,fun), z, mathieu_ang_ce(m,3,z,der))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$ce^{\ \prime}_1(z,q)$'; xlabel('$z$'); ylabel(ty);
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    f.figure_position=[sz(1) sz(2)]; f.figure_size=[sz(3) sz(4)]; 
