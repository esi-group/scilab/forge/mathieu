// Copyright (C) 2013-2016 - N. O. Strelkov, NRU MPEI
//
// This unit test compares mathieu_membrane_mode results with known publications:
//
// 1. Wilson, Howard B., and Robert W. Scharstein. "Computing elliptic membrane high frequencies by Mathieu and Galerkin methods." Journal of Engineering Mathematics 57.1 (2007): 41-55. (online at http://scharstein.eng.ua.edu/ENGI1589.pdf or http://dx.doi.org/10.1007/s10665-006-9070-1 )
// 2. Gutiérrez-Vega, J., S. Chávez-Cerda, and Ramón Rodríguez-Dagnino. "Free oscillations in an elliptic membrane." Revista Mexicana de Fisica 45.6 (1999): 613-622. (online at http://optica.mty.itesm.mx/pmog/Papers/P001.pdf )

ieee_mod_old = ieee();
ieee(2);

// assert tolerance
    tol = 1e-9;

// 1. // Comparison with [1, Tables 1-4]
    a = 2;
    b = 1;

    N_xi = 1;
    N_eta = 1;

    options.q_delta = 10;
    options.do_print = %f;
    options.do_plot_mode = %f;
    options.show_colorbar = %f;

    // Table 1
    //     b/a = 1/2, Soft (Dirichlet) Even mode with Mc1_n(xi0, q_p) = 0
    func_name = 'Mc1'; // Even mode
    soft_or_hard = %t;

    nn = [0, 1, 2, 3, 4, 0, 5, 1, 6, 2, 3, 7, 4, 8, 5, 0, 9, 6, 1, 10, 13, 12, 14, 2, 21, 16, 24, 19];
    pp = [1, 1, 1, 1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 3, 1, 2, 3, 1, 1, 2, 2, 5, 1, 3, 1, 3];
    
    ka_article = [3.77715586, 5.01016192, 6.33353033, 7.71422171, 9.13167372, 9.97712016, 10.57302174, 11.07869692, 12.02991059, 12.22517262, 13.41144729, 13.49662286, 14.63293864, 14.96900296, 15.88565752, 16.24823969, 16.44383320, 17.16623204, 17.32773381, 17.91847260, 22.32015058, 25.31322551, 28.17189035, 30.95381867, 33.73392302, 36.08259485, 37.91528358, 40.21013472];
    
    for i=1:length(nn)
        n = nn(i);
        p = pp(i);
            [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, n, p, func_name, soft_or_hard, N_xi, N_eta, options);
        ka_scilab(i) = ka($);
    end

    assert_checkalmostequal(ka_scilab, ka_article', tol);
    clear ka_scilab ka_article


    // Table 2
    //    b/a = 1/2, Soft (Dirichlet) Odd mode with Ms1_n(xi0, q_p) = 0
    func_name = 'Ms1'; // Odd mode
    soft_or_hard = %t;
    
    nn = [1, 2, 3, 4, 5, 6, 1, 2, 7, 3, 8, 4, 9, 5, 10, 6, 1, 11, 7, 2, 6, 13, 11, 13, 15, 7, 26, 20];
    pp = [1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 3, 1, 2, 3, 3, 2, 3, 3, 3, 5, 1, 3];
    
    ka_article = [6.85176340, 7.98096836, 9.17017540, 10.40781192, 11.68407134, 12.99089339, 13.11107543, 14.19879782, 14.32175778, 15.32171724, 15.67142165, 16.47686461, 17.03566767, 17.66143700, 18.41108976, 18.87282777, 19.38694084, 19.79492161, 20.10864040, 20.46098453, 24.98869886, 27.92706712, 31.07713445, 33.61883169, 36.21103129, 38.54496121, 40.68935279, 42.87782317];

    for i=1:length(nn)
        n = nn(i);
        p = pp(i);
            [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, n, p, func_name, soft_or_hard, N_xi, N_eta, options);
            ka_scilab(i) = ka($);
    end

    assert_checkalmostequal(ka_scilab, ka_article', tol);
    clear ka_scilab ka_article

    // Table 3
    //    b/a = 1/2, Hard (Neumann) Even mode with Mc1'_n(xi0, q_p) = 0
    func_name = 'Mc1'; // Even mode
    soft_or_hard = %f;

    nn = [1, 2, 3, 4, 0, 1, 5, 2, 6, 3, 7, 4, 8, 5, 0, 9, 1, 6, 10, 2, 0, 3, 14, 8, 10, 19, 3, 15];
    pp = [1, 1, 1, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 2, 1, 3, 2, 1, 3, 3, 4, 2, 4, 4, 2, 6, 4];
    
    ka_article = [1.87357563, 3.41903131, 4.92906598, 6.41801765, 6.76024730, 7.87147773, 7.88925340, 9.04814696, 9.34361693, 10.28174582, 10.78147167, 11.56493082, 12.20349662, 12.89119042, 13.06856493, 13.61095438, 14.15137926, 14.25447208, 15.00561142, 15.26955646, 19.35924738, 22.64703345, 25.87945557, 28.54914442, 31.03110041, 33.29248637, 35.16371615, 37.47393937];

    for i=1:length(nn)
        n = nn(i);
        p = pp(i);
            [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, n, p, func_name, soft_or_hard, N_xi, N_eta, options);
            ka_scilab(i) = ka($);
    end

    assert_checkalmostequal(ka_scilab, ka_article', tol);
    clear ka_scilab ka_article

    // Table 4
    //    b/a = 1/2, Hard (Neumann) Odd mode with Ms1'_n(xi0, q_p) = 0
    func_name = 'Ms1'; // Odd mode
    soft_or_hard = %f;

    nn = [1, 2, 3, 4, 5, 6, 1, 2, 7, 3, 8, 4, 9, 5, 10, 6, 1, 11, 7, 2, 15, 3, 6, 13, 5, 25, 9, 5];
    pp = [1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 3, 1, 2, 3, 1, 4, 4, 3, 5, 1, 5, 6];
    
    ka_article = [3.53539992, 4.64124553, 5.83516485, 7.08895334, 8.38235005, 9.70150067, 9.91905186, 11.01193392, 11.03709039, 12.15024012, 12.38285996, 13.32909409, 13.73458698, 14.54396048, 15.08942048, 15.79067193, 16.21470352, 16.44544806, 17.06542284, 17.29105294, 21.86231030, 24.66128486, 28.04849016, 30.67030677, 33.14329074, 35.29333207, 37.73806113, 39.39968721];

    for i=1:length(nn)
        n = nn(i);
        p = pp(i);
            [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, n, p, func_name, soft_or_hard, N_xi, N_eta, options);
            ka_scilab(i) = ka($);
    end

    assert_checkalmostequal(ka_scilab, ka_article', tol);
    clear ka_scilab ka_article


// 2. // Comparison with [2]
    soft_or_hard = %t;

    // Table II, p. 618
    a = 0.05;
    b = 0.049;
    options.q_delta = 0.1;
    tol = 1e-4;

        // Even
        mm = [0 1 2];
        nn = [1 2];
        func_name = 'Mc1';

        ws_article = [48.590, 77.027, 103.690; 111.603, 141.065, 169.802];

        for i=1:length(mm)
            for j=1:length(nn)
            m = mm(i);
            n = nn(j);
                [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, soft_or_hard, N_xi, N_eta, options);
                ws_scilab(i,j) = ws($);
            end
        end

        assert_checkalmostequal(ws_scilab, ws_article', tol);
        clear ws_scilab ws_article

        // Odd
        mm = [1 2];
        nn = [1 2];

        func_name = 'Ms1';

        ws_article = [77.809, 103.764; 142.49, 170.103];
        
        for i=1:length(mm)
            for j=1:length(nn)
            m = mm(i);
            n = nn(j);
                [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, soft_or_hard, N_xi, N_eta, options);
                ws_scilab(i,j) = ws($);
            end
        end

        assert_checkalmostequal(ws_scilab, ws_article', tol);
        clear ws_scilab ws_article

    // Table III, p. 618
    a = 0.05;
    b = 0.03;
    options.q_delta = 10;
    tol = 1e-4;

        // Even
        mm = [0 1 2];
        nn = [1 2];
        func_name = 'Mc1';
    
        ws_article = [65.865, 91.546, 118.880; 168.496, 191.229, 214.983];
        
        for i=1:length(mm)
            for j=1:length(nn)
            m = mm(i);
            n = nn(j);
                [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, soft_or_hard, N_xi, N_eta, options);
                ws_scilab(i,j) = ws($);
            end
        end

        assert_checkalmostequal(ws_scilab, ws_article', tol);
        clear ws_scilab ws_article

        // Odd
        mm = [1 2];
        nn = [1 2];

        func_name = 'Ms1';

        ws_article = [116.513, 139.813; 220.672, 243.083];

        for i=1:length(mm)
            for j=1:length(nn)
            m = mm(i);
            n = nn(j);
                [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, soft_or_hard, N_xi, N_eta, options);
                ws_scilab(i,j) = ws($);
            end
        end

        assert_checkalmostequal(ws_scilab, ws_article', tol);
        clear ws_scilab ws_article

    // Table IV, p. 618
    a = 0.05;
    b = 0.02;
    options.q_delta = 10;
    tol = 1e-5;

        // Even
        mm = [0 1 2];
        nn = [1 2];

        func_name = 'Mc1';

        ws_article = [90.514, 114.080, 139.416; 246.363, 267.814, 290.020];

        for i=1:length(mm)
            for j=1:length(nn)
            m = mm(i);
            n = nn(j);
                [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, soft_or_hard, N_xi, N_eta, options);
                ws_scilab(i,j) = ws($);
            end
        end

        assert_checkalmostequal(ws_scilab, ws_article', tol);
        clear ws_scilab ws_article

        // Odd
        mm = [1 2];
        nn = [1 2];
        
        func_name = 'Ms1';
        
        ws_article = [168.078, 189.992; 324.782, 346.009];

        for i=1:length(mm)
            for j=1:length(nn)
            m = mm(i);
            n = nn(j);
                [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, soft_or_hard, N_xi, N_eta, options);
                ws_scilab(i,j) = ws($);
            end
        end

        assert_checkalmostequal(ws_scilab, ws_article', tol);
        clear ws_scilab ws_article

    // Table V, p. 618
    a = 0.05;
    b = 0.008;
    options.q_delta = 1;
    tol = 1e-5;

        // Even
        mm = [0 1 2];
        n = 1;

        func_name = 'Mc1';

        ws_article = [206.955, 228.125, 250.204];

        for i=1:length(mm)
            m = mm(i);
                [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, soft_or_hard, N_xi, N_eta, options);
                ws_scilab(i) = ws($);
        end

        assert_checkalmostequal(ws_scilab, ws_article', tol);
        clear ws_scilab ws_article

        // Odd
        mm = [1 2];
        n = 1;
        
        func_name = 'Ms1';
        
        ws_article = [403.009, 423.619];
        
        for i=1:length(mm)
            m = mm(i);
                [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, soft_or_hard, N_xi, N_eta, options);
                ws_scilab(i) = ws($);
        end

        assert_checkalmostequal(ws_scilab, ws_article', tol);
        clear ws_scilab ws_article
        clear options

// Example:
    // calculate two Soft modes of elliptic membrane - Even (m=0, n=1) and Odd (m=1, n=1)
    a = 0.05;
    b = 0.03;
    N_xi = 101;
    N_eta = 101;
    options.font_size_axes = 4;

    f_wh = get(0, 'screensize_px'); margin = 50;
    f_h = figure('Background',-2);

    subplot(1,2,1);
        mathieu_membrane_mode(a, b, 0, 1, 'Mc1', %t, N_xi, N_eta, options);
    subplot(1,2,2);
        mathieu_membrane_mode(a, b, 1, 1, 'Ms1', %t, N_xi, N_eta, options);

    f_h.figure_size = [f_wh(3) - 2*margin, f_wh(4) - 2*margin];
    f_h.figure_position = [margin margin];

// set old value of ieee
ieee(ieee_mod_old)
