// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//

// comparison with values from Tables 20.1 and 20.2 from M. Abramowitz and I. A. Stegun

[Arm, ar] = mathieu_Arm(0,25);
assert_checkalmostequal(Arm(1), 0.429741038, 1.e-6);
assert_checkalmostequal(ar, -40.25677955, 1.e-6);

[Arm, ar] = mathieu_Arm(2,25);
assert_checkalmostequal(Arm(1), 0.330865777, 1.e-6);
assert_checkalmostequal(ar, -3.52216473, 1.e-6);

[Arm, ar] = mathieu_Arm(1,25);
assert_checkalmostequal(Arm(1), 0.391252265, 1.e-6);
assert_checkalmostequal(ar, -21.31489969, 1.e-6);

[Arm, ar] = mathieu_Arm(15,25);
assert_checkalmostequal(Arm(1), 0.0000047, 1.e-2);
assert_checkalmostequal(ar, 226.40072004, 1.e-6);

// Table 20.1 1/4
    abs_tol = 1e-8;
    as_r0 = [%eps -5.80004602 -13.93697996 -22.51303776 -31.31339007 -40.25677955];
    as_r2 = [4 7.44910974 7.71736985 5.07798320 1.15428288 -3.52216473];
    as_r10 = [100 100.12636922 100.50677002 101.14520345 102.04891602 103.23020480];
    
    for r=[0, 2, 10]
        i = 1;   
        for q=0:5:25
            [Arm,a]=mathieu_Arm(r, q);
            as_table = evstr(sprintf('%s%1d', 'as_r', r));
            a_table = as_table(i);
            assert_checkalmostequal(a_table, a, abs_tol);
            i = i + 1;
        end
    end

// Table 20.1 2/4
    as_r1 = [1 +1.85818754 -2.39914240 -8.10110513 -14.49130142 -21.31489969];
    as_r5 = [25 25.54997175 27.70376873 31.95782125 36.64498973 40.05019099];
    as_r15 = [225 225.05581248 225.22335698 225.50295624 225.89515341 226.40072004];
    
    for r=[1, 5, 15]
        i = 1;   
        for q=0:5:25
            [Arm,a]=mathieu_Arm(r, q);
            as_table = evstr(sprintf('%s%1d', 'as_r', r));
            a_table = as_table(i);
            assert_checkalmostequal(a_table, a, abs_tol);
            i = i + 1;
        end
    end


// comparison with values from Tables 14.1 from Zhang & Jin
// p. 521
    q = 50;
    tol = 1e-7;
    [Arm, ar] = mathieu_Arm(0,q);
    assert_checkalmostequal(ar, -86.1125385, tol);
    
    [Arm, ar] = mathieu_Arm(1,q);
    assert_checkalmostequal(ar, -58.8674030, tol);
    
    [Arm, ar] = mathieu_Arm(2,q);
    assert_checkalmostequal(ar, -32.7177617, tol);
    
    [Arm, ar] = mathieu_Arm(3,q);
    assert_checkalmostequal(ar, -7.7449720, tol);
    
    [Arm, ar] = mathieu_Arm(4,q);
    assert_checkalmostequal(ar, 15.9452336, tol);

    
// p. 522
  // q = 1
  q = 1;
  tol = 1e-6;
    [Arm, ar] = mathieu_Arm(0,q);
    assert_checkalmostequal(ar, -0.455139, tol);
    
    [Arm, ar] = mathieu_Arm(20,q);
    assert_checkalmostequal(ar, 400.001253, tol);

  // q = 10
  q = 10;
  tol = 1e-6;  
    [Arm, ar] = mathieu_Arm(0,q);
    assert_checkalmostequal(ar, -13.936980, tol);
    
    [Arm, ar] = mathieu_Arm(20,q);
    assert_checkalmostequal(ar, 400.125338, tol);
    
  // q = 100
  q = 100;
  tol = 1e-6;  
    [Arm, ar] = mathieu_Arm(0,q);
    assert_checkalmostequal(ar, -180.253249, tol);
    
    [Arm, ar] = mathieu_Arm(20,q);
    assert_checkalmostequal(ar, 412.796655, tol);
    
  // q = 200
  q = 200;
  tol = 1e-6;  
    [Arm, ar] = mathieu_Arm(0,q);
    assert_checkalmostequal(ar, -371.967999, tol);
    
    [Arm, ar] = mathieu_Arm(20,q);
    assert_checkalmostequal(ar, 456.125190, tol);
    
// comparison with values from Tables 14.3 from Zhang & Jin
// p. 525

q = 10;
tol = 1e-7;
    // m = 0
    [Arm, ar] = mathieu_Arm(0,q);
    assert_checkalmostequal(Arm(1:10), [4.8777536e-1, -6.7981154e-1,  2.4382588e-1, -5.0129505e-2,  6.5057306e-3, ...
                                       -5.7419432e-4,  3.6466089e-5, -1.7400756e-6,  6.4533085e-8, -1.9109857e-9]', tol);
    // m = 7
    [Arm, ar] = mathieu_Arm(7,q);
    assert_checkalmostequal(Arm(1:10), [2.5667160e-2, 1.0026278e-1,  3.8603861e-1, 8.6725311e-1, -2.9387809e-1, ...
                                        4.1927193e-2,-3.5422154e-3,  2.0297107e-4,-8.5061995e-6,  2.7379172e-7]', tol);
                                        
// Example: Expansion coefficients Arm as continous functions of q
    f=scf(); mrgn = 50; font_sz = 4; f.figure_name='Expansion coefficients Arm as continous functions of q';
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ; 

    q = linspace(0, 40, 40);
    Arm2n = zeros(6, length(q));
    Arm2n1 = zeros(6, length(q));
    for i=1:length(q)
        A2ns = mathieu_Arm(0, q(i));
        A2n1s = mathieu_Arm(1, q(i));
        Arm2n(1:6, i) = A2ns(1:6);
        Arm2n1(1:6, i) = A2n1s(1:6);
    end
    subplot(1,2,1)
        plot(q', Arm2n');
        legend('$A_0^{(0)}$', '$A_2^{(0)}$', '$A_4^{(0)}$', '$A_6^{(0)}$', '$A_8^{(0)}$', '$A_{10}^{(0)}$');
        xlabel('$q$');
        ylabel('$A_m^{(0)}$');
        xgrid
        h = gca(); h.margins=[0.15 0.05 0.05 0.12]; h.font_size = font_sz - 1;
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(1,2,2);
        plot(q', Arm2n1');
        legend('$A_1^{(1)}$', '$A_3^{(1)}$', '$A_5^{(1)}$', '$A_7^{(1)}$', '$A_9^{(1)}$', '$A_{11}^{(1)}$');
        xlabel('$q$');
        ylabel('$A_m^{(1)}$');        
        xgrid
        h = gca(); h.margins=[0.15 0.05 0.05 0.12]; h.font_size = font_sz - 1;
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    f.figure_position=[sz(1) sz(2)];
    f.figure_size=[sz(3) sz(4)];
