// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//

// comparison with tables from
// 'Tables relating to the radial Mathieu functions (vols. 1 and 2) by G. Blanch, D.S. Clemm.'.
// online at HathiTrust [http://catalog.hathitrust.org/Record/000585710]
tol = 1.e-6;

// Mc1
  kind = 1;
  // r=0
    // p.3 (44)
    r=0; q= 0.5; x= 0.5;
    Mc0_x05_q05_table = 0.6817323;
    Mc0_x05_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc0_x05_q05_here, Mc0_x05_q05_table, tol);

    // p.3 (44)
    r=0; q= 0.5; x= 1.00;
    Mc0_x1_q05_table = 0.2826959;
    Mc0_x1_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc0_x1_q05_table, Mc0_x1_q05_here, tol);

    // p.5 (46)
    r=0; q= 1.0; x= 1.00;
    Mc0_x1_q1_table = -0.1093206;
    Mc0_x1_q1_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc0_x1_q1_table, Mc0_x1_q1_here, tol);

  // 1
    // p.7 (48)
    r=1; q= 0.5; x= 0.5;
    Mc1_x05_q05_table = 7.0710678e-1*cosh(r*x)*0.7701805;
    Mc1_x05_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc1_x05_q05_table, Mc1_x05_q05_here, tol);
    
    // p.7 (48)
    r=1; q= 0.5; x= 1.00;
    Mc1_x1_q05_table = 7.0710678e-1*cosh(r*x)*0.5578048;
    Mc1_x1_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc1_x1_q05_table, Mc1_x1_q05_here, tol);

    // p.9 (50)
    r=1; q= 1.0; x= 1.00;
    Mc1_x1_q1_table = 1.0*cosh(r*x)*0.2638800;
    Mc1_x1_q1_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc1_x1_q1_table, Mc1_x1_q1_here, tol);

  // 2
    // p.11 (52)
    r=2; q= 0.5; x= 1.00;
    Mc2_x1_q05_table = 0.125*cosh(r*x)*0.7455070;
    Mc2_x1_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc2_x1_q05_table, Mc2_x1_q05_here, tol);
    
    // p.13 (54)
    r=2; q= 1.0; x= 1.00;
    Mc2_x1_q1_table = 0.25*cosh(r*x)*0.5242034;
    Mc2_x1_q1_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc2_x1_q1_table, Mc2_x1_q1_here, tol);

  // 3
    // p.15 (56)
    r=3; q= 0.5; x= 1.00;
    Mc3_x1_q05_table = 1.4731391e-2*cosh(r*x)*0.7971791;
    Mc3_x1_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc3_x1_q05_table, Mc3_x1_q05_here, tol);
        
    // p.17 (58)
    r=3; q= 1.0; x= 1.00;
    Mc3_x1_q1_table = 4.1666667e-2*cosh(r*x)*0.6266031;
    Mc3_x1_q1_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc3_x1_q1_table, Mc3_x1_q1_here, tol);

  // 14
    // p.87 (128)
    r=14; q= 0.5; x= 0.5;
    Mc14_x05_q05_table = 1.0939355e-17*cosh(r*x)*0.9810408;
    Mc14_x05_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc14_x05_q05_table, Mc14_x05_q05_here, tol);

    // p.89 (130)
    r=14; q= 1.0; x= 1.00;
    Mc14_x1_q1_table = 1.4002375e-15*cosh(r*x)*0.8859722;
    Mc14_x1_q1_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc14_x1_q1_table, Mc14_x1_q1_here, tol);
        
  // 15
    // p.95 (136)
    r=15; q= 0.5; x= 0.5;
    Mc15_x05_q05_table = 2.5784308e-19*cosh(r*x)*0.9821870;
    Mc15_x05_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc15_x05_q05_table, Mc15_x05_q05_here, tol);

    // p.97 (138)
    r=15; q= 1.0; x= 1.00;
    Mc15_x1_q1_table = 4.6674583e-17*cosh(r*x)*0.8927331;
    Mc15_x1_q1_here    = mathieu_rad_mc(r, q, x, 1, kind);
    assert_checkalmostequal(Mc15_x1_q1_table, Mc15_x1_q1_here, tol);


// Mc1'
  kind = 1;
  // 0
    // p.101 (142)
    r=0; q= 0.5; x= 0.5;
    Mc0p_x05_q05_table = -0.4905654;
    Mc0p_x05_q05_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc0p_x05_q05_table, Mc0p_x05_q05_here, tol);

    // p.101 (142)
    r=0; q= 0.5; x= 1.00;
    Mc0p_x1_q05_table = -1.1079497;
    Mc0p_x1_q05_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc0p_x1_q05_table, Mc0p_x1_q05_here, tol);

    // p.103 (144)
    r=0; q= 1.0; x= 1.00;
    Mc0p_x1_q1_table = -1.2598556;
    Mc0p_x1_q1_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc0p_x1_q1_table, Mc0p_x1_q1_here, tol);
        
  // 1
    // p.105 (146)
    r=1; q= 0.5; x= 0.5;
    Mc1p_x05_q05_table = (7.0710678e-1)*sinh(r*x)*0.2340243;
    Mc1p_x05_q05_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc1p_x05_q05_table, Mc1p_x05_q05_here, tol);

    // p.105 (146)
    r=1; q= 0.5; x= 1.00;
    Mc1p_x1_q05_table = (7.0710678e-1)*sinh(r*x)*(-0.2655553);
    Mc1p_x1_q05_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc1p_x1_q05_table, Mc1p_x1_q05_here, tol);

    // p.107 (148)
    r=1; q= 1.0; x= 1.00;
    Mc1p_x1_q1_table = 1*sinh(r*x)*(-0.8382471);
    Mc1p_x1_q1_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc1p_x1_q1_table, Mc1p_x1_q1_here, tol);
        
  // 2
    // p.109 (150)
    r=2; q= 0.5; x= 1.00;
    Mc2p_x1_q05_table = 0.25*sinh(r*x)*0.4593862;
    Mc2p_x1_q05_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc2p_x1_q05_table, Mc2p_x1_q05_here, tol);

    // p.111 (152)
    r=2; q= 1.0; x= 1.00;
    Mc2p_x1_q1_table = 0.5*sinh(r*x)*0.0827560;
    Mc2p_x1_q1_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc2p_x1_q1_table, Mc2p_x1_q1_here, tol);

  // 3
    // p.113 (154)
    r=3; q= 0.5; x= 1.00;
    Mc3p_x1_q05_table = 4.4194174e-2*sinh(r*x)*0.6609173;
    Mc3p_x1_q05_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc3p_x1_q05_table, Mc3p_x1_q05_here, tol);

    // p.115 (156)
    r=3; q= 1.0; x= 1.00;
        Mc3p_x1_q1_table = 0.125*sinh(r*x)*0.3999758;
        Mc3p_x1_q1_here    = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc3p_x1_q1_table, Mc3p_x1_q1_here, tol);

  // 14
    // p.185 (230)
    r=14; q= 0.5; x= 0.5;
    Mc14p_x05_q05_table = 1.5315098e-16*sinh(r*x)*0.9773656;
    Mc14p_x05_q05_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc14p_x05_q05_table, Mc14p_x05_q05_here, tol);
        
    // p.187 (232)
    r=14; q= 1.0; x= 1.00;
    Mc14p_x1_q1_table = 1.9603325e-14*sinh(r*x)*0.8699341;
    Mc14p_x1_q1_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc14p_x1_q1_table, Mc14p_x1_q1_here, tol);
        
  // 15
    // p.193 (238)
    r=15; q= 0.5; x= 0.5;
    Mc15p_x05_q05_table = 3.8676462e-18*sinh(r*x)*0.9789722;
    Mc15p_x05_q05_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc15p_x05_q05_table, Mc15p_x05_q05_here, tol);
        
    // p.195 (240)
    r=15; q= 1.0; x= 1.00;
    Mc15p_x1_q1_table = 7.0011875e-16*sinh(r*x)*0.8786082;
    Mc15p_x1_q1_here    = mathieu_rad_mc(r, q, x, 0, kind);
    assert_checkalmostequal(Mc15p_x1_q1_table, Mc15p_x1_q1_here, tol);

// Mc2
  kind = 2;
  // 0  
    // p.v.2 3 (32)
    r=0; q= 0.5; x= 0.5;
        Mc2_0_x05_q05_table = 0.1637951;
        Mc2_0_x05_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_0_x05_q05_table, Mc2_0_x05_q05_here, tol);
    
    // p.v.2 3 (32)
    r=0; q= 0.5; x= 1.00;    
        Mc2_0_x1_q05_table = 0.4870838;
        Mc2_0_x1_q05_here    = mathieu_rad_mc(r, q, x, 1, kind);
        assert_checkalmostequal(Mc2_0_x1_q05_table, Mc2_0_x1_q05_here, tol);
        
    // p.v.2 5 (34)
    r=0; q= 1.0; x= 1.00;    
        Mc2_0_x1_q1_table = 0.4601377;
        Mc2_0_x1_q1_here    = mathieu_rad_mc(r, q, x, 1, kind);
        assert_checkalmostequal(Mc2_0_x1_q1_table, Mc2_0_x1_q1_here, tol);

  // 1
    // p.v.2 7 (36)
    r=1; q= 0.5; x= 0.5;
        Mc2_1_x05_q05_table = exp(-r*x) * -0.6920969 / 7.0710678e-1;
        Mc2_1_x05_q05_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        Mc2_1_x05_q05d      = Mc2_1_x05_q05_table - Mc2_1_x05_q05_here;
        assert_checkalmostequal(Mc2_1_x05_q05_table, Mc2_1_x05_q05_here, tol);
        
    // p.v.2 7 (36)
    r=1; q= 0.5; x= 1.00;
        Mc2_1_x1_q05_table = exp(-r*x) * -0.1734008 / 7.0710678e-1;
        Mc2_1_x1_q05_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_1_x1_q05_table, Mc2_1_x1_q05_here, tol);

    // p.v.2 9 (38)
    r=1; q= 1.0; x= 1.00;
        Mc2_1_x1_q1_table = exp(-r*x) * 0.8218936 / 1.0;
        Mc2_1_x1_q1_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_1_x1_q1_table, Mc2_1_x1_q1_here, tol);
        
  // 2
    // p.v.2 11 (40)
    r=2; q= 0.5; x= 1.0;
        Mc2_2_x1_q05_table = exp(-r*x) * -1.2083001 / 0.25;
        Mc2_2_x1_q05_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_2_x1_q05_table, Mc2_2_x1_q05_here, tol);
        
    // p.v.2 13 (42)
    r=2; q= 1.0; x= 1.0;
        Mc2_2_x1_q1_table = exp(-r*x) * -0.9510689 / 0.5;
        Mc2_2_x1_q1_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_2_x1_q1_table, Mc2_2_x1_q1_here, tol);
        
  // 3
    // p.v.2 15 (44)
    r=3; q= 0.5; x= 1.0;
        Mc2_3_x1_q05_table = exp(-r*x) * -1.0789940 / 4.4194174e-2;
        Mc2_3_x1_q05_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_3_x1_q05_table, Mc2_3_x1_q05_here, tol);
        
    // p.v.2 15 (44)
    r=3; q= 1.0; x= 1.0;
        Mc2_3_x1_q1_table = exp(-r*x) * -1.6309630 / 1.25e-1;
        Mc2_3_x1_q1_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_3_x1_q1_table, Mc2_3_x1_q1_here, tol);
        
  // 14
    // p.v.2 83 (112)
    r=14; q= 0.5; x= 0.5;
        Mc2_14_x05_q05_table = exp(-r*x) * -0.6515049 / 1.5315098e-16;
        Mc2_14_x05_q05_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_14_x05_q05_table, Mc2_14_x05_q05_here, tol);
        
    // p.v.2 87 (118)
    r=14; q= 1.0; x= 1.0;
        Mc2_14_x1_q1_table = exp(-r*x) * -0.7328347 / 1.9603325e-14;
        Mc2_14_x1_q1_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_14_x1_q1_table, Mc2_14_x1_q1_here, tol);
        
  // 15
    // p.v.2 91 (122)
    r=15; q= 0.5; x= 0.5;
        Mc2_15_x05_q05_table = exp(-r*x) * -0.6504090 / 3.8676462e-18;
        Mc2_15_x05_q05_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_15_x05_q05_table, Mc2_15_x05_q05_here, tol);
        
    // p.v.2 97 (128)
    r=15; q= 1.0; x= 1.0;
        Mc2_15_x1_q1_table = exp(-r*x) * -0.7254014 / 7.0011875e-16;
        Mc2_15_x1_q1_here  = mathieu_rad_mc(r, q, x, 1, kind);        
        assert_checkalmostequal(Mc2_15_x1_q1_table, Mc2_15_x1_q1_here, tol);
      
        
 // Mc2`
 kind = 2;
  // 0
    // p.v2. 101 (132)
    r=0; q= 0.5; x= 0.5;
        Mc2_0p_x05_q05_table = 0.8159619;
        Mc2_0p_x05_q05_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_0p_x05_q05_table, Mc2_0p_x05_q05_here, tol);

    // p.v2. 101 (132)
    r=0; q= 0.5; x= 1.00;
        Mc2_0p_x1_q05_table = 0.3429671;
        Mc2_0p_x1_q05_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_0p_x1_q05_table, Mc2_0p_x1_q05_here, tol);

    // p.v2. 103 (134)
    r=0; q= 1.0; x= 1.00;
        Mc2_0p_x1_q1_table = -0.5206039;
        Mc2_0p_x1_q1_here    = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_0p_x1_q1_table, Mc2_0p_x1_q1_here, tol);
        
  // 1
    // p.v2. 105 (136)
    r=1; q= 0.5; x= 0.5;
        Mc2_1p_x05_q05_table = exp(-r*x) * 1.1113819 / 7.0710678e-1;
        Mc2_1p_x05_q05_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_1p_x05_q05_table, Mc2_1p_x05_q05_here, tol);

    // p.v2. 105 (136)
    r=1; q= 0.5; x= 1.00;
        Mc2_1p_x1_q05_table = exp(-r*x) * 2.0733689 / 7.0710678e-1;
        Mc2_1p_x1_q05_here    = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_1p_x1_q05_table, Mc2_1p_x1_q05_here, tol);

    // p.v2. 107 (138)
    r=1; q= 1.0; x= 1.00;
        Mc2_1p_x1_q1_table = exp(-r*x) * 2.2615033 / 1;
        Mc2_1p_x1_q1_here    = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_1p_x1_q1_table, Mc2_1p_x1_q1_here, tol);
        
  // 2
    // p.v2. 109 (140)
    r=2; q= 0.5; x= 1.00;
        Mc2_2p_x1_q05_table = exp(-r*x) * 0.9593872 / 1.25e-1;
        Mc2_2p_x1_q05_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_2p_x1_q05_table, Mc2_2p_x1_q05_here, tol);

    // p.v2. 111 (152)
    r=2; q= 1.0; x= 1.00;
        Mc2_2p_x1_q1_table = exp(-r*x) * 2.2404725 / 2.5e-1;
        Mc2_2p_x1_q1_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_2p_x1_q1_table, Mc2_2p_x1_q1_here, tol);

  // 3
    // p.v2. 113 (144)
    r=3; q= 0.5; x= 1.00;
        Mc2_3p_x1_q05_table = exp(-r*x) * 0.7030943 / 1.4731391e-2;
        Mc2_3p_x1_q05_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_3p_x1_q05_table, Mc2_3p_x1_q05_here, tol);

    // p.v2. 115 (146)
    r=3; q= 1.0; x= 1.00;
        Mc2_3p_x1_q1_table = exp(-r*x) * 0.9910127 / 4.1666667e-2;
        Mc2_3p_x1_q1_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_3p_x1_q1_table, Mc2_3p_x1_q1_here, tol);

  // 14
    // p.v2. 185 (216)
    r=14; q= 0.5; x= 0.5;
        Mc2_14p_x05_q05_table = exp(-r*x) * 0.6487815 / 1.0939355e-17;
        Mc2_14p_x05_q05_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_14p_x05_q05_table, Mc2_14p_x05_q05_here, tol);
        
    // p.v2. 187 (218)
    r=14; q= 1.0; x= 1.00;
        Mc2_14p_x1_q1_table = exp(-r*x) * 0.7175413 / 1.4002375e-15;
        Mc2_14p_x1_q1_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_14p_x1_q1_table, Mc2_14p_x1_q1_here, tol);
        
  // 15
    // p.v2. 189 (220)
    r=15; q= 0.5; x= 0.5;
        Mc2_15p_x05_q05_table = exp(-r*x) * 0.6480510 / 2.5784308e-19;
        Mc2_15p_x05_q05_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_15p_x05_q05_table, Mc2_15p_x05_q05_here, tol);
        
    // p.192 (237)
    r=15; q= 1.0; x= 1.00;
        Mc2_15p_x1_q1_table = exp(-r*x) * 0.7123024 / 4.6674583e-17;
        Mc2_15p_x1_q1_here  = mathieu_rad_mc(r, q, x, 0, kind);
        assert_checkalmostequal(Mc2_15p_x1_q1_table, Mc2_15p_x1_q1_here, tol);
        
// comparison with values from Tables 14.7(a) from Zhang & Jin
  // Mc1 function
  kind = 1;
    // p. 529 
    q = 10;
    z = 0.0;
    fun_or_der = 1;

    assert_checkalmostequal(mathieu_rad_mc(0, q, z, fun_or_der, kind), 0.33212261, tol);
    assert_checkalmostequal(mathieu_rad_mc(1, q, z, fun_or_der, kind), 0.37120315, tol);
    assert_checkalmostequal(mathieu_rad_mc(2, q, z, fun_or_der, kind), 0.44187265, tol);
    assert_checkalmostequal(mathieu_rad_mc(3, q, z, fun_or_der, kind), 0.54673793, tol);
    assert_checkalmostequal(mathieu_rad_mc(4, q, z, fun_or_der, kind), 0.46710205, tol);
    
    z = 4.0;
    assert_checkalmostequal(mathieu_rad_mc(0, q, z, fun_or_der, kind), -0.03484294, tol);
    assert_checkalmostequal(mathieu_rad_mc(1, q, z, fun_or_der, kind), 0.04853565, tol);
    assert_checkalmostequal(mathieu_rad_mc(2, q, z, fun_or_der, kind), 0.03789729, tol);
    assert_checkalmostequal(mathieu_rad_mc(3, q, z, fun_or_der, kind), -0.04658659, tol);
    assert_checkalmostequal(mathieu_rad_mc(4, q, z, fun_or_der, kind), -0.03971251, tol);

// comparison with values from Tables 14.7(b) from Zhang & Jin
  // Mc1 derivative
  kind = 1;
    // p. 529 
    q = 10;
    z = 0.2;
    fun_or_der = 0;
    assert_checkalmostequal(mathieu_rad_mc(0, q, z, fun_or_der, kind), -1.79807350, tol);
    assert_checkalmostequal(mathieu_rad_mc(1, q, z, fun_or_der, kind), -1.45362341, tol);
    assert_checkalmostequal(mathieu_rad_mc(2, q, z, fun_or_der, kind), -1.03827957, tol);
    assert_checkalmostequal(mathieu_rad_mc(3, q, z, fun_or_der, kind), -0.53215033, tol);
    assert_checkalmostequal(mathieu_rad_mc(4, q, z, fun_or_der, kind), 0.05306926, tol);
    
    z = 4.0;
    assert_checkalmostequal(mathieu_rad_mc(0, q, z, fun_or_der, kind), -8.56946493, tol);
    assert_checkalmostequal(mathieu_rad_mc(1, q, z, fun_or_der, kind), -6.32436206, tol);
    assert_checkalmostequal(mathieu_rad_mc(2, q, z, fun_or_der, kind), 8.17248010, tol);
    assert_checkalmostequal(mathieu_rad_mc(3, q, z, fun_or_der, kind), 6.74820042, tol);
    assert_checkalmostequal(mathieu_rad_mc(4, q, z, fun_or_der, kind), -7.91092549, tol);
    
// comparison with values from Tables 14.9(a) from Zhang & Jin
  // Mc2 function
  kind = 2;
    // p. 531
    q = 10;
    z = 0.0;
    fun_or_der = 1;

    assert_checkalmostequal(mathieu_rad_mc(0, q, z, fun_or_der, kind), -0.00001841, tol*100);
    assert_checkalmostequal(mathieu_rad_mc(1, q, z, fun_or_der, kind), -0.00090264, tol*100);
    assert_checkalmostequal(mathieu_rad_mc(2, q, z, fun_or_der, kind), -0.01903346, tol);
    assert_checkalmostequal(mathieu_rad_mc(3, q, z, fun_or_der, kind), -0.18198927, tol);
    assert_checkalmostequal(mathieu_rad_mc(4, q, z, fun_or_der, kind), -0.63294937, tol);
    
    z = 4.0;
    assert_checkalmostequal(mathieu_rad_mc(0, q, z, fun_or_der, kind), 0.04972262, tol);
    assert_checkalmostequal(mathieu_rad_mc(1, q, z, fun_or_der, kind), 0.03648799, tol);
    assert_checkalmostequal(mathieu_rad_mc(2, q, z, fun_or_der, kind), -0.04745002, tol);
    assert_checkalmostequal(mathieu_rad_mc(3, q, z, fun_or_der, kind), -0.03895998, tol);
    assert_checkalmostequal(mathieu_rad_mc(4, q, z, fun_or_der, kind), 0.04595056, tol);

// comparison with values from Tables 14.9(b) from Zhang & Jin
  // Mc2 derivative
  kind = 2;
    // p. 531 
    q = 10;
    z = 0.0;
    fun_or_der = 0;
    assert_checkalmostequal(mathieu_rad_mc(0, q, z, fun_or_der, kind), 1.91682151, tol);
    assert_checkalmostequal(mathieu_rad_mc(1, q, z, fun_or_der, kind), 1.71501716, tol);
    assert_checkalmostequal(mathieu_rad_mc(2, q, z, fun_or_der, kind), 1.44073134, tol);
    assert_checkalmostequal(mathieu_rad_mc(3, q, z, fun_or_der, kind), 1.16439658, tol);
    assert_checkalmostequal(mathieu_rad_mc(4, q, z, fun_or_der, kind), 1.36291369, tol);
    
    z = 4.0;
    assert_checkalmostequal(mathieu_rad_mc(0, q, z, fun_or_der, kind), -6.04207114, tol);
    assert_checkalmostequal(mathieu_rad_mc(1, q, z, fun_or_der, kind), 8.36202912, tol);
    assert_checkalmostequal(mathieu_rad_mc(2, q, z, fun_or_der, kind), 6.56604945, tol);
    assert_checkalmostequal(mathieu_rad_mc(3, q, z, fun_or_der, kind), -8.02183724, tol);
    assert_checkalmostequal(mathieu_rad_mc(4, q, z, fun_or_der, kind), -6.87713672, tol);

// Example: Mc0(1) and its derivative for comparison with Fig. 20.11-12 of Abramowitz-Stegun [1]
    f=scf(); mrgn = 50; font_sz = 4;
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ;
    f.figure_name = 'Mc0(1) and its derivative for comparison with Fig. 20.11-12 of Abramowitz-Stegun';

    z = linspace(0, 2, 100);
    fun = 1; der = 0;
    
    m = 0;
    subplot(1,2,1)
        plot(z, sqrt(%pi/2)*mathieu_rad_mc(m, 0.25, z, fun), z, sqrt(%pi/2)*mathieu_rad_mc(m, 0.75, z, fun), z, sqrt(%pi/2)*mathieu_rad_mc(m, 1.5, z, fun),z, sqrt(%pi/2)*mathieu_rad_mc(m, 3.75, z, fun), z, sqrt(%pi/2)*mathieu_rad_mc(m, 5.25, z, fun))
        xgrid
        ty1 = '$\sqrt{{\pi}/2} Mc^{(1)}_0(z,q)$'; xlabel('$z$'); ylabel(ty1);
        legend('$q = 0.25$','$q = 0.75$','$q = 1.5$','$q = 3.75$','$q = 5.25$');
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;

    subplot(1,2,2)
        plot(z, sqrt(%pi/2)*mathieu_rad_mc(m, 0.25, z, der), z, sqrt(%pi/2)*mathieu_rad_mc(m, 0.75, z, der), z, sqrt(%pi/2)*mathieu_rad_mc(m, 1.5, z, der),z, sqrt(%pi/2)*mathieu_rad_mc(m, 3.75, z, der), z, sqrt(%pi/2)*mathieu_rad_mc(m, 5.25, z, der))
        xgrid
        ty1 = '$\sqrt{{\pi}/2} Mc^{\ \prime\ (1)}_0(z,q)$'; xlabel('$z$'); ylabel(ty1);
        legend('$q = 0.25$','$q = 0.75$','$q = 1.5$','$q = 3.75$','$q = 5.25$', pos=2);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    f.figure_position=[sz(1) sz(2)]; f.figure_size=[sz(3) sz(4)];
