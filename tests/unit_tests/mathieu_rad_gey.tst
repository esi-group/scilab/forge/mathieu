// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//

// comparison with values from Tables 14.4, 14.6, 14.10 from Zhang & Jin
tol = 1e-5;

// Gey and Gey' ( calculating by formulas ~20.6.15 from Abramowitz-Stegun)
    m = 1; z = 0; q = 10; fun = 1; der = 0;
    se2n1_p_0deg     = 0.04402257; // table 14.6(b), p. 528
    se2n1_90deg      = 1.46875566;  // table 14.6(a), p. 528
    B1              = 8.9028653e-1;// table 14.4, p. 526
    Ms2             = -0.33212370; // table 14.10(a), p. 532
    Ms2_p           = 0.00010626;  // table 14.10(b), p. 532
    Gey_from_tables = (se2n1_p_0deg * se2n1_90deg / B1) * Ms2 / sqrt(q);
    Gey_p_from_tables = (se2n1_p_0deg * se2n1_90deg / B1) * Ms2_p / sqrt(q);

    assert_checkalmostequal(Gey_from_tables, mathieu_rad_gey(m, q, z, fun), tol);
    assert_checkalmostequal(Gey_p_from_tables, mathieu_rad_gey(m, q, z, der), tol*10);

// Gey and Gey' ( calculating by formulas ~20.6.15 from Abramowitz-Stegun)
    m = 2; z = 0; q = 10; fun = 1; der = 0;
    se2n_p_0deg     = 0.24882284;  // table 14.6(b), p. 528
    se2n_p_90deg    = -4.86342207; // table 14.6(a), p. 528
    B2              = 8.3390736e-1;// table 14.4, p. 526
    Ms2             = -0.37128233; // table 14.10(a), p. 532
    Ms2_p           = 0.00419071;  // table 14.10(b), p. 532
    Gey_from_tables = -(se2n_p_0deg * se2n_p_90deg / B2) * Ms2 / (q);
    Gey_p_from_tables = -(se2n_p_0deg * se2n_p_90deg / B2) * Ms2_p / (q);

    assert_checkalmostequal(Gey_from_tables, mathieu_rad_gey(m, q, z, fun), tol);
    assert_checkalmostequal(Gey_p_from_tables, mathieu_rad_gey(m, q, z, der), tol);

// Example: Gey1 (for comparison with Fig. 4d, p. 236 of the article J. C. Gutiérrez-Vega, et al. Mathieu functions, a visual approach [4]) and Gey2 with first derivative
    f=scf(); mrgn = 50; font_sz = 4;
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ;
    f.figure_name = 'Gey1 (for comparison with Fig. 4d, p. 236 of the article J. C. Gutiérrez-Vega, et al. Mathieu functions, a visual approach) and Gey2 with first derivative';

    z = linspace(0, 2, 100);
    fun = 1; der = 0;
    
    m = 1;
    subplot(2,2,1);
        plot(z, mathieu_rad_gey(m, 1, z, fun), 'r', z, mathieu_rad_gey(m, 2, z, fun), 'b', z, mathieu_rad_gey(m, 3, z, fun), 'g')
        xgrid
        ty1 = '$Gey_1(z,q)$'; xlabel('$z$'); ylabel(ty1);
        legend('$q = 1$', '$q = 2$', '$q = 3$',pos=2);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(2,2,3)
        plot(z, mathieu_rad_gey(m, 1, z, der), 'r-.', z, mathieu_rad_gey(m, 2, z, der), 'b-.', z, mathieu_rad_gey(m, 3, z, der), 'g-.')
        xgrid
        ty2 = '$Gey^{\ \prime}_1(z,q)$'; xlabel('$z$'); ylabel(ty2);
        legend('$q = 1$', '$q = 2$', '$q = 3$',pos=3);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
        
    m = 2;
    subplot(2,2,2);
        plot(z, mathieu_rad_gey(m, 1, z, fun), 'r', z, mathieu_rad_gey(m, 2, z, fun), 'b', z, mathieu_rad_gey(m, 3, z, fun), 'g')
        xgrid
        ty1 = '$Gey_2(z,q)$'; xlabel('$z$'); ylabel(ty1);
        legend('$q = 1$', '$q = 2$', '$q = 3$',pos=4);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(2,2,4)
        plot(z, mathieu_rad_gey(m, 1, z, der), 'r-.', z, mathieu_rad_gey(m, 2, z, der), 'b-.', z, mathieu_rad_gey(m, 3, z, der), 'g-.')
        xgrid
        ty2 = '$Gey^{\ \prime}_2(z,q)$'; xlabel('$z$'); ylabel(ty2);
        legend('$q = 1$', '$q = 2$', '$q = 3$',pos=3);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    f.figure_position=[sz(1) sz(2)]; f.figure_size=[sz(3) sz(4)];     
