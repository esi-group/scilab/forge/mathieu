// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//

// comparison with tables from
// 'Tables relating to the radial Mathieu functions (vols. 1 and 2) by G. Blanch, D.S. Clemm.'.
// online at HathiTrust [http://catalog.hathitrust.org/Record/000585710]
tol = 1.e-6;

// Ms1
  kind = 1;
  // 1
    // 199 (244)
    r=1; q= 0.5; x= 0.5;
    Ms1_x05_q05_table = 7.0710678e-1*sinh(r*x)*0.8796585;
    Ms1_x05_q05_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms1_x05_q05_table, Ms1_x05_q05_here, tol);
        
    // 199 (244)
    r=1; q= 0.5; x= 1.00;
    Ms1_x1_q05_table = 7.0710678e-1*sinh(r*x)*0.6477893;
    Ms1_x1_q05_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms1_x1_q05_table, Ms1_x1_q05_here, tol);
        
    // 201 (246)
    r=1; q= 1.0; x= 1.00;
    Ms1_x1_q1_table = 1.0*sinh(r*x)*0.3890032;
    Ms1_x1_q1_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms1_x1_q1_table, Ms1_x1_q1_here, tol);

  // 2
    // 203 (248)
    r=2; q= 0.5; x= 0.5;
    Ms2_x05_q05_table = 0.125*sinh(r*x)*0.8796304;
    Ms2_x05_q05_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms2_x05_q05_table, Ms2_x05_q05_here, tol);
        
    // 203 (248)
    r=2; q= 0.5; x= 1.00;
    Ms2_x1_q05_table = 0.125*sinh(r*x)*0.7230217;
    Ms2_x1_q05_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms2_x1_q05_table, Ms2_x1_q05_here, tol);

    // 205 (250)
    r=2; q= 1.0; x= 1.00;
    Ms2_x1_q1_table = 0.25*sinh(r*x)*0.5092808;
    Ms2_x1_q1_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms2_x1_q1_table, Ms2_x1_q1_here, tol);
        
  // 3
    // 207 (252)
    r=3; q= 0.5; x= 1.00;
    Ms3_x1_q05_table = 1.4731391e-2*sinh(r*x)*0.7933844;
    Ms3_x1_q05_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms3_x1_q05_table, Ms3_x1_q05_here, tol);
    
    // 209 (254)
    r=3; q= 1.0; x= 1.00;
    Ms3_x1_q1_table = 4.1666667e-2*sinh(r*x)*0.6197701;
    Ms3_x1_q1_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms3_x1_q1_table, Ms3_x1_q1_here, tol);

  // 4
    // 211 (256)
    r=4; q= 0.5; x= 1.00;
    Ms4_x1_q05_table = 1.3020833e-3*sinh(r*x)*0.8330354;
    Ms4_x1_q05_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms4_x1_q05_table, Ms4_x1_q05_here, tol);
        
    // 213 (258)
    r=4; q= 1.0; x= 1.00;
    Ms4_x1_q1_table = 5.2083333e-3*sinh(r*x)*0.6888246;
    Ms4_x1_q1_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms4_x1_q1_table, Ms4_x1_q1_here, tol);
        
 // 14
    // 279 (324)
    r=14; q= 0.5; x= 0.5;
    Ms14_x05_q05_table = 1.0939355e-17*sinh(r*x)*0.9810407;
    Ms14_x05_q05_here    =  mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms14_x05_q05_table, Ms14_x05_q05_here, tol);
        
    // 281 (326)
    r=14; q= 1.0; x= 1.00;
    Ms14_x1_q1_table = 1.4002375e-15*sinh(r*x)*0.8859722;
    Ms14_x1_q1_here    =  mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms14_x1_q1_table, Ms14_x1_q1_here, tol);
        
 // 15
    // 287 (332)
    r=15; q= 0.5; x= 0.5;
    Ms15_x05_q05_table = 2.5784308e-19*sinh(r*x)*0.9821870;
    Ms15_x05_q05_here    = mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms15_x05_q05_table, Ms15_x05_q05_here, tol);
        
    // 289 (334)
    r=15; q= 1.0; x= 1.00;
    Ms15_x1_q1_table = 4.6674583e-17*sinh(r*x)*0.8927331;
    Ms15_x1_q1_here    =  mathieu_rad_ms(r, q, x, 1, kind);
    assert_checkalmostequal(Ms15_x1_q1_table, Ms15_x1_q1_here, tol);

// Ms1'
  kind = 1;
  // 1
    // 293 (338)
    r=1; q= 0.5; x= 0.5;
    Ms1p_x05_q05_table = 7.0710678e-1*cosh(r*x)*0.7549225;
    Ms1p_x05_q05_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms1p_x05_q05_table, Ms1p_x05_q05_here, tol);
        
    // 293 (338)
    r=1; q= 0.5; x= 1.00;
    Ms1p_x1_q05_table = 7.0710678e-1*cosh(r*x)*0.1251067;
    Ms1p_x1_q05_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms1p_x1_q05_table, Ms1p_x1_q05_here, tol);
        
    // 295 (340)
    r=1; q= 1.0; x= 1.00;
    Ms1p_x1_q1_table = 1.0*cosh(r*x)*-0.3892801;
    Ms1p_x1_q1_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms1p_x1_q1_table, Ms1p_x1_q1_here, tol);
        
  // 2
    // 297 (342)
    r=2; q= 0.5; x= 0.5;
    Ms2p_x05_q05_table = 0.25*cosh(r*x)*0.8118493;
    Ms2p_x05_q05_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms2p_x05_q05_table, Ms2p_x05_q05_here, tol);

    // 297 (342)
    r=2; q= 0.5; x= 1.00;
    Ms2p_x1_q05_table = 0.25*cosh(r*x)*0.4936382;
    Ms2p_x1_q05_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms2p_x1_q05_table, Ms2p_x1_q05_here, tol);
        
    // 299 (344)
    r=2; q= 1.0; x= 1.00;
    Ms2p_x1_q1_table = 0.5*cosh(r*x)*0.1513718;
    Ms2p_x1_q1_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms2p_x1_q1_table, Ms2p_x1_q1_here, tol);

  // 3
    // 301 (346)
    r=3; q= 0.5; x= 1.00;
    Ms3p_x1_q05_table = 4.4194174e-2*cosh(r*x)*0.6634264;
    Ms3p_x1_q05_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms3p_x1_q05_table, Ms3p_x1_q05_here, tol);
        
    // 303 (348)
    r=3; q= 1.0; x= 1.00;
    Ms3p_x1_q1_table = 0.125*cosh(r*x)*0.4058623;
    Ms3p_x1_q1_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms3p_x1_q1_table, Ms3p_x1_q1_here, tol);

  // 4
    // 305 (350)
    r=4; q= 0.5; x= 1.00;
    Ms4p_x1_q05_table = 5.2083333e-3*cosh(r*x)*0.7515290;
    Ms4p_x1_q05_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms4p_x1_q05_table, Ms4p_x1_q05_here, tol);
        
    // 307 (352)
    r=4; q= 1.0; x= 1.00;
    Ms4p_x1_q1_table = 2.0833333e-2*cosh(r*x)*0.5494384;
    Ms4p_x1_q1_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms4p_x1_q1_table, Ms4p_x1_q1_here, tol);
        
  // 14
    // 373 (418)
    r=14; q= 0.5; x= 0.5;
    Ms14p_x05_q05_table = 1.5315098e-16*cosh(r*x)*0.9773657;
    Ms14p_x05_q05_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms14p_x05_q05_table, Ms14p_x05_q05_here, tol);

    // 375 (420)
    r=14; q= 1.0; x= 1.00;
    Ms14p_x1_q1_table = 1.9603325e-14*cosh(r*x)*0.8699341;
    Ms14p_x1_q1_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms14p_x1_q1_table, Ms14p_x1_q1_here, tol);
        
  // 15
    // 381 (426)
    r=15; q= 0.5; x= 0.5;
    Ms15p_x05_q05_table = 3.8676462e-18*cosh(r*x)*0.9789722;
    Ms15p_x05_q05_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms15p_x05_q05_table, Ms15p_x05_q05_here, tol);
        
    // 383 (428)
    r=15; q= 1.0; x= 1.00;
    Ms15p_x1_q1_table = 7.0011875e-16*cosh(r*x)*0.8786082;
    Ms15p_x1_q1_here    = mathieu_rad_ms(r, q, x, 0, kind);
    assert_checkalmostequal(Ms15p_x1_q1_table, Ms15p_x1_q1_here, tol);

// Ms2
  kind = 2;
  // 1
    // v.2 199 (230)
    r=1; q= 0.5; x= 0.5;
        Ms2_1_x05_q05_table = exp(-r*x) * -0.7882842 / 7.0710678e-1;
        Ms2_1_x05_q05_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_1_x05_q05_table, Ms2_1_x05_q05_here, tol);

    // v.2 199 (230)
    r=1; q= 0.5; x= 1.00;
        Ms2_1_x1_q05_table = exp(-r*x) * -0.4198673 / 7.0710678e-1;
        Ms2_1_x1_q05_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_1_x1_q05_table, Ms2_1_x1_q05_here, tol);
           
    // v.2 201 (232)
    r=1; q= 1.0; x= 1.0;
        Ms2_1_x1_q1_table = exp(-r*x) * 0.3749459 / 1.0;
        Ms2_1_x1_q1_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_1_x1_q1_table, Ms2_1_x1_q1_here, tol);
        
  // 2
    // v.2 203 (234)
    r=2; q= 0.5; x= 0.5;
        Ms2_2_x05_q05_table = exp(-r*x) * -0.8800308 / 0.25;
        Ms2_2_x05_q05_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_2_x05_q05_table, Ms2_2_x05_q05_here, tol);

    // v.2 203 (234)
    r=2; q= 0.5; x= 1.00;
        Ms2_2_x1_q05_table = exp(-r*x) * -1.2149101 / 0.25;
        Ms2_2_x1_q05_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_2_x1_q05_table, Ms2_2_x1_q05_here, tol);
           
    // v.2 205 (236)
    r=2; q= 1.0; x= 1.0;
        Ms2_2_x1_q1_table = exp(-r*x) * -1.0778573 / 0.5;
        Ms2_2_x1_q1_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_2_x1_q1_table, Ms2_2_x1_q1_here, tol);

  // 3
    // v.2 207 (238)
    r=3; q= 0.5; x= 1.0;
        Ms2_3_x1_q05_table = exp(-r*x) * -1.0785876 / 4.4194174e-2;
        Ms2_3_x1_q05_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_3_x1_q05_table, Ms2_3_x1_q05_here, tol);
        
    // v.2 209 (240)
    r=3; q= 1.0; x= 1.0;
        Ms2_3_x1_q1_table = exp(-r*x) * -1.6317760 / 1.25e-1;
        Ms2_3_x1_q1_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_3_x1_q1_table, Ms2_3_x1_q1_here, tol);

  // 4
    // v.2 211 (242)
    r=4; q= 0.5; x= 1;
        Ms2_4_x1_q05_table = exp(-r*x) * -0.8915950 / 5.2083333e-3;
        Ms2_4_x1_q05_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_4_x1_q05_table, Ms2_4_x1_q05_here, tol);
        
    // v.2 213 (244)
    r=4; q= 1.0; x= 1.0;
        Ms2_4_x1_q1_table = exp(-r*x) * -1.3099061 / 2.0833333e-2;
        Ms2_4_x1_q1_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_4_x1_q1_table, Ms2_4_x1_q1_here, tol);
  // see p. 223 (254): for r>= 7 Ts_r(x,q) = Tc_r(x,q). See pages 30-97 (59-128) for values of Tc_r(x,q)
  // 14
    // v.2 83 (112)
    r=14; q= 0.5; x= 0.5;
        Ms2_14_x05_q05_table = exp(-r*x) * -0.6515049 / 1.5315098e-16;
        Ms2_14_x05_q05_here  = mathieu_rad_ms(r, q, x, 1, kind);
        assert_checkalmostequal(Ms2_14_x05_q05_table, Ms2_14_x05_q05_here, tol);
        
    // v.2 87 (118)
    r=14; q= 1.0; x= 1.0;
        Ms2_14_x1_q1_table = exp(-r*x) * -0.7328347 / 1.9603325e-14;
        Ms2_14_x1_q1_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_14_x1_q1_table, Ms2_14_x1_q1_here, tol);
   
  // 15
    // v.2 91 (122)
    r=15; q= 0.5; x= 0.5;
        Ms2_15_x05_q05_table = exp(-r*x) * -0.6504090 / 3.8676462e-18;
        Ms2_15_x05_q05_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        assert_checkalmostequal(Ms2_15_x05_q05_table, Ms2_15_x05_q05_here, tol);
        
    // v.2 97 (128)
    r=15; q= 1.0; x= 1.0;
        Ms2_15_x1_q1_table = exp(-r*x) * -0.7254014 / 7.0011875e-16;
        Ms2_15_x1_q1_here  = mathieu_rad_ms(r, q, x, 1, kind);        
        Ms2_15_x1_q1d      = Ms2_15_x1_q1_table - Ms2_15_x1_q1_here;
        assert_checkalmostequal(Ms2_15_x1_q1_table, Ms2_15_x1_q1_here, tol);

// Ms2`
  // 1
    // v2. 227 (258)
    r=1; q= 0.5; x= 0.5;
        Ms2_1p_x05_q05_table = exp(-r*x) * 0.8258667 / 7.0710678e-1;
        Ms2_1p_x05_q05_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_1p_x05_q05_table, Ms2_1p_x05_q05_here, tol);

    // v2. 227 (258)
    r=1; q= 0.5; x= 1.00;
        Ms2_1p_x1_q05_table = exp(-r*x) * 2.1666807 / 7.0710678e-1;
        Ms2_1p_x1_q05_here    = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_1p_x1_q05_table, Ms2_1p_x1_q05_here, tol);

    // v2. 229 (260)
    r=1; q= 1.0; x= 1.00;
        Ms2_1p_x1_q1_table = exp(-r*x) * 3.2927103 / 1;
        Ms2_1p_x1_q1_here    = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_1p_x1_q1_table, Ms2_1p_x1_q1_here, tol);
        
  // 2
    // v2. 231 (262)
    r=2; q= 0.5; x= 1.00;
        Ms2_2p_x1_q05_table = exp(-r*x) * 0.9334300 / 1.25e-1;
        Ms2_2p_x1_q05_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_2p_x1_q05_table, Ms2_2p_x1_q05_here, tol);

    // v2. 205 (236)
    r=2; q= 1.0; x= 1.00;
        Ms2_2p_x1_q1_table = exp(-r*x) * 2.2143960 / 2.5e-1;
        Ms2_2p_x1_q1_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_2p_x1_q1_table, Ms2_2p_x1_q1_here, tol);

  // 3
    // v2. 235 (266)
    r=3; q= 0.5; x= 1.00;
        Ms2_3p_x1_q05_table = exp(-r*x) * 0.7024133 / 1.4731391e-2;
        Ms2_3p_x1_q05_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_3p_x1_q05_table, Ms2_3p_x1_q05_here, tol);

    // v2. 237 (268)
    r=3; q= 1.0; x= 1.00;
        Ms2_3p_x1_q1_table = exp(-r*x) * 0.9855843 / 4.1666667e-2;
        Ms2_3p_x1_q1_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_3p_x1_q1_table, Ms2_3p_x1_q1_here, tol);
        
  // 4
    // v2. 239 (270)
    r=4; q= 0.5; x= 1.00;
        Ms2_4p_x1_q05_table = exp(-r*x) * 0.7240480 / 1.3020833e-3;
        Ms2_4p_x1_q05_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_3p_x1_q05_table, Ms2_3p_x1_q05_here, tol);

    // v2. 241 (272)
    r=4; q= 1.0; x= 1.00;
        Ms2_4p_x1_q1_table = exp(-r*x) * 0.8035007 / 5.2083333e-3;
        Ms2_4p_x1_q1_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_4p_x1_q1_table, Ms2_4p_x1_q1_here, tol);

  // see p. 223: for r >= 7 Ts_r(x,q) = Tc_r(x,q). See pages 30-97 (59-128) for values of Tc_r(x,q).
  // 14
    // v2. 185 (216)
    r=14; q= 0.5; x= 0.5;
        Ms2_14p_x05_q05_table = exp(-r*x) * 0.6487815 / 1.0939355e-17;
        Ms2_14p_x05_q05_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_14p_x05_q05_table, Ms2_14p_x05_q05_here, tol);
        
    // v2. 187 (218)
    r=14; q= 1.0; x= 1.00;
        Ms2_14p_x1_q1_table = exp(-r*x) * 0.7175413 / 1.4002375e-15;
        Ms2_14p_x1_q1_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_14p_x1_q1_table, Ms2_14p_x1_q1_here, tol);
        
  // 15
    // v2. 189 (220)
    r=15; q= 0.5; x= 0.5;
        Ms2_15p_x05_q05_table = exp(-r*x) * 0.6480510 / 2.5784308e-19;
        Ms2_15p_x05_q05_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_15p_x05_q05_table, Ms2_15p_x05_q05_here, tol);
        
    // 192 (237)
    r=15; q= 1.0; x= 1.00;
        Ms2_15p_x1_q1_table = exp(-r*x) * 0.7123024 / 4.6674583e-17;
        Ms2_15p_x1_q1_here  = mathieu_rad_ms(r, q, x, 0, kind);
        assert_checkalmostequal(Ms2_15p_x1_q1_table, Ms2_15p_x1_q1_here, tol);

// comparison with values from Tables 14.8(a) from Zhang & Jin
  kind = 1;
  // Ms1 function
    // p. 530 
    q = 10;
    z = 0.2;
    fun_or_der = 1;
    assert_checkalmostequal(mathieu_rad_ms(1, q, z, fun_or_der, kind), 0.30126082, tol);
    assert_checkalmostequal(mathieu_rad_ms(2, q, z, fun_or_der, kind), 0.29299787, tol);
    assert_checkalmostequal(mathieu_rad_ms(3, q, z, fun_or_der, kind), 0.26274268, tol);
    assert_checkalmostequal(mathieu_rad_ms(4, q, z, fun_or_der, kind), 0.19743531, tol);
    assert_checkalmostequal(mathieu_rad_ms(5, q, z, fun_or_der, kind), 0.11061245, tol);
    
    z = 4.0;
    assert_checkalmostequal(mathieu_rad_ms(1, q, z, fun_or_der, kind), 0.04972258, tol);
    assert_checkalmostequal(mathieu_rad_ms(2, q, z, fun_or_der, kind), 0.03649038, tol);
    assert_checkalmostequal(mathieu_rad_ms(3, q, z, fun_or_der, kind), -0.04742062, tol);
    assert_checkalmostequal(mathieu_rad_ms(4, q, z, fun_or_der, kind), -0.03921348, tol);
    assert_checkalmostequal(mathieu_rad_ms(5, q, z, fun_or_der, kind), 0.04529535, tol);

// comparison with values from Tables 14.8(b) from Zhang & Jin
  kind = 1;
  // Ms1 derivative
    // p. 530 
    q = 10;
    z = 0.0;
    fun_or_der = 0;
    assert_checkalmostequal(mathieu_rad_ms(1, q, z, fun_or_der, kind), 1.91681522, tol);
    assert_checkalmostequal(mathieu_rad_ms(2, q, z, fun_or_der, kind), 1.71465142, tol);
    assert_checkalmostequal(mathieu_rad_ms(3, q, z, fun_or_der, kind), 1.42989357, tol);
    assert_checkalmostequal(mathieu_rad_ms(4, q, z, fun_or_der, kind), 1.00788774, tol);
    assert_checkalmostequal(mathieu_rad_ms(5, q, z, fun_or_der, kind), 0.53055829, tol);
    
    z = 4.0;
    assert_checkalmostequal(mathieu_rad_ms(1, q, z, fun_or_der, kind), -6.04208173, tol);
    assert_checkalmostequal(mathieu_rad_ms(2, q, z, fun_or_der, kind), 8.36171685, tol);
    assert_checkalmostequal(mathieu_rad_ms(3, q, z, fun_or_der, kind), 6.57239247, tol);
    assert_checkalmostequal(mathieu_rad_ms(4, q, z, fun_or_der, kind), -7.98487580, tol);
    assert_checkalmostequal(mathieu_rad_ms(5, q, z, fun_or_der, kind), -7.00560482, tol);

// comparison with values from Tables 14.10(a) from Zhang & Jin
  kind = 2;
  // Ms2 function
    // p. 532 
    q = 10;
    z = 0.0;
    fun_or_der = 1;
    assert_checkalmostequal(mathieu_rad_ms(1, q, z, fun_or_der, kind), -0.33212370, tol);
    assert_checkalmostequal(mathieu_rad_ms(2, q, z, fun_or_der, kind), -0.37128233, tol);
    assert_checkalmostequal(mathieu_rad_ms(3, q, z, fun_or_der, kind), -0.44522179, tol);
    assert_checkalmostequal(mathieu_rad_ms(4, q, z, fun_or_der, kind), -0.63163758, tol);
    assert_checkalmostequal(mathieu_rad_ms(5, q, z, fun_or_der, kind), -1.19990542, tol);
    
    z = 4.0;
    assert_checkalmostequal(mathieu_rad_ms(1, q, z, fun_or_der, kind), 0.03484300, tol);
    assert_checkalmostequal(mathieu_rad_ms(2, q, z, fun_or_der, kind), -0.04853386, tol);
    assert_checkalmostequal(mathieu_rad_ms(3, q, z, fun_or_der, kind), -0.03793429, tol);
    assert_checkalmostequal(mathieu_rad_ms(4, q, z, fun_or_der, kind), 0.04637466, tol);
    assert_checkalmostequal(mathieu_rad_ms(5, q, z, fun_or_der, kind), 0.04046256, tol);

// comparison with values from Tables 14.10(b) from Zhang & Jin
  kind = 2;
  // Ms2 derivative
    // p. 532
    q = 10;
    z = 0.0;
    fun_or_der = 0;
    assert_checkalmostequal(mathieu_rad_ms(1, q, z, fun_or_der, kind), 0.00010626, tol*100);
    assert_checkalmostequal(mathieu_rad_ms(2, q, z, fun_or_der, kind), 0.00419071, tol);
    assert_checkalmostequal(mathieu_rad_ms(3, q, z, fun_or_der, kind), 0.06623432, tol);
    assert_checkalmostequal(mathieu_rad_ms(4, q, z, fun_or_der, kind), 0.54026218, tol);
    assert_checkalmostequal(mathieu_rad_ms(5, q, z, fun_or_der, kind), 2.83309615, tol);
    
    z = 4.0;
    assert_checkalmostequal(mathieu_rad_ms(1, q, z, fun_or_der, kind), 8.56945742, tol);
    assert_checkalmostequal(mathieu_rad_ms(2, q, z, fun_or_der, kind), 6.32477245, tol);
    assert_checkalmostequal(mathieu_rad_ms(3, q, z, fun_or_der, kind), -8.16734951, tol);
    assert_checkalmostequal(mathieu_rad_ms(4, q, z, fun_or_der, kind), -6.79164015, tol);
    assert_checkalmostequal(mathieu_rad_ms(5, q, z, fun_or_der, kind), 7.79671799, tol);

// Example: Ms1(2) for comparison with Fig. 20.13 of Abramowitz-Stegun [1]
    f=scf(); mrgn = 50; font_sz = 4;
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ;
    f.figure_name = 'Ms1(2) for comparison with Fig. 20.13 of Abramowitz-Stegun';

    z = linspace(0, 2, 100);
    fun = 1;
    
    m = 1; kind = 2;
    plot(z, sqrt(%pi/2)*mathieu_rad_ms(m, 0.25, z, fun, kind), z, sqrt(%pi/2)*mathieu_rad_ms(m, 0.75, z, fun, kind), z, sqrt(%pi/2)*mathieu_rad_ms(m, 1.5, z, fun, kind),z, sqrt(%pi/2)*mathieu_rad_ms(m, 3.75, z, fun, kind), z, sqrt(%pi/2)*mathieu_rad_ms(m, 5.25, z, fun, kind))
    xgrid
    ty1 = '$\sqrt{{\pi}/2} Ms^{(2)}_1(z,q)$'; xlabel('$z$'); ylabel(ty1);
    legend('$q = 0.25$','$q = 0.75$','$q = 1.5$','$q = 3.75$','$q = 5.25$',pos=4);
    
    h = gca(); h.margins=[0.15 0.05 0.05 0.12]; h.font_size = font_sz - 1;  
    h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
    h.children(1).font_size = font_sz;
    f.figure_position=[sz(1) sz(2)]; f.figure_size=[sz(3) sz(4)]; 
