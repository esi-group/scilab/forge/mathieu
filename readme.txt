Mathieu Functions Toolbox

Purpose
-------

This Mathieu Functions Toolbox is used to solve Mathieu function numerically
[1].

The Mathieu equation is a second-order homogeneous linear differential equation
and appears in several different situations in Physics: electromagnetic or
elastic wave equations with elliptical boundary conditions as in waveguides or
resonators, the motion of particles in alternated-gradient focussing or
electromagnetic traps, the inverted pendulum, parametric oscillators, the motion
of a quantum particle in a periodic potential, the eigenfunctions of the quantum
pendulum, are just few examples. Their solutions, known as Mathieu functions,
were first discussed by Mathieu in 1868 in the context of the free oscillations
of an elliptic membrane. 

We present both the Floquet solution and solution in angular and radial
(modified) functions [2-4].

The toolbox has 14 demos.
All demos can be acessed from "?->Scilab Demonstrations->Mathieu functions"

Dependencies
------------
This module depends on the assert module.

Authors
-------
R.Coisson & G. Vernizzi, Parma University, 2001-2002.
X. K. Yang
N. O. Strelkov, NRU MPEI
Michael Baudin - DIGITEO - 2011

Features
--------
> Coordinate transformation
Almost all tasks, involving Mathieu functions have elliptical geometry. For
coordinate transformation we have functions: 
* mathieu_cart2ell 	convert coordinates from Cartesian to elliptical
* mathieu_ell2cart 	convert coordinates from elliptical to Cartesian
* mathieu_cart2pol 	convert coordinates from Cartesian to polar
* mathieu_pol2cart 	convert coordinates from polar to Cartesian
* mathieu_ell_in_pol 	calculate polar coordinates of a point at known angle on ellipse

  * For better understanding of elliptical coordinates you can use the first tab "Elliptic and Cartesian coordinates" of demo named "GUI for Mathieu Function Toolbox" (available for Scilab 5.5 and higher).
  * Also you can use the demo named "Elliptic and Cartesian coordinates".

> Floquet solution
For Floquet solution we have the following functions: 
  * mathieu_mathieuf 	evaluate characteristic values and expansion coefficients
  * mathieu_mathieu 	evaluate periodic Mathieu functions by calling mathieu_mathieuf
  * mathieu_mathieuexp 	calculate the characteristic exponent of non-periodic solutions and the coefficients of the expansion of the periodic factor
  * mathieu_mathieuS 	calculate solutions of ME with arbitrary a and q parameters (uses mathieu_mathieuexp)

  For Floquet solutions we have demos:
  * Plots ce_m(z,q), ce_m'(z,q), se_m(z,q), se_m'(z,q) for q = -25..25 and m = 0(1)..15 are available on the second tab "Angular Mathieu Functions" of demo named "GUI for Mathieu Function Toolbox" (available for Scilab 5.5 and higher).
  * Plots of ce_m(z,q), se_m(z,q) of order 0-5 for q = 1,10 for comparison with Abramowitz and Stegun (Figs. 20.2-20.5, [3, pp. 725-726]).
  * Tables of ce_m(z,q) and ce_m'(z,q) for comparison with Abramowitz and Stegun (Table 20.1 [3, p. 748]).
  * Tables of se_m(z,q) and se_m'(z,q) for comparison with Abramowitz and Stegun (Table 20.1 [3, p. 749]).
  * Plots of ce_m(z,q), se_m(z,q) of order 0-3 for q = 0-30 and z = [0, π] (Fig. 2, [5, p. 235]).

> Solution in angular and radial (modified) functions
  We present solutions of Mathieu equation as angular Mathieu functions [2,3]: 
ce_m(z,q) and se_m(z,q) [2] 
and solutions of modified Mathieu equation (2) as radial Mathieu functions [2,3]: 			
Mc_m(1)(z,q), Ce_m(z,q), Mc_m(2)(z,q), Gey_m(z,q) and Ms_m(1)(z,q), Se_m(z,q),
Ms_m(2)(z,q), Fey_m(z,q).

  Before calculating any Mathieu function we calculate expansion coefficients
and eigenvalues for given order m and parameter q using tri-diagonal matrixes
[5-8].

  We have the following functions for computation solutions of Mathieu
equations: 
  * mathieu_Arm 	compute expansion coefficients 'Arm' and eigenvalue 'am' for even angular and radial Mathieu functions
  * mathieu_Brm 	compute expansion coefficients 'Brm' and eigenvalue 'bm' for odd angular and radial Mathieu functions
  * mathieu_ang_ce 	compute even angular Mathieu function 'ce' or its first derivative
  * mathieu_ang_se 	compute odd angular Mathieu function 'se' or its first derivative
  * mathieu_rad_mc 	compute even radial (modified) Mathieu function 'Mc' or its first derivative (kinds 1 and 2)
  * mathieu_rad_ms 	compute odd radial (modified) Mathieu function 'Ms' or its first derivative (kinds 1 and 2)
  * mathieu_rad_ce 	compute even radial (modified) Mathieu function of the first kind 'Ce' or its first derivative
  * mathieu_rad_se 	compute odd radial (modified) Mathieu function of the first kind 'Se' or its first derivative
  * mathieu_rad_fey 	compute even radial (modified) Mathieu function of the second kind 'Fey' or its first derivative
  * mathieu_rad_gey 	compute odd radial (modified) Mathieu function of the second kind 'Gey' or its first derivative

During unit-testing all functions were tested against known tables: eigenvalues,
expansion coefficients and angular functions and its first derivatives were
compared with [3, 9], radial functions and their first derivatives were compared
with [9-12].

  * For eigenvalues we have demo, named "Stability chart for
eigenvalues of Mathieu`s equations" (for comparison with http://dlmf.nist.gov/28.17 [4]).

  For modified solutions we have demos:

  * Plots of Mc_m(1,2)(z,q), Mc_m(1,2)`(z,q); Ms_m(1,2)(z,q), Ms_m(1,2)`(z,q); Ce_m(z,q), Ce_m`(z,q); Se_m(z,q), Se_m`(z,q); Fey_m(z,q), Fey_m`(z,q); Gey_m(z,q), Gey_m`(z,q) for q = 1..11 and m = 0(1)..15 are available on the third tab "Radial Mathieu Functions" of demo named "GUI for Mathieu Function Toolbox" (available for Scilab 5.5 and higher).
  * Plots of Mc_m(1)(z,q), Ms_m(1)(z,q) of order 0-2 for q = 0-3 and z =[0, π].
  * Plots of Mc_m(2)(z,q), Ms_m(2)(z,q) of order 0-2 for q = 0-3 and z =[0, π].
  * Plots of Ce_m(z,q), Se_m(z,q) of order 0-2 for q = 0-3 and z = [0,π].
  * Plots of Fey_m(z,q), Gey_m(z,q) of order 0-2 for q = 0-3 and z = [0,π].

  All functions have examples with plots, some of them for comparison with [3,4].

> Calculation modes of elliptical membrane

This toolbox allows elliptical membrane mode calculation. We have two functions for this purpose:
  * mathieu_rootfinder - rootfinder for radial Mathieu function or its first derivative (finds q values of given radial function type with known order m and radial argument ξ0, which satisfies the equation RMF_m(q,xi0) = 0 (Dirichlet boundary condition) or RMF_m'(q,xi0) = 0 (Neumann boundary condition).
  * mathieu_membrane_mode - calculate elliptical membrane mode for known semi-axes, mode numbers, mode type and boundary condition (Soft/Dirichlet or Hard/Neumann).

During unit-testing these functions were tested against tables and plots from [2, 13-18].

For elliptical membrane we have demos:

  * The fourth tab "Elliptic membrane" of demo named "GUI for Mathieu Function Toolbox" (available for Scilab 5.5 and higher).
  * Elliptic membrane: comparison of 2 even soft modes.
  * Elliptic membrane: comparison of 4 even & odd, soft & hard.
  * Elliptic membrane: 3D surface plot of a even soft mode with m=3, n=3.

Example
-------
// plot Even Soft membrane mode with m=3, n=2
a = 0.05; // semi-major axis
b = 0.03; // semi-minor axis
m = 3; // function order (angular variations)
n = 2; // number of q root (radial variations)
mathieu_membrane_mode(a, b, m, n, 'Mc1', %t, 101, 101);


Bibliography
------------
1. R. Coïsson, G. Vernizzi and X.K. Yang, "Mathieu functions and numerical solutions of the Mathieu equation", IEEE Proceedings of OSSC2009 (online at http://www.fis.unipr.it/~coisson/Mathieu.pdf).
2. N.W. McLachlan, Theory and Application of Mathieu Functions, Oxford Univ. Press, 1947.
3. M. Abramowitz and I.A. Stegun, Handbook of Mathematical Functions, Dover, New York, 1965.
4. Chapter 28 Mathieu Functions and Hill's Equation. Digital Library of Mathematical Functions. NIST. (online at http://dlmf.nist.gov/28).
5. J. C. Gutiérrez-Vega, R. M. Rodríguez-Dagnino, M. A. Meneses-Nava, and S. Chávez-Cerda, "Mathieu functions, a visual approach", American
Journal of Physics, 71 (233), 233-242. An introduction to applications (online at http://www.df.uba.ar/users/sgil/physics_paper_doc/papers_phys/modern/matheiu0.pdf).
6. J. J. Stamnes and B. Spjelkavik. New method for computing eigenfunctions (Mathieu functions) for scattering by elliptical cylinders. Pure Appl. Opt. 4
251-62, 1995.
7. L. Chaos-Cador, E. Ley-Koo. Mathieu functions revisited: matrix evaluation and generating functions. Revista Mexicana de Fisica, Vol. 48, p.67-75, 2002.
8. Julio C. Gutiérrez-Vega, "Formal analysis of the propagation of invariant optical fields in elliptic coordinates", Ph. D. Thesis, INAOE,
México, 2000. (online at http://homepages.mty.itesm.mx/jgutierr/).
9. S. Zhang and J. Jin. Computation of Special Functions. New York, Wiley, 1996.
10. G. Blanch and D. S. Clemm. Tables relating to the radial Mathieu functions. Volume 1. Functions of the First Kind. ARL, US Air Force. 1963. (online at http://catalog.hathitrust.org/Record/000585710).
11. G. Blanch and D. S. Clemm. Tables relating to the radial Mathieu functions. Volume 2. Functions of the Second Kind. ARL, US Air Force. 1963. (online at http://catalog.hathitrust.org/Record/000585710). 
12. E. T. Kirkpatrick. Tables of Values of the Modified Mathieu Functions. Mathematics of Computation, Vol. 14, No. 70 (Apr., 1960), pp. 118-129. (online at http://www.ams.org/journals/mcom/1960-14-070/S0025-5718-1960-0113288-4/S0025-5718-1960-0113288-4.pdf).
13. Wilson, Howard B., and Robert W. Scharstein. "Computing elliptic membrane high frequencies by Mathieu and Galerkin methods." Journal of Engineering Mathematics 57.1 (2007): 41-55. (online at http://scharstein.eng.ua.edu/ENGI1589.pdf or http://dx.doi.org/10.1007/s10665-006-9070-1 )
14. Neves, Armando GM. "Eigenmodes and eigenfrequencies of vibrating elliptic membranes: a Klein oscillation theorem and numerical calculations." Comm. Pure Appl. Anal. 2009. (online at http://www.ma.utexas.edu/mp_arc/c/09/09-174.pdf )
15. Shibaoka, Yoshio, and Fusako Iida. "On the free oscillation of water in a lake of elliptic boundary." The Journal of the Oceanographical Society of Japan. 21.3 (1965): 103-108. (online at http://www.terrapub.co.jp/journals/JO/JOSJ/pdf/2103/21030103.pdf )
16. Hamidzadeh, Hamid R., and L. Moxey. "Analytical modal analysis of thin-film flat lenses." Proceedings of the Institution of Mechanical Engineers, Part K: Journal of Multi-body Dynamics 219.1 (2005): 55-59.
17. Lee, W. M. "Natural mode analysis of an acoustic cavity with multiple elliptical boundaries by using the collocation multipole method." Journal of Sound and Vibration 330.20 (2011): 4915-4929.
18. Gutiérrez-Vega, J., S. Chávez-Cerda, and Ramón Rodríguez-Dagnino. "Free oscillations in an elliptic membrane." Revista Mexicana de Fisica 45.6 (1999): 613-622. (online at http://optica.mty.itesm.mx/pmog/Papers/P001.pdf )

Licence
-------
This toolbox is distributed under the Gnu General Public License, Version 2.

