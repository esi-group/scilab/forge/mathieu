﻿<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
 *
 * This file must be used under the terms of the GPL Version 2
 * http://www.gnu.org/licenses/gpl-2.0.html
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en_US" xml:id="mathieu_ang_ce">
  <info>
    <pubdate>$LastChangedDate: 14-04-2012 $</pubdate>
  </info>
  <refnamediv>
    <refname>mathieu_ang_ce</refname>
    <refpurpose>Compute even angular Mathieu function 'ce' or its first derivative.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>y = mathieu_ang_ce( m, q, z, fun_or_der [, Arm] )</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>m</term>
        <listitem>
          <para>
            the order of Mathieu function
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>q</term>
        <listitem>
          <para>
            the value of <emphasis>q</emphasis> parameter (can be positive, negative or complex number)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>z</term>
        <listitem>
          <para>
            an argument
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>fun_or_der</term>
        <listitem>
          <para>
            calculate function (1) or first derivative (0)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Arm</term>
        <listitem>
          <para>
            expansion coefficients for even angular and radial Mathieu function for the same <emphasis>m</emphasis> and <emphasis>q</emphasis> (optional, for speed).
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>y</term>
        <listitem>
          <para>
            value of the 'ce' function or its first derivative
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      <link linkend="mathieu_ang_ce">Mathieu_ang_ce</link> computes even angular Mathieu function
      <math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>c</mi><msubsup><mi>e</mi><mi>m</mi><mrow/></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math>
      &#160;or its first derivative 
      <math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>c</mi><msubsup><mi>e</mi><mi>m</mi><mrow><mo>&#160;</mo><mo>'</mo></mrow></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math>
      &#160;using the following formulas [1]:
      <para>functions - </para>
      <para>
			<math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mtable columnalign="left"><mtr><mtd><mi>c</mi><msub><mi>e</mi><mrow><mn>2</mn><mi>n</mi></mrow></msub><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><munderover><mo>&#8721;</mo><mrow><mi>r</mi><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><mn>0</mn></mrow><mo>&#8734;</mo></munderover><msubsup><mi>A</mi><mrow><mn>2</mn><mi>r</mi></mrow><mfenced><mrow><mn>2</mn><mi>n</mi></mrow></mfenced></msubsup><mi mathvariant="normal">cos</mi><mo>&#160;</mo><mfenced><mrow><mn>2</mn><mi>r</mi><mo>&#183;</mo><mi mathvariant="normal">z</mi></mrow></mfenced></mtd></mtr><mtr><mtd/></mtr><mtr><mtd><mi>c</mi><msub><mi>e</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></msub><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><munderover><mo>&#8721;</mo><mrow><mi>r</mi><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><mn>0</mn></mrow><mo>&#8734;</mo></munderover><msubsup><mi>A</mi><mrow><mn>2</mn><mi>r</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow><mfenced><mrow><mn>2</mn><mi>n</mi><mo>+</mo><mn>1</mn></mrow></mfenced></msubsup><mi mathvariant="normal">cos</mi><mfenced><mrow><mn>2</mn><mi>r</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></mfenced><mi mathvariant="normal">z</mi></mtd></mtr></mtable></math>
      </para>
      <para>
		  derivatives - 
      </para>
      <para>
		  <math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mtable columnalign="left"><mtr><mtd><mi>c</mi><msubsup><mi>e</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo></mrow><mrow><mo>&#160;</mo><mo>&#160;</mo><mo>'</mo></mrow></msubsup><mstyle displaystyle="false"><mo>(</mo></mstyle><mstyle displaystyle="false"><mi mathvariant="normal">z</mi></mstyle><mstyle displaystyle="false"><mo>,</mo></mstyle><mstyle displaystyle="false"><mi>q</mi></mstyle><mstyle displaystyle="false"><mo>)</mo></mstyle><mstyle displaystyle="false"><mo>=</mo></mstyle><mfrac><mrow><mo>d</mo><mi>c</mi><msub><mi>e</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo></mrow></msub><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></mrow><mrow><mo>d</mo><mi>z</mi></mrow></mfrac><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><mo>-</mo><munderover><mo>&#8721;</mo><mrow><mi>r</mi><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><mn>0</mn></mrow><mo>&#8734;</mo></munderover><mo>(</mo><mn>2</mn><mi>r</mi><mo>)</mo><msubsup><mi>A</mi><mrow><mn>2</mn><mi>r</mi></mrow><mfenced><mrow><mn>2</mn><mi>n</mi></mrow></mfenced></msubsup><mi mathvariant="normal">sin</mi><mo>&#160;</mo><mfenced><mrow><mn>2</mn><mi>r</mi><mo>&#183;</mo><mi mathvariant="normal">z</mi></mrow></mfenced></mtd></mtr><mtr><mtd/></mtr><mtr><mtd><mi>c</mi><msubsup><mi>e</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn><mo>&#160;</mo></mrow><mrow><mo>&#160;</mo><mo>&#160;</mo><mo>'</mo></mrow></msubsup><mstyle displaystyle="false"><mo>(</mo></mstyle><mstyle displaystyle="false"><mi mathvariant="normal">z</mi></mstyle><mstyle displaystyle="false"><mo>,</mo></mstyle><mstyle displaystyle="false"><mi>q</mi></mstyle><mstyle displaystyle="false"><mo>)</mo></mstyle><mstyle displaystyle="false"><mo>=</mo></mstyle><mfrac><mrow><mo>d</mo><mi>c</mi><msub><mi>e</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></msub><mo>&#160;</mo><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></mrow><mrow><mo>d</mo><mi>z</mi></mrow></mfrac><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><munderover><mrow><mo>-</mo><mo>&#8721;</mo></mrow><mrow><mi>r</mi><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><mn>0</mn></mrow><mo>&#8734;</mo></munderover><mo>&#160;</mo><mfenced><mrow><mn>2</mn><mi>r</mi><mo>+</mo><mn>1</mn></mrow></mfenced><msubsup><mi>A</mi><mrow><mn>2</mn><mi>r</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow><mfenced><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></mfenced></msubsup><mi mathvariant="normal">sin</mi><mo>&#160;</mo><mfenced><mrow><mn>2</mn><mi>r</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></mfenced><mi mathvariant="normal">z</mi></mtd></mtr></mtable></math>
      </para>
	  <para>
			During unit-testing of this function values of even angular function
			<math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>c</mi><msubsup><mi>e</mi><mi>m</mi><mrow/></msubsup><mo>&#160;</mo><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math>
			&#160;and its derivative
			<math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>c</mi><msubsup><mi>e</mi><mi>m</mi><mrow><mo>&#160;</mo><mo>'</mo></mrow></msubsup><mo>&#160;</mo><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math>
			&#160;were compared to well-known tables: Tables 20.1 from [2], Tables 14.5(a), 14.5(b) from [3]. The results are very close.
	</para>		
    </para>   
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example">
      <![CDATA[
// Example: Even periodic Mathieu functions and their first derivatives
    f=scf(); mrgn = 50; font_sz = 4;
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ; 
    f.figure_name = 'Even periodic Mathieu functions and their first derivatives';

    z = linspace(0, 2*%pi, 100);
    fun = 1; der = 0;

    m = 0;
    subplot(2,2,1)
        plot(z, mathieu_ang_ce(m,-3,z,fun), z, mathieu_ang_ce(m,-1,z,fun),...
		z, mathieu_ang_ce(m,0,z,fun), z, mathieu_ang_ce(m,1,z,fun), z, mathieu_ang_ce(m,3,z,fun))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$ce_0(z,q)$'; xlabel('$z$'); ylabel(ty);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;

    subplot(2,2,3)
        plot(z, mathieu_ang_ce(m,-3,z,der), z, mathieu_ang_ce(m,-1,z,der),...
		z, mathieu_ang_ce(m,0,z,der), z, mathieu_ang_ce(m,1,z,fun), z, mathieu_ang_ce(m,3,z,der))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$ce^{\ \prime}_0(z,q)$'; xlabel('$z$'); ylabel(ty);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
        
    m = 1;
    subplot(2,2,2)        
        plot(z, mathieu_ang_ce(m,-3,z,fun), z, mathieu_ang_ce(m,-1,z,fun),...
		z, mathieu_ang_ce(m,0,z,fun), z, mathieu_ang_ce(m,1,z,fun), z, mathieu_ang_ce(m,3,z,fun))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$',pos=4);
        ty = '$ce_1(z,q)$'; xlabel('$z$'); ylabel(ty);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;

    subplot(2,2,4)
        plot(z, mathieu_ang_ce(m,-3,z,der), z, mathieu_ang_ce(m,-1,z,der),...
		z, mathieu_ang_ce(m,0,z,der), z, mathieu_ang_ce(m,1,z,fun), z, mathieu_ang_ce(m,3,z,der))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$ce^{\ \prime}_1(z,q)$'; xlabel('$z$'); ylabel(ty);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    f.figure_position=[sz(1) sz(2)]; f.figure_size=[sz(3) sz(4)]; 
]]>
    </programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
		<link linkend="mathieu_Arm">mathieu_Arm</link>
      </member>
    </simplelist>
  </refsection>  
  <refsection>
    <title>Authors</title>
    <para>
      R.Coisson and G. Vernizzi, <emphasis>Parma University</emphasis>
    </para>
    <para>
      N. O. Strelkov, <emphasis>NRU MPEI</emphasis>
    </para>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>	   
         1. N. W. McLachlan, Theory and Application of Mathieu Functions, Oxford Univ. Press, 1947.
       </para>
       <para>
         2. M. Abramowitz and I.A. Stegun, Handbook of Mathematical Functions, Dover, New York, 1965.
       </para>
       <para>
         3. S. Zhang and J. Jin. Computation of Special Functions. New York, Wiley, 1996.
	   </para>
   </refsection>
</refentry>
