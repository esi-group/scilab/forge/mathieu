﻿<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
 *
 * This file must be used under the terms of the GPL Version 2
 * http://www.gnu.org/licenses/gpl-2.0.html
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en_US" xml:id="mathieu_rad_ce">
  <info>
    <pubdate>$LastChangedDate: 15-04-2012 $</pubdate>
  </info>
  <refnamediv>
    <refname>mathieu_rad_ce</refname>
    <refpurpose>Compute even radial (modified) Mathieu function of the first kind 'Ce' or its first derivative.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>y = mathieu_rad_ce( m, q, z, fun_or_der [, Arm] )</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>m</term>
        <listitem>
          <para>
            the order of Mathieu function
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>q</term>
        <listitem>
          <para>
            the value of <emphasis>q</emphasis> parameter (can be positive, negative or complex number)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>z</term>
        <listitem>
          <para>
            an argument
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>fun_or_der</term>
        <listitem>
          <para>
            calculate function (1) or first derivative (0)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Arm</term>
        <listitem>
          <para>
            expansion coefficients for even angular and radial Mathieu function for the same <emphasis>m</emphasis> and <emphasis>q</emphasis> (optional, for speed).
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>y</term>
        <listitem>
          <para>
            value of the 'Ce' function or its first derivative
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      <link linkend="mathieu_rad_ce">Mathieu_rad_ce</link> computes even radial (modified) Mathieu function of the first kind
      <math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>C</mi><msubsup><mi>e</mi><mi>m</mi><mrow/></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math>
      &#160;or its first derivative <math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>C</mi><msubsup><mi>e</mi><mi>m</mi><mrow><mo>&#160;</mo><mo>'</mo></mrow></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math>
      &#160;using the following formulas [1, 2] from known 
      <math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>M</mi><msubsup><mi>c</mi><mrow><mo>&#160;</mo><mi>m</mi></mrow><mrow><mo>&#160;</mo><mo>(</mo><mn>1</mn><mo>)</mo></mrow></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math> 
      &#160;function (or its derivative).
    </para>
    <para>Functions 
		<math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>C</mi><msub><mi>e</mi><mrow><mo>&#160;</mo><mn>2</mn><mi>n</mi></mrow></msub><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math> 
		&#160;and&#160;
		<math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>C</mi><msub><mi>e</mi><mrow><mo>&#160;</mo><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></msub><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math>
		&#160;are calculated by 20.6.15 [1] with multipliers <math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><msub><mi>p</mi><mrow><mn>2</mn><mi>n</mi></mrow></msub></math>
		&#160;and&#160;
		<math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><msub><mi>p</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mn>1</mn></mrow></msub></math>
		, described in (3)-(4) [2, p. 368-369]:
    </para>    
    <para>
		<math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mtable  columnalign="left"><mtr><mtd><mi>C</mi><msub><mi>e</mi><mrow><mn>2</mn><mi>n</mi></mrow></msub><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><mfrac><msub><mi>p</mi><mrow><mn>2</mn><mi>n</mi></mrow></msub><msup><mfenced><mrow><mo>-</mo><mn>1</mn></mrow></mfenced><mi>n</mi></msup></mfrac><mi>M</mi><msubsup><mi>c</mi><mrow><mn>2</mn><mi>n</mi></mrow><mrow><mo>(</mo><mn>1</mn><mo>)</mo></mrow></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><mfrac><mrow><mi>c</mi><msub><mi>e</mi><mrow><mn>2</mn><mi>n</mi></mrow></msub><mo>&#160;</mo><mfenced><mrow><mstyle displaystyle="true"><mfrac><mi mathvariant="normal">&#960;</mi><mn>2</mn></mfrac></mstyle><mo>,</mo><mo>&#160;</mo><mi>q</mi></mrow></mfenced><mi>c</mi><msub><mi>e</mi><mrow><mn>2</mn><mi>n</mi></mrow></msub><mo>&#160;</mo><mfenced><mrow><mn>0</mn><mo>,</mo><mo>&#160;</mo><mi>q</mi></mrow></mfenced></mrow><mrow><msup><mfenced><mrow><mo>-</mo><mn>1</mn></mrow></mfenced><mi>n</mi></msup><mo>&#160;</mo><msubsup><mi>A</mi><mn>0</mn><mrow><mn>2</mn><mi>n</mi></mrow></msubsup></mrow></mfrac><mi>M</mi><msubsup><mi>c</mi><mrow><mn>2</mn><mi>n</mi></mrow><mrow><mo>(</mo><mn>1</mn><mo>)</mo></mrow></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo><mo>,</mo></mtd></mtr><mtr><mtd/></mtr><mtr><mtd><mi>C</mi><msub><mi>e</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn><mo>&#160;</mo></mrow></msub><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><mfrac><msub><mi>p</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></msub><msup><mfenced><mrow><mo>-</mo><mn>1</mn></mrow></mfenced><mrow><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></msup></mfrac><mi>M</mi><msubsup><mi>c</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow><mrow><mo>(</mo><mn>1</mn><mo>)</mo></mrow></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo><mo>&#160;</mo><mo>=</mo><mo>&#160;</mo><mfrac><mrow><mi>c</mi><msubsup><mi>e</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow><mrow><mo>&#160;</mo><mo>&#160;</mo><mo>'</mo></mrow></msubsup><mfenced><mrow><mstyle displaystyle="true"><mfrac><mi mathvariant="normal">&#960;</mi><mn>2</mn></mfrac></mstyle><mo>,</mo><mo>&#160;</mo><mi>q</mi></mrow></mfenced><mi>c</mi><msub><mi>e</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></msub><mo>&#160;</mo><mfenced><mrow><mn>0</mn><mo>,</mo><mo>&#160;</mo><mi>q</mi></mrow></mfenced></mrow><mrow><msup><mfenced><mrow><mo>-</mo><mn>1</mn></mrow></mfenced><mrow><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></msup><mo>&#160;</mo><msqrt><mi>q</mi></msqrt><mo>&#160;</mo><msubsup><mi>A</mi><mn>1</mn><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow></msubsup></mrow></mfrac><mi>M</mi><msubsup><mi>c</mi><mrow><mn>2</mn><mi>n</mi><mo>&#160;</mo><mo>+</mo><mo>&#160;</mo><mn>1</mn></mrow><mrow><mo>(</mo><mn>1</mn><mo>)</mo></mrow></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo><mo>.</mo></mtd></mtr></mtable></math>
    </para>
    <para>
		The formulations for derivatives were found symbolically from the equations above.
    </para>
	<para>
		During unit-testing of this function values of even radial (modified) function of the first kind
		<math xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt"><mi>C</mi><msubsup><mi>e</mi><mi>m</mi><mrow/></msubsup><mo>(</mo><mi mathvariant="normal">z</mi><mo>,</mo><mi>q</mi><mo>)</mo></math>
      &#160; were compared to Tables 1-6 from [3]. The results are very close.
	</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example">
      <![CDATA[
// Example: Ce0 and Ce1 (for comparison with Fig. 4a, p. 236 of the article J. C. Gutiérrez-Vega,
//			et al. Mathieu functions, a visual approach [4]) with first derivative
    f=scf(); mrgn = 50; font_sz = 4;
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ;
    f.figure_name = 'Ce0 and Ce1 (for comparison with Fig. 4a, p. 236 of the article'+...
					' J. C. Gutiérrez-Vega, et al. Mathieu functions, a visual approach) with first derivative';

    z = linspace(0, 2, 100);
    fun = 1; der = 0;
    
    m = 0;
    subplot(2,2,1)
        plot(z, mathieu_rad_ce(m, 1, z, fun), 'r', z, mathieu_rad_ce(m, 2, z, fun), 'b',...
		z, mathieu_rad_ce(m, 3, z, fun), 'g')
        xgrid
        ty1 = '$Ce_0(z,q)$'; xlabel('$z$'); ylabel(ty1);
        legend('$q = 1$', '$q = 2$', '$q = 3$');
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(2,2,3)
        plot(z, mathieu_rad_ce(m, 1, z, der), 'r-.', z, mathieu_rad_ce(m, 2, z, der), 'b-.',...
		z, mathieu_rad_ce(m, 3, z, der), 'g-.')
        xgrid
        ty2 = '$Ce^{\ \prime}_0(z,q)$'; xlabel('$z$'); ylabel(ty2);
        legend('$q = 1$', '$q = 2$', '$q = 3$',pos=2);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    
    m = 1;
    subplot(2,2,2)
        plot(z, mathieu_rad_ce(m, 1, z, fun), 'r', z, mathieu_rad_ce(m, 2, z, fun), 'b',...
		z, mathieu_rad_ce(m, 3, z, fun), 'g')
        xgrid
        ty1 = '$Ce_1(z,q)$'; xlabel('$z$'); ylabel(ty1);
        legend('$q = 1$', '$q = 2$', '$q = 3$');
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(2,2,4)
        plot(z, mathieu_rad_ce(m, 1, z, der), 'r-.', z, mathieu_rad_ce(m, 2, z, der), 'b-.',...
		z, mathieu_rad_ce(m, 3, z, der), 'g-.')
        xgrid
        ty2 = '$Ce^{\ \prime}_1(z,q)$'; xlabel('$z$'); ylabel(ty2);
        legend('$q = 1$', '$q = 2$', '$q = 3$',pos=2);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    f.figure_position=[sz(1) sz(2)]; f.figure_size=[sz(3) sz(4)]; 
]]>
    </programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="mathieu_rad_mc">mathieu_rad_mc</link>
      </member>
      <member>
        <link linkend="mathieu_rad_fey">mathieu_rad_fey</link>
      </member>
      <member>
		<link linkend="mathieu_Arm">mathieu_Arm</link>
      </member>
    </simplelist>
  </refsection>  
  <refsection>
    <title>Authors</title>
    <para>
      R.Coisson and G. Vernizzi, <emphasis>Parma University</emphasis>
    </para>
    <para>
      N. O. Strelkov, <emphasis>NRU MPEI</emphasis>
    </para>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         1. M. Abramowitz and I.A. Stegun. Handbook of Mathematical Functions, Dover, New York, 1965.
       </para>
       <para>
		 2. N. W. McLachlan. Theory and Application of Mathieu Functions, Oxford Univ. Press, 1947.
       </para>
       <para>
		 3. E. T. Kirkpatrick. Tables of Values of the Modified Mathieu Functions. Mathematics of Computation, Vol. 14, No. 70 (Apr., 1960), pp. 118-129. (online at <ulink url="http://www.ams.org/journals/mcom/1960-14-070/S0025-5718-1960-0113288-4/S0025-5718-1960-0113288-4.pdf">AMS</ulink>).
       </para>
	   <para>
        4. J. C. Gutiérrez-Vega, R. M. Rodríguez-Dagnino, M. A. Meneses-Nava, and S. Chávez-Cerda, "Mathieu functions, a visual approach", American Journal of Physics, 71 (233), 233-242. An introduction to applications (online at <ulink url="http://www.df.uba.ar/users/sgil/physics_paper_doc/papers_phys/modern/matheiu0.pdf">http://www.df.uba.ar/users/sgil/physics_paper_doc/papers_phys/modern/matheiu0.pdf</ulink>).
	   </para>
   </refsection>
</refentry>
