<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
 * Copyright (C) X. K. Yang
 * Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 *
 * This file must be used under the terms of the GPL Version 2
 * http://www.gnu.org/licenses/gpl-2.0.html
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en_US" xml:id="mathieu_mathieuexp">
  <info>
    <pubdate>$LastChangedDate: 02-02-2011 $</pubdate>
  </info>
  <refnamediv>
    <refname>mathieu_mathieuexp</refname>
    <refpurpose>Evaluates the characteristic exponent.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[nu,c] = mathieu_mathieuexp(a,q,ndet)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>a, q</term>
        <listitem>
          <para>
            parameters in Mathieu Equation:
<math display="block" xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt">
<mrow>
  <mfrac>
    <mrow>
      <msup>
              <mi>d</mi>
            <mn>2</mn>
      </msup>
      <mi>y</mi>
    </mrow>
    <mrow>
      <msup>
            <mrow>
              <mi>d</mi>
              <mi mathvariant="normal">z</mi>
            </mrow>
            <mn>2</mn>
      </msup>
    </mrow>
  </mfrac>
  <mo>+</mo>
  <mfenced open="[" close="]" separators=",">
    <mrow>
      <mi>a</mi>
      <mo>-</mo>
      <mn>2</mn>
      <mi>q</mi>
      <mi>cos</mi>
      <mfenced open="(" close=")" separators=",">
        <mrow>
          <mo>2</mo>
          <mi mathvariant="normal">z</mi>
        </mrow>
      </mfenced>
    </mrow>
  </mfenced>
  <mi>y</mi>
  <mo>=</mo>
  <mn>0</mn>
</mrow>
</math>
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ndet</term>
        <listitem>
          <para>
            a positive integer number: it is the matrix dimension used in the algorithm. Precision increases by increasing ndet. Typical value is ndet=12.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>nu</term>
        <listitem>
          <para>
            characteristic exponent of mathieu equation
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>c</term>
        <listitem>
          <para>
            coefficients of the expansion of the periodic factor
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      <link linkend="mathieu_mathieuexp">mathieu_mathieuexp</link> evaluates the characteristic exponent <emphasis>nu</emphasis>, corresponding to solutions of Mathieu Equation
     </para>
    <para>
<math display="block" xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt">
<mrow>
  <mfrac>
    <mrow>
      <msup>
              <mi>d</mi>
            <mn>2</mn>
      </msup>
      <mi>y</mi>
    </mrow>
    <mrow>
      <msup>
            <mrow>
              <mi>d</mi>
              <mi mathvariant="normal">z</mi>
            </mrow>
            <mn>2</mn>
      </msup>
    </mrow>
  </mfrac>
  <mo>+</mo>
  <mfenced open="[" close="]" separators=",">
    <mrow>
      <mi>a</mi>
      <mo>-</mo>
      <mn>2</mn>
      <mi>q</mi>
      <mi>cos</mi>
      <mfenced open="(" close=")" separators=",">
        <mrow>
          <mo>2</mo>
          <mi mathvariant="normal">z</mi>
        </mrow>
      </mfenced>
    </mrow>
  </mfenced>
  <mi>y</mi>
  <mo>=</mo>
  <mn>0</mn>
</mrow>
</math>
    </para>
    <para>where <emphasis>a</emphasis> and <emphasis>q</emphasis> are real variables.</para>
    <para>
      The alghoritm consider two different cases: a=(2k)&#178;
      or not (<emphasis>k</emphasis> integer).
      <emphasis>nu</emphasis> is such that its real part belongs to the interval [0,1).
      Of course, every other solutions are obtained by the formula
      &#177;nu+2⋅k, with <emphasis>k</emphasis> integer.
    </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example">
      <![CDATA[
// Stability curves of Mathieu equation
// (see fig. 3 of [1] for detailed view or change N to 180 manually)
N=30;
a=linspace(-2, 7, N);
q=linspace(-4, 4, N);
st=zeros(N,N);
for j=1:length(a)
    for h=1:length(q)
        st(h, j)=mathieu_mathieuexp(a(j), q(h), 12);
    end 
end
contour(q, a, imag(st), N);
xtitle('Stability curves of Mathieu equation', 'q', 'a');
]]>
    </programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="mathieu_mathieuf">mathieu_mathieuf</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <para>
      R.Coisson and G. Vernizzi, <emphasis>Parma University</emphasis>
    </para>
    <para>
      X. K. Yang
    </para>
    <para>
      N. O. Strelkov, <emphasis>NRU MPEI</emphasis>
    </para>
    <para>
      2011 - DIGITEO - Michael Baudin
    </para>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
		1. R. Coïsson, G. Vernizzi and X.K. Yang, "Mathieu functions and numerical solutions of the Mathieu equation", IEEE Proceedings of OSSC2009 (online at <ulink url="http://www.fis.unipr.it/~coisson/Mathieu.pdf">http://www.fis.unipr.it/~coisson/Mathieu.pdf</ulink>).
       </para>
   </refsection>
</refentry>
