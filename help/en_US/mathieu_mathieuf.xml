<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
 * Copyright (C) X. K. Yang
 * Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 *
 * This file must be used under the terms of the GPL Version 2
 * http://www.gnu.org/licenses/gpl-2.0.html
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en_US" xml:id="mathieu_mathieuf">
  <info>
    <pubdate>$LastChangedDate: 02-02-2011 $</pubdate>
  </info>
  <refnamediv>
    <refname>mathieu_mathieuf</refname>
    <refpurpose>Evaluates characteristic values and expansion coefficients.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[ab,c] = mathieu_mathieuf(q,dimM)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>ab</term>
        <listitem>
          <para>
            the characteristic values with ordering 
<math display="block" xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt">
<mrow>
  <msub>
          <mi>a</mi>
        <mn>0</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>a</mi>
        <mn>1</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>1</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>2</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>a</mi>
        <mn>2</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>a</mi>
        <mn>3</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>3</mn>
  </msub>
  <mo>&lt;</mo>
  <mo>...</mo>
  <mspace width="0.5em" />
  <mi>for</mi>
  <mspace width="0.5em" />
  <mi>q</mi>
  <mo>&lt;</mo>
  <mn>0</mn>
  <mspace width="0.5em" />  
</mrow>
</math>
 and 
<math display="block" xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt">
<mrow>
  <msub>
          <mi>a</mi>
        <mn>0</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>1</mn>
  </msub>
  <mo>&#8804;</mo>
  <msub>
          <mi>a</mi>
        <mn>1</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>2</mn>
  </msub>
  <mo>&#8804;</mo>
  <msub>
          <mi>a</mi>
        <mn>2</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>3</mn>
  </msub>
  <mo>&#8804;</mo>
  <msub>
          <mi>a</mi>
        <mn>3</mn>
  </msub>
  <mo>&lt;</mo>
  <mo>...</mo>
  <mspace width="0.5em" />
  <mi>for</mi>
  <mspace width="0.5em" />
  <mi>q</mi>
  <mo>&#8805;</mo>
  <mn>0</mn>
  <mspace width="0.5em" />  
</mrow>
</math>
 , 
for <emphasis>q</emphasis> complex, the ordering is not well-defined.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>c</term>
        <listitem>
          <para>
            coefficients of expansions of Periodic Mathieu Functions ce,se,Ce,Se,Fey,Gey,Fek,Gek. c is a matrix: all values of n from -dimM to dimM are calculated.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>q</term>
        <listitem>
          <para>
            the parameter(real) of the confocal ellipse family (q=0: circle)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>dimM</term>
        <listitem>
          <para>
            matrix dimension in the calculation (default dimM=10)
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      <link linkend="mathieu_mathieuf">Mathieu_mathieuf</link> evaluates characteristic values and expansion coefficients of periodic solutions of the differential equation
    </para>
    <para>
<math display="block" xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt">
<mrow>
  <mfrac>
    <mrow>
      <msup>
              <mi>d</mi>
            <mn>2</mn>
      </msup>
      <mi>y</mi>
    </mrow>
    <mrow>
      <msup>
            <mrow>
              <mi>d</mi>
              <mi mathvariant="normal">z</mi>
            </mrow>
            <mn>2</mn>
      </msup>
    </mrow>
  </mfrac>
  <mo>+</mo>
  <mfenced open="[" close="]" separators=",">
    <mrow>
      <mi>a</mi>
      <mo>-</mo>
      <mn>2</mn>
      <mi>q</mi>
      <mi>cos</mi>
      <mfenced open="(" close=")" separators=",">
        <mrow>
          <mo>2</mo>
          <mi mathvariant="normal">z</mi>
        </mrow>
      </mfenced>
    </mrow>
  </mfenced>
  <mi>y</mi>
  <mo>=</mo>
  <mn>0</mn>
</mrow>
</math>
     (Mathieu equation) </para>
    <para>or to solutions of</para>
    <para>
<math display="block" xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt">
<mrow>
  <mfrac>
    <mrow>
      <msup>
              <mi>d</mi>
            <mn>2</mn>
      </msup>
      <mi>y</mi>
    </mrow>
    <mrow>
      <msup>
            <mrow>
              <mi>d</mi>
              <mi mathvariant="normal">z</mi>
            </mrow>
            <mn>2</mn>
      </msup>
    </mrow>
  </mfrac>
  <mo>+</mo>
  <mfenced open="[" close="]" separators=",">
    <mrow>
      <mi>a</mi>
      <mo>-</mo>
      <mn>2</mn>
      <mi>q</mi>
      <mi>cosh</mi>
      <mfenced open="(" close=")" separators=",">
        <mrow>
          <mo>2</mo>
          <mi mathvariant="normal">z</mi>
        </mrow>
      </mfenced>
    </mrow>
  </mfenced>
  <mi>y</mi>
  <mo>=</mo>
  <mn>0</mn>
</mrow>
</math>
     (Modified Mathieu equation)</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example">
      <![CDATA[
// Eigenvalues (imaginary vs. real part) for complex values of q
// (see fig. 7 of [1])
for t=0.1:0.2:15
    plot( real(2+mathieu_mathieuf(2+t*%i, 6)), imag(mathieu_mathieuf(2+t*%i, 6)), '.');
end
xgrid;
xtitle('Eigenvalues (imaginary vs. real part) for complex values of q', '$Re[\lambda]$', '$Im[\lambda]$');
  ]]>
    </programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="mathieu_mathieuexp">mathieu_mathieuexp</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Used Functions</title>
    <para>besselj</para>
  </refsection>
  <refsection>
    <title>Authors</title>
    <para>
      R.Coisson and G. Vernizzi, <emphasis>Parma University</emphasis>
    </para>
    <para>
      X. K. Yang
    </para>
    <para>
      N. O. Strelkov, <emphasis>NRU MPEI</emphasis>
    </para>
    <para>
      2011 - DIGITEO - Michael Baudin
    </para>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
		1. R. Coïsson, G. Vernizzi and X.K. Yang, "Mathieu functions and numerical solutions of the Mathieu equation", IEEE Proceedings of OSSC2009 (online at <ulink url="http://www.fis.unipr.it/~coisson/Mathieu.pdf">http://www.fis.unipr.it/~coisson/Mathieu.pdf</ulink>).
       </para>
   </refsection>
</refentry>

