// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function [y, ab] = mathieu_mathieu(kind,n,z,q,prec)
    // mathieu_mathieu(''kind'',n,z,q,[prec])
    // evaluates periodic Mathieu functions by calling mathieu_mathieuf(q,dimM)
    // 
    // output: y(n,z) a matrix (each column corresp. to a value of n)
    // input: if no inputs are entered, they will be asked for.  
    //   kind is ''ce'', ''ce_'' (ce first derivative),''se'', ''se_'' (se first derivative),''Ce'',''Se'',(''Fy'',''Gy'',''Fk'',''Gk'' not yet) 
    //   n    is the order, an array of positive integers
    //   z    is the argument, a real row vector
    //   q    parameter of the confocal ellipse family (q=0 : circle)
    //   prec is the precision (default 1e-3)
    // 
    // Matlab program by R.Coisson & G. Vernizzi,
    // Parma University, 2000-2002.


    [%nargout,%nargin] = argn(0)					// Number of arguments in function call
    if %nargin==0 then								// interactive input
        kind=input("kind: se or se_ or ce or ce_ or Se or Ce or Fy (=Fey) etc...?");
        n=input("order: ");
        q=input("q: ");
        numpoints=input("number of points: ");
        zmin=input("minimum z: ");
        zmax=input("maximum z: ");
        z=zmin:(zmax-zmin)/numpoints:zmax;
    end
    if %nargin==5 then
        p=prec;
    else 
        p=0.001;									// default precision 1e-3
    end

    if ((kind=="se" | kind =="se_" | kind=="Se") & or(n==0)) 		//each element of n must be nonzero
        //use Scilab "or" function to substitute "any" in Matlab
        error("''se_0'', ''se`_0'' and ''Se_0'' do not exist"); 
    end
    nn=5+max(n)+abs(fix(q/2));						// default matrix size 2*nn+1
    err=p*2;

    if real(q)>=0 & (kind=="ce" | kind=="ce_" | kind=="Ce" | kind=="Fy" | kind=="Fk")
        ind=2*n+1;
    elseif real(q)>=0 & (kind=="se" | kind=="se_" | kind=="Se" | kind=="Gy" | kind=="Gk")
        ind=2*n;
    elseif real(q)<0 & (kind=="ce" | kind=="ce_" | kind=="Ce" | kind=="Fy" | kind=="Fk")
        ind=4*floor(n/2)+modulo(n,2)+1;				//since n>0, modulo() in Scilab <=> rem() in Matlab
    elseif real(q)<0 & (kind=="se" | kind=="se_" | kind=="Se" | kind=="Gy" | kind=="Gk")
        ind=4*floor(n/2)+modulo(n,2)+3;
    end

    y1=zeros(length(z),length(ind));
    while err>p										// increase nn until err<precision
        nn=nn+2;
        [ab,c]=mathieu_mathieuf(q,nn);
        cc = c(:,ind);
        ab = ab(ind);
        k=-nn:nn;
        if kind=="ce" then
            y2=cos(z'*k)*cc;						// ce function
        elseif kind=="ce_" then           
            kk = ones(max(size(mtlb_double(z))),1)*(-k);
            y2=kk.*sin(z'*k)*cc;					// ce function derivative
        elseif kind=="se" then
            y2=sin(z'*k)*cc;						// se function
        elseif kind=="se_" then           
            kk = ones(max(size(mtlb_double(z))),1)*k;
            y2=kk.*cos(z'*k)*cc;					// se function derivative		
        elseif kind=="Ce" then						// Ce fuction
            kk = ones(max(size(mtlb_double(z))),1)*((-1) .^floor(k/2));
            y2=(kk.*(besselj(k,2*sqrt(q)*cosh(z'))))*cc; 	//besselj is a built-in function of Matlab, Scilab also.
        elseif kind=="Se" then						// Se function
            kk=tanh(z')*(-1).^floor((k-1)/2);
            oo=ones(length(z),1)*k;
            y2=(kk.*oo.*(besselj(k,2*sqrt(q)*cosh(z'))))*cc;  
            //elseif kind=="Fy" | kind=="Gy" then 		// other functions can be added here 
        end
        err=max(abs(y2-y1));
        y1=y2;
    end
    y=y2;											// each column corresp. to one order


endfunction
