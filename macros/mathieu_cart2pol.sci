// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function [theta, rho, z] = mathieu_cart2pol (x, y, z)
    // mathieu_cart2pol (x, y, [z])
    // transform from Cartesian to polar (cylindrical) coordinates
    // 
    // output: polar coordinates
    //        theta - angle
    //        rho   - radius
    //        z     - z height (cylindrical case)
    // input: Cartesian coordinates - x, y, z
    // 
    theta = atan(y, x);
    rho = sqrt(x.^2 + y.^2);
endfunction
