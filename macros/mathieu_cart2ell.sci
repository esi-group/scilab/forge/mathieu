// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

// vvvv original code from G. Vernizzi
//function [u,v] = mathieu_cart2ell(x,y,c) 
    // questo programma calcola la trasformazione inversa
    // di x=c*cosh(u)*cos(v),y=c*sinh(u)*sin(v)

    //ro2=(x.^2+y.^2+c^2);
    //R=sqrt((ro2+sqrt(ro2^2-4*c^2 .*x.^2))/(2*c^2))

    //u=acosh(sqrt((ro2+sqrt(ro2.^2-4*c^2.*x.^2))/(2*c^2)));
    //v=acos(sqrt((ro2-sqrt(ro2.^2-4*c^2.*x.^2))/(2*c^2)));

    //the followign are test code
    //[x,y]=meshgrid(-2.0001:.1:2.0001,-2:.1:2);
    //[u,v]=mathieu_cart2ell(x,y,1);
    //xset("colormap",jetcolormap(10))   
    //xset("auto clear","off") 
    //contour(-2.0001:.1:2.0001,-2:.1:2,u,10)
    //contour(-2.0001:.1:2.0001,-2:.1:2,v,10)
    //mesh(x,y,v)
    //mesh(x,y,u)
    //set(gca(),"rotation_angles",[180,180])

    //----------------THE FOLLOWING CANNOT BE TRANSLATED--------
    //mf1 not defined
    // the arguments provided to mathieu_mathieuf seem to be wrong.
    //----------------------------------------------------------
    //for k=1:41;w1(k,:)=mf1(''ce'',0,1,v(k,:));end
    //for q=0.1:.1:2;hold on;plot(mathieu_mathieuf(''ce'',i*q,1,5),''.'');end
//endfunction

// ^^^^ original code from G. Vernizzi



// below is code, written by N. O. Strelkov based on formulae from N.W.McLachlan (9.11, p. 170).

function [xi, eta] = mathieu_cart2ell(x, y, f) 
// Converts from Cartesian coordinates (x,y) to elliptical coordinates (xi, eta).
    // output: elliptical coordinates
    //        xi  - coordinates of the confocal elliptic cylinders centered on the origin. [0, ∞]
    //        eta - coordinates of the asymptotic angle of confocal hyperbolic cylinders symmetrical about the x-axis. [0, 2π)
    // input: 
    //        x, y - Cartesian coordinates
    //        f    - the distance between the foci and the origin
    // 
    
    z = x + %i * y; 
    el = acosh(z/f);
    xi = real(el);
    eta = imag(el);
endfunction
