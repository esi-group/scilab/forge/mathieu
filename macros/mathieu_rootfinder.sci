// Copyright (C) 2013-2016 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function q_found_out = mathieu_rootfinder(m, xi0, q_start, q_delta, q_roots_tot, q_fun_tol, q_maxiter, func_name, fun_or_der, do_print, do_plot)
// MATHIEU_ROOTFINDER Rootfinder for radial Mathieu functions. 
//   Finds 'q' values of given radial function type with known order 'm' and radial argument 'xi0', which
//   satisfies the equation RMF_m(q,xi0) = 0 (Dirichlet boundary condition) or RMF_m'(q,xi0) = 0 (Neumann boundary condition).
//
//   Arguments:
//      m - order of radial function
//      xi0 - value of radial argument in elliptic coordinates (xi)
//      q_start - initial guess for 'q'
//      q_delta - step increment on search of 'q'
//      q_roots_tot - number of roots to be found
//      q_fun_tol - relative tolerance of zero value (fsolve tol)
//      q_maxiter - maximum number if iterations
//      func_name - radial Mathieu function type (should be on of 'Ce', 'Se', 'Fey', 'Gey', 'Mc1', 'Ms1', 'Mc2', 'Ms2')
//      fun_or_der - search roots of function (%t - Dirichlet boundary condition) or derivative (%f - Neumann boundary condition)
//      do_print - print infromation in console? (boolean)
//      do_plot - plot graphics of rootfinding? (boolean)

    tic(); // start time
    kind = 1; // function kind
    has_kind = %t; // kind existence (call function with 5 or 4 arguments)
    func_name_leg = func_name; // название функции для легенды
    
    // select radial Mathieu function
    select func_name
        case 'Ce' then
            mathieu_fun_hndl = mathieu_rad_ce;
            has_kind = %f;
        case 'Se' then
            mathieu_fun_hndl = mathieu_rad_se;
            has_kind = %f;
        case 'Fey' then
            mathieu_fun_hndl = mathieu_rad_fey;
            has_kind = %f;
        case 'Gey' then
            mathieu_fun_hndl = mathieu_rad_gey;
            has_kind = %f;
        case 'Mc1' then
            mathieu_fun_hndl = mathieu_rad_mc;
            func_name_leg = 'Mc^{(1)}';
            has_kind = %t;
        case 'Ms1' then
            mathieu_fun_hndl = mathieu_rad_ms;
            func_name_leg = 'Ms^{(1)}';
            has_kind = %t;
        case 'Mc2' then
            mathieu_fun_hndl = mathieu_rad_mc;
            kind   = 2; 
            func_name_leg = 'Mc^{(2)}';
            has_kind = %t;
        case 'Ms2' then
            mathieu_fun_hndl = mathieu_rad_ms;
            kind   = 2;
            func_name_leg = 'Ms^{(2)}';
            has_kind = %t;
        else
            error('func_name should be Ce or Se or Fey or Gey or Mc1 or Ms1 or Mc2 or Ms2');
    end
    
    // Internal function for rootfinding
    function f_val = root_find(q)
      if has_kind then  
        f_val = mathieu_fun_hndl(m, q, xi0, fun_or_der, kind);
      else
        f_val = mathieu_fun_hndl(m, q, xi0, fun_or_der);
      end
    endfunction
        
    // Local function for bisection method from
    // http://stemsoup.wordpress.com/2011/10/21/the-bisection-method-using-scilab/
    function root = Bisection_Method(func_name, xl, xr, rel_tol)
        fl = func_name(xl); //evaluate the function at xl
        fr = func_name(xr); //evaluate the function at xr
    
        //test to see if there is a root in the interval
        if (fr*fl >= 0) then
            root = %nan;
        else
            //there is a root in the interval
            exact_root_found = %f;
            rel_tol_curr = 100;
            xr_new =(xr + xl)/2;
            
            //iteratively progress toward to root until it is found or it
            //is approximated with a relative error less than required
            while rel_tol_curr > rel_tol & ~exact_root_found
                fl = func_name(xl);
                fr = func_name(xr_new);
    
                if(fl*fr < 0) then
                    xr = xr_new;
                elseif(fl*fr > 0) then
                    xl = xr_new;
                elseif(fl*fr == 0) then
                    root = xr_new;
                    exact_root_found = %t;
                end
    
                //calculate the approximate relative error for the iteration
                xr_old = xr_new;
                xr_new =(xr + xl)/2;
                rel_tol_curr = abs((xr_new - xr_old)/xr_new)*100;
            end
    
            //if error criterion has been met but the exact answer has not
            //been found, set the root to the adequate approximation.
            if ~exact_root_found then
                root = xr_new;
            end
        end
    endfunction    
    

  // 1/2 : initial rootfinding by bisection method
   // variable initialization
    Q = [];
    i = 1;j = 1;
    q_i = 0;

    if do_print then
        printf('[ Part 1/2: Bisection root pre-search ]\n')
    end
    
    while (q_roots_tot+1 > length(q_i)) & (j < q_maxiter)
      try  
        q_r = Bisection_Method(root_find, q_start, q_start + q_delta, 0.1);
        
        if ~isnan(q_r) then
            Q(i) = q_r
            [q_unique, q_i] = unique(Q);

            if do_print
                printf('  [%5d ]: q_start = %f, q_delta = %f, q_found_bisec = %f\n', i, q_start, q_delta, q_r);
            end
            i = i + 1;
        end
       catch 
            q_start = q_start + q_delta;
            if do_print
                printf('  [%5d ]: root not found (q_start = %f)\n', i, q_start);
            end
       end
            q_start = q_start + q_delta;
            j = j + 1;
    end
    
    if do_print
        // show array of found 'q' roots
        printf('\nq_found_bisec (len = %d):', length(Q));
        disp(Q)
    end
    
  // 2/2 root search with fsolve
  if length(Q) > 0 then
    if do_print then
        printf('\n[ Part 2/2: Fsolve root search ]\n')
    end

    for i=1:length(Q)
        [Q_exact_t(i), fQ_exact_t(i)] = fsolve(Q(i), root_find, q_fun_tol);
         
        if do_print
            if fun_or_der then
                printf('  [ root %5d ] q_found_fsolve=%f, %s_%d(q,%e)=%e\n', i, Q_exact_t(i), func_name, m, xi0, fQ_exact_t(i));
            else
                printf('  [ root %5d ] q_found_fsolve=%f, %sʹ_%d(q,%e)=%e\n', i, Q_exact_t(i), func_name, m, xi0, fQ_exact_t(i));
            end
        end
    end

    // filter roots - save only unique
    [_Q_exact, nQe] = unique(round(Q_exact_t*1000));
    
    if length(nQe) < q_roots_tot then
        error(sprintf('Unable to find desired number of roots (%d iterations of %d)!',j, q_maxiter))
    elseif length(nQe) == q_roots_tot then
        Q_exact = Q_exact_t(nQe);
        fQ_exact = fQ_exact_t(nQe);
    elseif length(nQe) == q_roots_tot+1 then
        Q_exact = Q_exact_t(nQe(1:q_roots_tot));
        fQ_exact = fQ_exact_t(nQe(1:q_roots_tot));        
    end
    
    q_found_out = Q_exact;

    // show array of finaly found roots
    if do_print
        printf('\nq_found_out (len = %d):', length(q_found_out));
        disp(q_found_out)
    end    
    
  // show plot with roots and function (or derivative)
    if do_plot then
        fh=figure('BackgroundColor',[1,1,1]);
        
        if (getos() == "Windows") then
          font_size_normal = 2; 
        else
          font_size_normal = 3; 
        end

        s_wh = get(0, 'screensize_px');
        margin_h = 50;
        margin_w = 5;
        max_w = s_wh(3)-2*margin_w;
        max_h = s_wh(4)-2*margin_h;
        
        qq = linspace(0, max(Q_exact), 1000);
        ff = zeros(1:length(qq));
        ZZ = ff;
        for i=1:length(qq)
           ff(i) = root_find(qq(i));
        end

        plot(qq, ff, 'b', qq, ZZ, 'g--', Q_exact, fQ_exact, 'rx');
        
        if fun_or_der then
            func_leg = func_name_leg + '_{' + sprintf('%d',m) + '}\ (q,\ ' + sprintf('%f',xi0) + ')';
        else
            func_leg = func_name_leg + '_{' + sprintf('%d',m) + '}^{\ \prime}\ (q,\ ' + sprintf('%f',xi0) + ')';
        end

        tl = '$'+func_leg+'\ = 0$';
        xtitle(tl, '$q$', '$'+func_leg+'$');
        legend('$'+func_leg+'$', '$0$', tl, -6);
        xgrid;
        fh.figure_position = [margin_w margin_h];
        fh.figure_size = [max_w max_h];

        h = gca();
        h.font_size = font_size_normal+1; h.title.font_size=font_size_normal+1; h.x_label.font_size=font_size_normal+1; h.y_label.font_size=font_size_normal+1;
        leg = h.children(1);
        leg.font_size = font_size_normal+1;
        leg.background = -2; // white
        h.margins(4) = 0.2;
    end
  else
      if j==q_maxiter then
          printf('\n\nNo roots found for %d iterations!\n', q_maxiter);
      end
      q_found_out = [];
  end
    if do_print then
        printf('\n[ mathieu_rootfinder took %3.3e seconds ]\n\n', toc())
    end
 
endfunction
