// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function y = mathieu_rad_se( m, q, z, fun_or_der, Brm )
//MATHIEU_RAD_SE Odd radial (modified) Mathieu function or its first derivative.
//   Function for calculation of odd radial (modified) Mathieu function 'Se' or its
//   first derivative
//
//   Arguments:
//       m - function order
//       q - value of parameter q
//       z - argument
//    fun_or_der - calculate function (1) or derivative (0)
//      Brm - expansion coefficients for the same order m and value of q.
   
    // calculate Brm expansion coefficients inside or outside of this function
    if argn(2)<5
        // calculate expansion coefficients here for given m and q
        Brm = mathieu_Brm(m,q);
    end

    // calculation of Mathieu function 'Ms1' or its first derivative (AS 20.6.9-20.6.10 with 20.4.13)
        tMs = mathieu_rad_ms( m, q, z, fun_or_der, 1, Brm );

    // calculation of Mathieu function 'Se' or its first derivative (AS 20.6.15)
    if modulo(m, 2) == 0 // 2r+2
            // multiplier s2n2 (ML p. 369 (6)): 
            // s2n2 = se2n+2'(0,q) * se2n+2'(%pi/2,q) / B2_2n+2
            der = 0;
            s2n2 = mathieu_ang_se(m, q, 0, der, Brm) * mathieu_ang_se(m, q, %pi/2, der, Brm) / (q * Brm(1));
            mult = s2n2 / (-1) ^ (m/2);
    else // 2r+1
            // multiplier s2n1 (ML p. 369 (5)): 
            // s2n1 = se2n+1'(0,q) * se2n+1(%pi/2,q) / (sqrt(q)*B1_2n+1)
            fun = 1; der = 0;
            s2n1 = mathieu_ang_se(m, q, 0, der, Brm) * mathieu_ang_se(m, q, %pi/2, fun, Brm) / (sqrt(q) * Brm(1));
            mult = s2n1 / (-1) ^ ((m-1)/2);
    end
    
    // returning function of its derivative
    y = real(mult * tMs);
endfunction
