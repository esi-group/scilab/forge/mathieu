// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function [theta, rho, z] = mathieu_ell_in_pol (a, b, theta, z)
    // mathieu_ell_in_pol (a, b, theta, z)
    // calculates polar coordinates of a point at angle (theta) on ellipse with
    // major semiaxis (a) and minor semiaxis (b)
    // 
    // output: polar coordinates
    //        theta - polar angle
    //        rho   - polar radius
    //        z     - polar heigth (cylindrical case)
    // input: a     - major semiaxis (on x axis)
    //        b     - minor semiaxis (on y axis)
    //        theta - polar angle
    //        z     - height (cylindrical case)
    
    e = sqrt (a^2 - b^2) / a; // eccentricity
    rho = b ./ sqrt( 1 - (e.*cos(theta)).^2 ); 
endfunction
