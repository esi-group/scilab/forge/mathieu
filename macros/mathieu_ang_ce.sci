// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function y = mathieu_ang_ce( m, q, z, fun_or_der, Arm )
//MATHIEU_ANG_CE Even angular Mathieu function or its first derivative.
//   Function for calculation of even angular Mathieu function 'ce' or its
//   first derivative
//
//   Arguments:
//       m - function order
//       q - value of parameter q
//       z - argument
//    fun_or_der - calculate function (1) or derivative (0)
//      Arm - expansion coefficients for the same order m and value of q.

    // calculate Arm expansion coefficients inside or outside of this function
    if argn(2)<5
        // calculate expansion coefficients here for given m and q
        Arm = mathieu_Arm(m, q);
    end

    // indexes
    k = 0:length(Arm)-1;

    // even order
    if modulo(m,2)==0
        rr = 2*k;
    // odd order
    else
        rr = 2*k+1;
    end

    // calculation of function or its first derivative
    if fun_or_der
        // function
        y = Arm(k+1)' * cos(rr(:)*z);
    else
        // derivative
        y = -rr .* Arm(k+1)' * sin(rr(:)*z);
    end
endfunction
