// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function [ab,c] = mathieu_mathieuf(q,n) 
    // [ab,c]=mathieu_mathieuf(q,dimM) 
    // evaluation of characteristic values and expansion coefficients 
    // of periodic solutions of the differential equation 
    // y''''(z)+(a-2*q cos(2*z))*y(z)=0         (Mathieu equation) 
    // or to solutions of 
    // (y''''(z)-(a-2*q cosh(2*z))*y(z)=0)     (Modified Mathieu equation) 
    // 
    // output: 
    // ab = the Characteristic Values 
    //    with ordering:  a0<a1<b1<b2<a2<a3<b3<... for q<0 
    //                    a0<b1<a1<b2<a2<b3<a3<... for q>0 
    //     (for q complex, the ordering is not well-defined) 
    // c =  coefficients of expansions of Periodic Mathieu Functions 
    //      ce,se,Ce,Se,Fey,Gey,Fek,Gek 
    //              c is a matrix : 
    //              all values of n from -dimM to dimM are calculated 
    //     
    // input: 
    //     q:    the parameter (real) 
    //  dimM:  Matrix dimension in the calculation (default dimM=10) 
    // 
    // Matlab program by R.Coisson & G. Vernizzi; 
    // Parma University, 2001-2002. 
    d0=[-n:n].^2;							// diagonal part 
    d1=q*ones(1,2*n-1);						// upper and lower sub-diagonal part 
    m=diag(d0)+diag(d1,2)+diag(d1,-2);		// build matrix 
    [U,D]=spec(m);							// diagonalization 
    [ab,I]=gsort(diag(D));					// "ordering" // sort is deprecated, removed from Scilab 5.3
    ab=ab($:-1:1);							//Since "sort" in Scilab is decreasing
    I=I($:-1:1);							//use this method to filp the array	
    U=U(:,I);
    Id=eye(2*n+1,2*n+1);					// Identity matrix 
    Gamma=Id(:,$:-1:1);						// flipping matrix 
    Pplus=mtlb_a(Id,Gamma)/2;				//Positive projector 
    Pminus=mtlb_s(Id,Gamma)/2;				//Negative projector 
    Uplus = Pplus*U;						//Projecting 
    Uminus = Pminus*U;

    if real(q)>=0 then						//Ordering for a_k,b_k 
        ind=[2:2:2*n+1];
    else
        ind=union([3:4:2*n+1],[4:4:2*n+1]);
    end;

    Uplus(:,ind)=Uminus(:,ind);				//New matrix U                    
    Unew = Uplus;
    c=Unew./(sqrt(2*ones(2*n+1,1)*diag(Unew'*Unew)'));		//normalization 

    z0=sum(c,"r");											//Value at  z=0 
    dz0=[-n:n]*c;											//derivative at z=0 

    // both z0 and dz0 have to be positive or zero. 
    // z0 .*dz0 must be a vector of zeroes; if not it has to be cut: 
    // cut=min(find(abs(z0 .*dz0)>10^(-4))) 

    //flipping directions 
    index=find(real(z0+dz0)<0);
    c(:,index)=-c(:,index);


    // example: commands for stability curves: 
    // h=0;for j=-10:.1:10;h=h+1; q(h)=j;a(:,h)=mathieu_mathieuf(j,10);end 
    // plot(q,a) 
endfunction

//note: in this program, the return values of spec functoin are different
//from those in Matlab, so during the calculation, the values of 
//some variables may differ, but that doesn't affect the result, 
//since the algorithms are the same.
