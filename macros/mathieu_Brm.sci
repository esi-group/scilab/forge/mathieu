// Copyright (C) 2012-2016 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

// local function for computation spdiags (downloaded from http://users.powernet.co.uk/kienzle/octave/matcompat/scripts/fake-sparse/spdiags.m)
 // Copyright (C) 2000-2001 Paul Kienzle 
 //
 // This program is free software; you can redistribute it and/or modify
 // it under the terms of the GNU General Public License as published by
 // the Free Software Foundation; either version 2 of the License, or
 // (at your option) any later version.
 //
 // This program is distributed in the hope that it will be useful,
 // but WITHOUT ANY WARRANTY; without even the implied warranty of
 // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 // GNU General Public License for more details.
 //
 // You should have received a copy of the GNU General Public License
 // along with this program; if not, write to the Free Software
 // Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 // spdiags(v,c,m,n): use columns of v as the diagonals c of mxn matrix A
 //
 // -c lower diagonal, 0 main diagonal, +c upper diagonal
 // Fake sparse function: represents sparse matrices using full matrices

//// Program was modified by N. O. Strelkov for needs of this toolbox
 function [A, c] = mathieu_spdiags(v,c,m,n)     
      // Create new matrix of size mxn using v,c
      A = zeros(m,n);
      [nr, nc] = size(v);
      ridx = [1:nr]'*ones(1,nc) - ones(nr,1)*c(:)';
      Aidx = ridx + m*[0:nr-1]'*ones(1,nc);
      idx = find(ridx > 0 & ridx <= m);
      A(Aidx(idx)) = v(idx);
 endfunction

// main function
function [ Brm, bm ] = mathieu_Brm( m, q )
//     Compute expansion coefficients for odd angular and radial Mathieu functions
//     Input :
//              m  - order of Mathieu functions
//              q  - parameter of Mathieu functions (q can be positive, negative or complex number)

//     Output:  Brm(k) - expansion coefficients of odd Mathieu functions (k= 1,2,...,n)
//                         Brm(1),Arm(2),Arm(3),... correspond to
//                         B2,B4,B6,... for se_2m+2
//                         B1,B3,B5,... for se_2m+1
//              bm --- Characteristic value of Mathieu functions for given m and q
//

//// This file must be used under the terms of the Simplified BSD License
//// This program was originally written by Da Ma, Southeast University, China (fcoef1.m, http://www.mathworks.com/matlabcentral/fileexchange/27101-general-mathieu-functions-with-arbitrary-parameters-v1-0 ).

//// Program was modified by N. O. Strelkov for needs of this toolbox

    // 0-th odd function is not exist
    if m==0
        error('m must be positive');
    end

    // asymptotic formulaes from Fortran 'fcoef' function of S. Zhang & J. Jin "Computation of Special Functions" (Wiley, 1996).
    // online at: http://jin.ece.illinois.edu/specfunc.html
    if(abs(q) <= 1.0d0)
        qm = 7.5 + 56.1.*sqrt(abs(q)) - 134.7.*abs(q) + 90.7.*sqrt(abs(q)).*abs(q);
    else
        qm = 17.0 + 3.1.*sqrt(abs(q)) - .126.*abs(q) + .0037.*sqrt(abs(q)).*abs(q);
    end

    // calculate the size of matrix
    n = fix(qm+0.5.*fix(m));
    
    // tri-diagonal matrix construction and eigenvalues finding is below
    if modulo(m,2) == 0 then // even order
      e1 = q .*ones(n,1);
      e2 = zeros(n,1);
      e3 = q .*ones(n,1);
      
      nn=1:n;
      e2(nn,1) = (2*nn').^2;
    
      s3 = mathieu_spdiags([e1,e2,e3], [-1,0,1], n, n);
    
      [v, d] = spec(full(s3));
      Brm = v(:, m/2);
      bm = d(m/2, m/2);

    else                    // odd order
      e1 = q .*ones(n,1);
      e2 = zeros(n,1);
      e3 = q .*ones(n,1);
      
      nn=1:n;
      e2(nn,1) = (2*nn'-1).^2;
    
      s3 = mathieu_spdiags([e1,e2,e3],[-1,0,1],n,n);
      s3(1,1) = 1-q;
      
      [v, d] = spec(full(s3));
      Brm=v(:, (m+1)/2);
      bm = d((m+1)/2, (m+1)/2);

    end;
    
    if real(Brm(1))<0 then
      Brm = -Brm;
    end
endfunction