// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function [x, y] = mathieu_ell2cart(xi, eta, f) 
    //this program transforms a point from Cartesian to elliptical coordinates
    //eta coordinates are the asymptotic angle of confocal hyperbolic cylinders symmetrical
    //about the x-axis. [0,2pi)
    //xi coordinates are confocal elliptic cylinders centered on the origin.[0,inf)  
    //f is the distance between the foci and the origin.
    //x, y the Cartesian coordiantes along the x and y axises, they are both matrices when xi and eta are vectors.

    x = f*cosh(xi)'*cos(eta);
    y = f*sinh(xi)'*sin(eta);
endfunction
