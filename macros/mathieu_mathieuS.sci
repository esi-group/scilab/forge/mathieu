// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function [y] = mathieu_mathieuS(z,a,q,ndet)
    // y=mathieu_mathieuS(z,a,q,ndet)
    //This program evaluates the  solution of first kind of  Mathieu Equation 
    // y''(t)+(a-2q cos(2t)) y(t)=0;
    // where a and q  are given real variables.

    // Matlab program by R.Coisson and G.Vernizzi, 
    // Parma University, 1999.

    [nu,c] = mathieu_mathieuexp(a,q,ndet);
    y=c'*exp(%i.*(nu+[-ndet:ndet]')*z);
endfunction
