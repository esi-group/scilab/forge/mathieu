// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function y = mathieu_rad_ms( m, q, z, fun_or_der, kind, Brm )
//MATHIEU_RAD_MS Odd radial (modified) Mathieu function or its first derivative.
//   Function for calculation of odd radial (modified) Mathieu function 'Ms' or its
//   first derivative (kinds 1 and 2)
//
//   Arguments:
//       m - function order
//       q - value of parameter q
//       z - argument
//    fun_or_der - calculate function (1) or derivative (0)
//     kind - kind of function (1 for besselj series, 2 for bessely series)
//      Arm - expansion coefficients for the same order m and value of q.

    // by default for 4 arguments we calculate 1-st kind
    if argn(2)<5
        kind=1;
    end

    // calculate Brm expansion coefficients inside or outside of this function
    if argn(2)<6
        // calculate expansion coefficients here for given m and q
        Brm = mathieu_Brm(m,q);
    end

    Ms = 0;
    Ms_ = 0;

    v1 = sqrt(q) * exp(-1.0*z);
    v2 = sqrt(q) * exp(z);    
    
        
    if modulo(m,2) == 0 // 2r+2
            // for numerical purposes we will normalize all functions as suggested in 20.4.13 from A-S
            [maxB,s]=max(abs(Brm));     
            B_r_mult = 1 / Brm(s);
        
            if kind==1 // besselj series - 2r+2 
                k=0:length(Brm)-1;
                k = k + 1;
                
                if fun_or_der // function (A-S 20.6.9 [s is found from 20.4.13, j=1])
                    Ms = power(-1, (m/2+k)) .* Brm' * (besselj(k-s,v1').*besselj(k+s,v2') - besselj(k+s,v1').*besselj(k-s,v2'))' / Brm(s);
                    
                else // derivative (obtained from A-S 20.6.9 [s is found from 20.4.13, j=1])
                    Ms_ = (power(-1, k+m/2)) .* -Brm(k)' * ( besselj(k-s,v1') .* ( besselj( s+k+1,v2') - besselj( s+k-1,v2') ))' .* v2 / 2 + ...
                          (power(-1, k+m/2)) .* -Brm(k)' * ( besselj(s+k,v1') .* ( besselj(-s+k-1,v2') - besselj(-s+k+1,v2') ))' .* v2 / 2 + ...
                          (power(-1, k+m/2)) .* -Brm(k)' * ( besselj(k-s,v2') .* ( besselj( s+k+1,v1') - besselj( s+k-1,v1') ))' .* v1 / 2 + ...
                          (power(-1, k+m/2)) .* -Brm(k)' * ( besselj(s+k,v2') .* ( besselj(-s+k-1,v1') - besselj(-s+k+1,v1') ))' .* v1 / 2;

                    Ms_ = Ms_ * B_r_mult ;
                end
                
            elseif kind==2  // bessely series - 2r+2
                
                k=0:length(Brm)-1;
                k = k + 1;
                
                if fun_or_der // function (A-S 20.6.9 [s is found from 20.4.13, j=2])
                    Ms = power(-1, (m/2+k)) .* Brm' * (besselj(k-s,v1').*bessely(k+s,v2') - besselj(k+s,v1').*bessely(k-s,v2'))' / Brm(s);
                else // derivative (obtained from A-S 20.6.9 [s is found from 20.4.13, j=2])
                    Ms_ = power(-1, (m/2+k)) .* -Brm(k)' * ( besselj(k-s,v1') .* (bessely( s+k+1,v2') - bessely( s+k-1,v2')) )' .* v2 / 2 +...
                          power(-1, (m/2+k)) .* -Brm(k)' * ( besselj(s+k,v1') .* (bessely(-s+k-1,v2') - bessely(-s+k+1,v2')) )' .* v2 / 2 +...
                          power(-1, (m/2+k)) .* -Brm(k)' * ( bessely(k-s,v2') .* (besselj( s+k+1,v1') - besselj( s+k-1,v1')) )' .* v1 / 2 +...
                          power(-1, (m/2+k)) .* -Brm(k)' * ( bessely(s+k,v2') .* (besselj(-s+k-1,v1') - besselj(-s+k+1,v1')) )' .* v1 / 2;

                    Ms_ = Ms_ * B_r_mult ;
               end
            end
        
    else // 2r+1
            // for numerical purposes we will normalize all functions as suggested in 20.4.13 from A-S
            [maxB,s]=max(abs(Brm));
            B_r_mult = 1 / Brm(s); 
            s=s-1;
        
        
            if kind==1 // besselj series - 2r+1
                k=0:length(Brm)-1;
                
                if fun_or_der // function (A-S 20.6.10 [s is found from 20.4.13, j=1])
                        Ms = power(-1, (m-1)/2+k) .* Brm' *  (besselj(k-s,v1').*besselj(k+s+1,v2') - besselj(k+s+1,v1').*besselj(k-s,v2'))' * B_r_mult;
                    
                else // derivative (obtained from A-S 20.6.10 [s is found from 20.4.13, j=1])
                        k=0:length(Brm)-1;
                        Ms_ = (power(-1, k+(m-1)/2)) .* -Brm(k+1)' * (   besselj(k-s,v1') .* ( besselj( s+k+2,v2') - besselj(   s+k,v2') ))' .* v2 / 2 + ...
                              (power(-1, k+(m-1)/2)) .* -Brm(k+1)' * ( besselj(s+k+1,v1') .* ( besselj(-s+k-1,v2') - besselj(-s+k+1,v2') ))' .* v2 / 2 + ...
                              (power(-1, k+(m-1)/2)) .* -Brm(k+1)' * ( besselj(  k-s,v2') .* ( besselj( s+k+2,v1') - besselj(   s+k,v1') ))' .* v1 / 2 + ...
                              (power(-1, k+(m-1)/2)) .* -Brm(k+1)' * ( besselj(s+k+1,v2') .* ( besselj(-s+k-1,v1') - besselj(-s+k+1,v1') ))' .* v1 / 2;

                        Ms_ = Ms_ * B_r_mult;
                end
                
            elseif kind==2 // bessely series - 2r+1
                k=0:length(Brm)-1;

                if fun_or_der // function (A-S 20.6.10 [s is found from 20.4.13, j=2])
                    Ms = power(-1, (m-1)/2+k) .* Brm' *  (besselj(k-s,v1').*bessely(k+s+1,v2') - besselj(k+s+1,v1').*bessely(k-s,v2'))' * B_r_mult;
                else // derivative (obtained from A-S 20.6.10 [s is found from 20.4.13, j=2])
                    Ms_ = (power(-1, k+(m-1)/2)) .* -Brm(k+1)' * (besselj(  k-s,v1') .* (bessely( s+k+2,v2') - bessely(   s+k,v2')) )' .* v2 / 2+...
                          (power(-1, k+(m-1)/2)) .* -Brm(k+1)' * (besselj(s+k+1,v1') .* (bessely(-s+k-1,v2') - bessely(-s+k+1,v2')) )' .* v2 / 2+...
                          (power(-1, k+(m-1)/2)) .* -Brm(k+1)' * (bessely(  k-s,v2') .* (besselj( s+k+2,v1') - besselj(   s+k,v1')) )' .* v1 / 2+...
                          (power(-1, k+(m-1)/2)) .* -Brm(k+1)' * (bessely(s+k+1,v2') .* (besselj(-s+k-1,v1') - besselj(-s+k+1,v1')) )' .* v1 / 2;

                    Ms_ = Ms_ * B_r_mult;
                end
            end
    end
    
    // returning function of its derivative
    if fun_or_der        
        y = Ms;
    else
        y = Ms_;
    end
    y = real(y);
endfunction


// local function for better readability
function p=power(a, b)
    p = a .^ b;
endfunction
